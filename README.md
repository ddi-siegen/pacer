# PACER DDI Uni Siegen
***
Applikation zur Vermittlung von Wissen im Bereich der Grundlagen der Informatik

## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [FAQs](#faqs)

### General Info
***
Pacer gibt seinen Nutzer*innen die Möglichkeit, sich spielerisch Wissen anzueignen. 
Dabei muss der Spieler in einem Escape Room Scenario Aufgaben erledigen, welche dafür konzipiert wurden, neues zu lernen und gelerntes zu testen oder zu wiederholen.

## Technologies
***
* [Unity](https://unity.com): Version 2022.1.21f1
* [GitHub](https://github.com)

## Installation
***
Die Pacer Applikation kann sowohl für Desktop-, als auch für Mobile Endgeräte als Web-App über https://pacer.server.ddi-siegen.de genutzt werden.
Link zum Android Playstore: https://play.google.com/store/apps/details?id=com.DDI.Pacer

## FAQs
***
Eine Auflistung von häufig gestellten Fragen
1. **Für welche Alters- und Zielgruppe wurde Pacer entwickelt?**
Das Pacer Projekt wurde für Student*innen im Bereich Lehramt Informatik entwickelt, um einen einfachen Einstieg in die informatische Grundbildung gewährleisten zu können. Im Laufe der Entwicklung und Evaluation in einer Schulklasse zeigte sich, dass auch Schüler großes Interesse und Spaß am Spiel haben.
2. **Welchen Schwierigkeitsgrad haben die im Projekt verwendeten Inhalte?**
Die Inhalte und Aufgaben richten sich an Anfänger und haben einen einfachen Schwierigkeitsgrad.
