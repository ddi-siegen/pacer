using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class ControlPanelPassword : MonoBehaviour
{
    public GameObject controlPanelPassword;
    public NavMeshAgent agent;

    public InputField passwordInput;
    public Button enterButton;
    public Button visibleButton;

    public TextBoxLogic TextBoxLogic;

    public GameObject orderTool;

    public int wrongPasswordCounter;
    // Start is called before the first frame update
    void Start()
    {
        passwordInput.contentType = InputField.ContentType.Password;
    }

    public void VisibleButtonCLicked()
    {
        AudioManager.instance.Play("Maus");
        if (passwordInput.inputType == InputField.InputType.Standard)
        {
            passwordInput.inputType = InputField.InputType.Password;
            passwordInput.ForceLabelUpdate();
        }
        else if (passwordInput.inputType == InputField.InputType.Password)
        {
            passwordInput.inputType = InputField.InputType.Standard;
            passwordInput.ForceLabelUpdate();

        }
    }
    public void EnterButtonClicked()
    {
        AudioManager.instance.Play("Maus");
        //correct Inputs
        string passwort = "Rc1Ea1Dt0";

        //delete all spaces from the two Input strings
        passwordInput.text = passwordInput.text.Replace(" ", "");

        //wrong input

        if (!passwordInput.text.Equals(passwort))
        {
            Text textPasswort = passwordInput.transform.Find("Text").GetComponent<Text>();
            textPasswort.color = Color.red;
            //increase wrong password counter
            wrongPasswordCounter += 1;

            if (wrongPasswordCounter == 3)
            {
                if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("wrongPasswordCPU*01")) // Check if Collider exists for Texts
                {
                    FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["wrongPasswordCPU*01"]);
                }
                else
                    Debug.Log("Collider not defined. Collider is: " + "wrongPasswordCPU*01");
            }
        }
        else
        {
            Text textPasswort = passwordInput.transform.Find("Text").GetComponent<Text>();
            textPasswort.color = Color.green;
            PlayerPrefs.SetInt("CpuBossPasswordInserted", 1);
            passwordInput.interactable = false;
            StartCoroutine(CoroutineWaitForSeconds(1.5f));
        }
    }

    private IEnumerator CoroutineWaitForSeconds(float secGreen) // Coroutine that waits for sec until the nextTextboxId will be shown
    {
        yield return new WaitForSeconds(secGreen);

        orderTool.SetActive(true);
        //orderTool.GetComponent<orderTool_manager>().activate(true);
        controlPanelPassword.SetActive(false);
    }
}


