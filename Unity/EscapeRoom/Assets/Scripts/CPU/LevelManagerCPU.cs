using UnityEngine;
using UnityEngine.AI;

public class LevelManagerCPU : LevelManager
{
    public LevelChanger levelChanger;
    public InventoryManager inventory;

    public GameObject quizMachinePrefab;

    //UI
    public GameObject Poster_GebaeudeplanCloseUp;
    public GameObject passwordNote;
    public GameObject passwordControlPanel;

    //Interior
    public Sprite trash_empty, trash_full;

    public GameObject trash, ControlePanel, Plug, deskContainerWithNote, deskContainerWithoutNote;
    public Sprite ControlePanel_on, insertedPlug;

    public GameObject orderTool;

    //player
    AgentScript agentScript;
    public GameObject player;
    public NavMeshAgent agent;
    public TextBoxLogic TextBoxLogic;

    //dog
    public GameObject doggo;
    DoggoScript doggoScript;
    NavMeshAgent navmeshDoggo;

    private bool doggoAtPlug = false;


    void Awake()
    {
        doggoScript = doggo.GetComponent<DoggoScript>();
        navmeshDoggo = doggo.GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        agentScript = player.GetComponent<AgentScript>();
        agent = player.GetComponent<NavMeshAgent>();
        if (PlayerPrefs.GetInt("REDpwTaken").Equals(0))
        {
            deskContainerWithNote.SetActive(true);
            deskContainerWithoutNote.SetActive(false);
        }
        else
        {
            deskContainerWithNote.SetActive(false);
            deskContainerWithoutNote.SetActive(true);
        }

        if (PlayerPrefs.GetString("currDogScene") != "CPU")
        {
            doggo.SetActive(false);
        }
        else
        {
            doggoScript.teleport(new Vector3(-17.3600006f, -6.9000001f, 0));
            doggoScript.startDrinking(new Vector3(-17.3600006f, -6.9000001f, 0));
        }

        if (PlayerPrefs.GetInt("BinaryPosterFound").Equals(0))
        {
            trash.GetComponent<SpriteRenderer>().sprite = trash_full;
        }
        else
        {
            trash.GetComponent<SpriteRenderer>().sprite = trash_empty;
        }

        if (PlayerPrefs.GetInt("PlugInserted").Equals(1))
        {
            insertPlugAndActivateMachine();
        }
    }

    //handles the collision of mouse (player) with objects named with specified tag
    public override void AgentCollision(string myCollider)
    {
        if (myCollider != "")
        {
            agentScript.stopAtCurrentPosition();
        }

        switch (myCollider)
        {
            case "slot_machine":
                Instantiate(quizMachinePrefab);
                break;
            case "JoyStick":
                FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["doggoHelpsWithPlug*01"]);
                if (doggoAtPlug == false)
                {
                    doggo.SetActive(true);
                    doggoScript.teleport(new Vector3(11f, 1f, 0));
                    doggoScript.walkToPosition(new Vector3(11.8f, 5.8f, 0));
                    doggoAtPlug = true;
                }
                break;

            case "exitSign":
                //Debug.Log("Exit");
                levelChanger.FadeToLevel(3);
                break;

            case "buildingPlan":
                Poster_GebaeudeplanCloseUp.SetActive(true);
                agent.isStopped = true;

                if (PlayerPrefs.GetInt("BuildingPlanFound").Equals(0))
                {
                    inventory.FillInventory(myCollider);

                    if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("buildingPlan*01")) // Check if Collider exists for Texts
                    {
                        FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["buildingPlan*01"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "buildingPlan*01");
                }
                break;

            case "trash":
                //print(PlayerPrefs.GetInt("BinaryPosterFound").Equals(0));
                if (PlayerPrefs.GetInt("BinaryPosterFound").Equals(0))
                {
                    inventory.FillInventory(myCollider);
                    trash.GetComponent<SpriteRenderer>().sprite = trash_empty;
                }
                break;

            case "desk_container_02":
            case "desk_container_02_withNote":
                if (PlayerPrefs.GetInt("REDpwTaken").Equals(0))
                {
                    passwordNote.SetActive(true);
                    agent.isStopped = true;
                    deskContainerWithNote.SetActive(false);
                    deskContainerWithoutNote.SetActive(true);

                    //notify NotesApp & Smartphone
                    FindObjectOfType<NewSmartphoneManager>().NotifyNewNote();

                    //add information from post it to Smartphone notes app
                    NoteManager noteManager = new NoteManager();
                    noteManager.newNote(content: "REDcat110", isNew: true, isEditable: false);
                    PlayerPrefs.SetInt("REDpwTaken", 1);
                    TextBoxLogic.setText(TextBoxLogic.Texts["PWNoteFound"]);
                }
                else
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["PWNoteFoundClickedAgain"]);
                }
                break;

            case "control_panel":
                if (PlayerPrefs.GetInt("CpuBossPasswordInserted").Equals(0))
                {
                    if (PlayerPrefs.GetInt("PlugInserted").Equals(0))
                    {
                        if (TextBoxLogic.Texts.ContainsKey("noEnergy")) // Check if Collider exists for Texts
                        {
                            TextBoxLogic.setText(TextBoxLogic.Texts["noEnergy"]);
                        }
                        else
                            Debug.Log("Collider not defined. Collider is: " + "noEnergy");
                        break;
                    }

                    if (PlayerPrefs.GetInt("REDpwTaken").Equals(1) && PlayerPrefs.GetInt("PlugInserted").Equals(1))
                    {
                        agent.isStopped = true;
                        orderTool.SetActive(true);
                        break;
                    }
                    else if (PlayerPrefs.GetInt("REDpwTaken").Equals(0) && PlayerPrefs.GetInt("PlugInserted").Equals(1))
                    {
                        if (TextBoxLogic.Texts.ContainsKey("control_panel")) // Check if Collider exists for Texts
                        {
                            TextBoxLogic.setText(TextBoxLogic.Texts["control_panel"]);
                        }
                        else
                            Debug.Log("Collider not defined. Collider is: " + "control_panel");
                        break;
                    }
                }
                else if (PlayerPrefs.GetInt("CpuBossPasswordInserted").Equals(1) && PlayerPrefs.GetInt("CreditCardCodeCorrect").Equals(0))
                {
                    orderTool.SetActive(true);
                }
                else
                {
                    if (TextBoxLogic.Texts.ContainsKey("bossPasswordCorrect")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["bossPasswordCorrect"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "bossPasswordCorrect");
                    break;
                }
                break;

            case "":
                //Debug.Log("Empty myCollider");
                break;

            //**Default**  
            default:
                bool isInTexts = false, isInItems = false;
                if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                {
                    isInTexts = true;
                    TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                }
                if (inventory.Items.ContainsKey(myCollider))
                {
                    isInItems = true;
                    //Debug.Log("Before fill Inventory");
                    //add specific item to items list in order to place it in inventory at correct position
                    //in need to check in which status we are currently in
                    inventory.FillInventory(myCollider);
                }
                if (!isInTexts && !isInItems)
                    Debug.Log("Collider not defined. Collider is: " + myCollider);
                break;
        }
        agentScript.myCollider = "";
    }

    public void insertPlugAndActivateMachine()
    {
        Plug.GetComponent<SpriteRenderer>().sprite = insertedPlug;
        ControlePanel.GetComponent<SpriteRenderer>().sprite = ControlePanel_on;
    }
}

