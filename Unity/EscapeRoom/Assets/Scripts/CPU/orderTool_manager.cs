using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.AI;

public class orderTool_manager : MonoBehaviour
{
    [Header("General")]
    [SerializeField] GameObject orderTool, closeButton, orderTool_CloseUp;
    [SerializeField] NavMeshAgent agent;

    [Header("selectShop")]
    [SerializeField] GameObject screen_selectShop, button1_selectShop, button2_selectShop, button3_selectShop;

    [Header("2Auth")]
    [SerializeField] GameObject screen_2auth, button_2auth_resendCode, button_2auth_submit;
    [SerializeField] InputField inputField_2auth_code;

     [Header("Login")]
    [SerializeField] GameObject screen_login, button_login;
    [SerializeField] InputField inputField_login;


    public void activate(bool boolean)
    {
        if (boolean)
        {
            if (PlayerPrefs.GetInt("cpuComputerLoggedIn") == 0) //if not logged in yet, show login screen
            {
                orderTool.SetActive(true);
                screen_selectShop.SetActive(false);
                screen_2auth.SetActive(false);
                screen_login.SetActive(true);
            }
            else if (PlayerPrefs.GetInt("correctOnlineShopSelected") == 0) //if not selected correct shop yet, show shop selection screen
            {
                orderTool.SetActive(true);
                screen_selectShop.SetActive(true);
                screen_login.SetActive(false);
                screen_2auth.SetActive(false);
            }
            else // if logged in and correct shop selected, show 2auth screen
            {
                orderTool.SetActive(true);
                screen_login.SetActive(false);
                screen_selectShop.SetActive(false);
                screen_2auth.SetActive(true);
            }
        }
        else
        {
            GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = false;
            FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(true);
            orderTool.SetActive(false);
        }
    }

    public void login()
    {
        if (inputField_login.text == "Rc1Ea1Dt0")
        {
            PlayerPrefs.SetInt("cpuComputerLoggedIn", 1);
            inputField_login.GetComponent<Image>().color = Color.green;
            screen_login.SetActive(false);
            screen_2auth.SetActive(false);
            screen_selectShop.SetActive(true);
        }
        else
        {
            inputField_login.GetComponent<Image>().color = Color.red;
        }
    }

    public void button_selectShopClicked(int id)
    {
        if (id == 1)
        {
            PlayerPrefs.SetInt("correctOnlineShopSelected", 1);
            MessageManager MessageManager = new MessageManager();
            MessageManager.newmessage(content: "Kreditkarten-Banking: Ihr 2-Faktor Sicherheitscode lautet: 2d355", isNew: true, isEditable: false, icon: "defaultIcon");
            screen_selectShop.SetActive(false);
            screen_2auth.SetActive(true);
        }
        else
        {
            FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["orderTool_wrongShopSelected00"]);
        }
    }

    public void button_2auth_resendCodeClicked()
    {
        FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["orderTool_resendCode00"]);
        MessageManager MessageManager = new MessageManager();
        MessageManager.newmessage(content: "Kreditkarten-Banking: Ihr 2-Faktor Sicherheitscode lautet: 2d355", isNew: true, isEditable: false, icon: "defaultIcon");
    }
    public void button_2auth_submitClicked()
    {
        if (inputField_2auth_code.text == "2d355")
        {
            PlayerPrefs.SetInt("SsdDepartmentOrdered", 1);
            if (PlayerPrefs.GetInt("LiftStatesCorrect").Equals(0))
            {
                GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Repariere den Fahrstuhl und fahre und zum Festplattenstockwerk.");
            }
            if (PlayerPrefs.GetInt("LiftStatesCorrect").Equals(1))
            {
                GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Fahre zum Festplattenstockwerk und erkunde es.");
            }

            agent.isStopped = false;
            inputField_2auth_code.GetComponent<Image>().color = Color.green;
            FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["orderTool_success00"]);
        }
        else
        {
            inputField_2auth_code.GetComponent<Image>().color = Color.red;
            FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["orderTool_wrongCode00"]);
        }
    }
}
