using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class CameraShake : MonoBehaviour
{
    public NavMeshAgent agent;
    [SerializeField] float shakeDuration = 1f;
    [SerializeField] float shakeMagnitude = 0.4f;

    // for Backgroundmovement
    [SerializeField] GameObject stars0;
    [SerializeField] GameObject stars1;


    [SerializeField] GameObject[] currentRoomNumbers;

    public Animator doorAnimator;

    Vector3 initialPosition;

    Vector3 agentinitialPosition;

    void Start()
    {
        initialPosition = transform.position;
    }

    public void Play(int levelIndex)
    {
        //check if same level is pressed, if so do nothing but enable walking again
        int pathLength = Mathf.Abs(PlayerPrefs.GetInt("previousScene") - levelIndex);
        if(pathLength != 0)
        {
            //activate animator controller
            doorAnimator.SetTrigger("Active");
            StartCoroutine(Shake(levelIndex));
        }
        else
        {
            agent.isStopped = false;
        }

    }
    IEnumerator Shake(int levelIndex)
    {
        int pathLength = Mathf.Abs(PlayerPrefs.GetInt("previousScene") - levelIndex);
        
        doorAnimator.SetBool("Open", false);
        yield return new WaitForSecondsRealtime(1.4f);
        float elapsedTime = 0;
        
        agentinitialPosition = agent.transform.position;
        while (elapsedTime < shakeDuration * pathLength)
        {
            if ((PlayerPrefs.GetInt("previousScene") - levelIndex) < 0)
            {
                stars0.GetComponent<SpriteScroller>().setBackgroundspeed(new Vector2(0f, 0.3f));
                stars1.GetComponent<SpriteScroller>().setBackgroundspeed(new Vector2(0f, 0.6f));
            }
            else
            {
                stars0.GetComponent<SpriteScroller>().setBackgroundspeed(new Vector2(0f, -0.3f));
                stars1.GetComponent<SpriteScroller>().setBackgroundspeed(new Vector2(0f, -0.6f));
            }


            agent.isStopped = true;
            transform.position = initialPosition + (Vector3)Random.insideUnitCircle * shakeMagnitude;
            agent.transform.position = agentinitialPosition + (Vector3)Random.insideUnitCircle * shakeMagnitude;
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        stars0.GetComponent<SpriteScroller>().setBackgroundspeed(new Vector2(0.02f, 0.02f));
        stars1.GetComponent<SpriteScroller>().setBackgroundspeed(new Vector2(0f, 0f));

        PlayerPrefs.SetInt("previousScene", levelIndex); //workaround to get last scene id
        agent.isStopped = false;
        resetButtonsCurrentRoomNumbers();
        currentRoomNumbers[levelIndex].SetActive(true);
        transform.position = initialPosition;
        agent.transform.position = agentinitialPosition;

        doorAnimator.SetBool("Open", true);

    }
        public void resetButtonsCurrentRoomNumbers()
    {
        for (int i = 0; i < currentRoomNumbers.Length; i++)
        {
            currentRoomNumbers[i].SetActive(false);
        }
    }
    
}
