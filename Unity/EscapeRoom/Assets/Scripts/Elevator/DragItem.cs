using UnityEngine;
using UnityEngine.EventSystems;

public class DragItem : MonoBehaviour, IPointerDownHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IDropHandler
{
    public DragSlot inSlot { get; set; }
    public static bool isDragging { get; private set; }

    private Vector2 _defaultPosition;
    private Vector2 _lastPosition;
    private RectTransform _rectTransform;
    private Canvas _canvas;
    private CanvasGroup _canvasGroup;
    [SerializeField] private GameObject _puzzleUI;
    private Transform _startParent;
    [SerializeField] private Transform _dragCanvas;
    private int _siblingIndex;


    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _canvas = GetComponentInParent<Canvas>();
        _canvasGroup = GetComponent<CanvasGroup>();
        _defaultPosition = transform.position;
        _lastPosition = transform.position;
    }

    // implements interfaces required for drag n drop
    public void OnPointerDown(PointerEventData eventData)
    {
        // Debug.Log("Clicked");
    }

    public void OnDrag(PointerEventData eventData)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            _puzzleUI.GetComponent<RectTransform>(),
            eventData.position,
            eventData.pressEventCamera,
            out Vector2 localPoint);

        // limit drag to puzzleUI

        int bgAdjustment = 12; // adjust for background image

        if (localPoint.x >= _puzzleUI.GetComponent<RectTransform>().rect.xMax - _rectTransform.rect.width / 2 - _puzzleUI.GetComponent<RectTransform>().rect.width / 8) // leave some hspace for menu buttons
        {
            localPoint.x = _puzzleUI.GetComponent<RectTransform>().rect.xMax - _rectTransform.rect.width / 2 - _puzzleUI.GetComponent<RectTransform>().rect.width / 8;
        }
        else if (localPoint.x <= _puzzleUI.GetComponent<RectTransform>().rect.xMin + _rectTransform.rect.width / 2 + bgAdjustment)
        {
            localPoint.x = _puzzleUI.GetComponent<RectTransform>().rect.xMin + _rectTransform.rect.width / 2 + bgAdjustment;
        }

        if (localPoint.y >= _puzzleUI.GetComponent<RectTransform>().rect.yMax - _rectTransform.rect.height / 2 - bgAdjustment)
        {
            localPoint.y = _puzzleUI.GetComponent<RectTransform>().rect.yMax - _rectTransform.rect.height / 2 - bgAdjustment;
        }
        else if (localPoint.y <= _puzzleUI.GetComponent<RectTransform>().rect.yMin + _rectTransform.rect.height / 2 + bgAdjustment)
        {
            localPoint.y = _puzzleUI.GetComponent<RectTransform>().rect.yMin + _rectTransform.rect.height / 2 + bgAdjustment;
        }

        _rectTransform.anchoredPosition = new Vector2(localPoint.x, localPoint.y);
        _canvasGroup.blocksRaycasts = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        DragItem.isDragging = true;
        _lastPosition = transform.position;
        _startParent = transform.parent;
        _siblingIndex = transform.GetSiblingIndex();
        transform.SetParent(_dragCanvas);
        // Debug.Log("Drag started");
        if (inSlot != null)
        {
            inSlot.isFilled = false;
            inSlot.isCorrectlyFilled = false;
            inSlot = null;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        DragItem.isDragging = false;
        // Debug.Log("Drag ended");
        _canvasGroup.blocksRaycasts = true;
        if (transform.parent == _dragCanvas)
        {
            transform.SetParent(_startParent);
            transform.SetSiblingIndex(_siblingIndex);
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        // Debug.Log("Dropped");
        eventData.pointerDrag.GetComponent<DragItem>().MoveToLastPosition();
    }

    public void MoveToLastPosition()
    {
        transform.position = _lastPosition;
    }

    public void ResetPosition()
    {
        transform.position = _defaultPosition;
    }
}