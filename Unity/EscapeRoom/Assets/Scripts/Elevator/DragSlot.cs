using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragSlot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    private PanelManager _panelManager;

    public bool isFilled { get; set; }
    public bool isCorrectlyFilled { get; set; }


    private void Awake()
    {
        _panelManager = GetComponentInParent<PanelManager>();
    }

    // implements interfaces required for drag n drop
    public void OnDrop(PointerEventData eventData)
    {
        // Debug.Log("Dropped in slot " + this);

        if (eventData.pointerDrag != null)
        {
            if (isFilled)
            {
                // Debug.Log("Slot is already filled");
                eventData.pointerDrag.GetComponent<DragItem>().MoveToLastPosition();
                return;
            }

            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = this.GetComponent<RectTransform>().anchoredPosition + new Vector2(0, 34); // adjust position of dragged item to center of slot

            isFilled = true;
            eventData.pointerDrag.GetComponent<DragItem>().inSlot = this;

            DragItem result;
            if (_panelManager.correctPairings.TryGetValue(this, out result))
            {
                if (result == eventData.pointerDrag.GetComponent<DragItem>())
                {
                    isCorrectlyFilled = true;
                }
            }

            AudioManager.instance.Play("drag_drop");
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // Debug.Log("Mouse entered slot " + this);
        if (DragItem.isDragging)
        {
            GetComponent<Outline>().enabled = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // Debug.Log("Mouse exited slot " + this);
        GetComponent<Outline>().enabled = false;
    }
}