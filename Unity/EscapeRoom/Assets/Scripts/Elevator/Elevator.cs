using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Elevator : MonoBehaviour
{
    Vector2 mousePos;
    private Vector2 targetPos;
    RaycastHit2D hit;
    public GameObject elevatorPanelScreen;

    bool panelOpen = false;
    void Update()
    {

        //getting coordinates of the mouse position in the game
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);


        //Check if left click, if yes get coordinates of click
        //0 is index for left mouse button
        if (Input.GetMouseButtonDown(0))
        {

            targetPos = new Vector2(mousePos.x, mousePos.y);

            //Raycast2D
            hit = Physics2D.Raycast(targetPos, Vector2.zero);

            if (hit.collider != null)
            {
                print("Objekt mit Collider wurde getroffen!");
                print("Name: " + hit.collider.gameObject.name);
                print("Tag: " + hit.collider.gameObject.tag);
                print("Klasse: Elevator");

                if (hit.collider.gameObject.tag == "PanelButton")
                {
                    if (!panelOpen)
                    {
                        elevatorPanelScreen.SetActive(true);
                    }
                    else
                    {
                        elevatorPanelScreen.SetActive(false);
                    }
                }
                
                else
                {
                    print("Kein Collider erkannt!");
                    print("Klasse: Elevator");
                }


            }
        }

    }

}
