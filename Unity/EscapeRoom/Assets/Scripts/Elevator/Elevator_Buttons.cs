using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Elevator_Buttons : MonoBehaviour
{
    [SerializeField] CameraShake cameraShake;
    [SerializeField] GameObject[] currentRoomNumbers;
    public LevelChanger levelChanger;
    public Button button_GoToHO, button_GoToCPU, button_GoToHD; // a button for each room/scene
    public Button button_exit; // button to exit ui 

    private int currentscene;

    [SerializeField] GameObject PanelButtonUI;

    public NavMeshAgent agent;

    void Start()
    {   // every button gets an Listener. When the button is clicked, the GoToScene method is called
        button_GoToHO.onClick.AddListener(delegate { GoToLevel(2); });
        button_GoToHD.onClick.AddListener(delegate { if (PlayerPrefs.GetInt("SsdDepartmentOrdered") == 1) { GoToLevel(1);} else {FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["FPNotYetRepaired"]);}; });
        button_GoToCPU.onClick.AddListener(delegate { GoToLevel(0); });
        button_exit.onClick.AddListener(delegate { exitUI(); });
    }
    public void GoToLevel(int levelIndex)
    {
        ShakeCamera(levelIndex);
        PlayerPrefs.SetInt("nextScene", levelIndex);
        PanelButtonUI.SetActive(false);
    }
    void exitUI()
    {
        PanelButtonUI.SetActive(false);
        agent.isStopped = false;

    }

    void ShakeCamera(int levelIndex)
    {
        if (cameraShake != null)
        {
            cameraShake.Play(levelIndex);
        }
    }
}