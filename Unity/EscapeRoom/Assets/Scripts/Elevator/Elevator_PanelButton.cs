using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator_PanelButton : MonoBehaviour
{
   public GameObject elevatorPanelScreen;
    bool panelOpen = false;
    // Start is called before the first frame update
    public void ButtonClicked()
    {
        //change state of bool isDisabled
        panelOpen = !panelOpen;
        elevatorPanelScreen.SetActive(panelOpen);
    }
}
