using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HatchManager : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _agent;

    [SerializeField] private GameObject _menuPanel;
    private MenuPanelManager _menuPanelManager;
    private InventoryManager _inventoryManager;

    [SerializeField] private GameObject _textBox;
    private TextBoxLogic _textBoxLogic;

    private bool _isLeverDown = false;
    public bool isLeverDown { get { return _isLeverDown; } }

    [SerializeField] private GameObject _backgroundImageLeverUp;
    [SerializeField] private GameObject _backgroundImageLeverDown;
    [SerializeField] private GameObject _backgroundImageSuccess;
    [SerializeField] private GameObject _PDFButton;
    [SerializeField] private GameObject _PDFScreen;

    // coordinates the wire dragging puzzle
    public Wire currentDraggedWire { get; set; }
    public Wire currentHoveredWire { get; set; }

    [SerializeField] private List<Wire> _wires = new List<Wire>();

    // correct wire pairings
    [SerializeField] private Dictionary<Wire, List<Wire>> _correctWires;
    public Dictionary<Wire, List<Wire>> correctWires { get { return _correctWires; } }

    private bool _isTaskCompleted = false;


    private void Awake()
    {
        // correct configurations (list of wires)
        _correctWires = new Dictionary<Wire, List<Wire>>
        {
            [_wires[0]] = new List<Wire> { _wires[5] },
            [_wires[1]] = new List<Wire> { _wires[3], _wires[8], _wires[11] },
            [_wires[2]] = new List<Wire> { _wires[4], _wires[7] },
            [_wires[3]] = new List<Wire> { _wires[1], _wires[8], _wires[11] },
            [_wires[4]] = new List<Wire> { _wires[2], _wires[6] },
            [_wires[5]] = new List<Wire> { _wires[0] },
            [_wires[6]] = new List<Wire> { _wires[4], _wires[7] },
            [_wires[7]] = new List<Wire> { _wires[2], _wires[6] },
            [_wires[8]] = new List<Wire> { _wires[1], _wires[3], _wires[11] },
            [_wires[9]] = new List<Wire> { _wires[10] },
            [_wires[10]] = new List<Wire> { _wires[9] },
            [_wires[11]] = new List<Wire> { _wires[1], _wires[3], _wires[8] }
        };
    }

    private void OnEnable()
    {
        _menuPanelManager = _menuPanel.GetComponent<MenuPanelManager>();
        _menuPanelManager.SetMenuPanelActive(false);

        _inventoryManager = _menuPanel.GetComponentInChildren<InventoryManager>();

        _textBoxLogic = _textBox.GetComponent<TextBoxLogic>();

        _agent.isStopped = true;

        if (PlayerPrefs.GetInt("WiringFilePrinted").Equals(1))
        {
            _PDFButton.SetActive(true);
        }
        else
        {
            _PDFButton.SetActive(false);
        }

        Debug.Log("CTC coroutine started");
        StartCoroutine(CheckTaskCompletion());
    }

    private void OnDisable()
    {
        _menuPanelManager.SetMenuPanelActive(true);

        _agent.isStopped = false;

        Debug.Log("CTC coroutine stopped");
        StopCoroutine(CheckTaskCompletion());
    }

    private IEnumerator CheckTaskCompletion()
    {
        while (!_isTaskCompleted)
        {
            int correctWires = 0;
            int connectedWires = 0;

            foreach (Wire wire in _wires)
            {
                if (wire.isConnected) { connectedWires++; }
                if (wire.isCorrect) { correctWires++; }
            }

            // all wires connected?
            if (connectedWires >= _wires.Count)
            {
                // all pairings correct?
                if (correctWires >= _wires.Count)
                {
                    Debug.Log("TASK COMPLETED");

                    _backgroundImageLeverUp.SetActive(false);
                    _backgroundImageLeverDown.SetActive(false);
                    _backgroundImageSuccess.SetActive(true);

                    AudioManager.instance.Play("Strom");
                    _isTaskCompleted = true;
                    PlayerPrefs.SetInt("LiftCablesCorrect", 1);
                    _inventoryManager.RefreshInventory();

                    GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Fahre zum Festplattenraum.");

                    if (_textBoxLogic.Texts.ContainsKey("hatchPuzzle*04")) // Check if Collider exists for Texts
                    {
                        _textBoxLogic.setText(_textBoxLogic.Texts["hatchPuzzle*04"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "hatchPuzzle*04");
                }
                else
                {
                    Debug.Log("TASK NOT COMPLETED");
                    if (_textBoxLogic.Texts.ContainsKey("hatchPuzzle*05")) // Check if Collider exists for Texts
                    {
                        _textBoxLogic.setText(_textBoxLogic.Texts["hatchPuzzle*05"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "hatchPuzzle*05");

                    AudioManager.instance.Play("StromFail");
                    Reset();
                }
            }
            else
            {
                // Debug.Log("TASK NOT COMPLETED");
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    private void Reset()
    {
        foreach (Wire wire in _wires)
        {
            wire.isConnected = false;
            wire.isCorrect = false;
            // remove the drawn wires
            wire.lineRenderer.SetPosition(0, Vector3.zero);
            wire.lineRenderer.SetPosition(1, Vector3.zero);
            // reset wire colors
            Color32 defaultColor = new Color32(0x80, 0x80, 0x80, 0xFF);
            wire.lineRenderer.startColor = defaultColor;
            wire.lineRenderer.endColor = defaultColor;
        }
        if (_isLeverDown)
        {
            LeverToggle();
        }
    }

    private void LeverToggle()
    {
        _isLeverDown = !_isLeverDown;
        _backgroundImageLeverDown.SetActive(!_backgroundImageLeverDown.activeInHierarchy);
        _backgroundImageLeverUp.SetActive(!_backgroundImageLeverUp.activeInHierarchy);
    }

    public void ButtonClickedBack()
    {
        gameObject.SetActive(false);
    }

    public void ButtonClickedReset() => Reset();

    public void ButtonClickedLeverToggle() => LeverToggle();

    public void ButtonClickedPDF()
    {
        _PDFScreen.SetActive(true);

        // fix for wires showing over pdf
        foreach (Wire wire in _wires)
        {
            wire.lineRenderer.startWidth = 0;
            wire.lineRenderer.endWidth = 0;
        }
    }

    public void BGBlockerClicked()
    {
        // make wires visible again
        foreach (Wire wire in _wires)
        {
            wire.lineRenderer.startWidth = 0.25f;
            wire.lineRenderer.endWidth = 0.25f;
        }

        _PDFScreen.SetActive(false);
    }

    public void ButtonClickedSkip()
    {
        Debug.Log("TASK SKIPPED");
        _isTaskCompleted = true;
        PlayerPrefs.SetInt("LiftCablesCorrect", 1);
        GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Fahre zum Festplattenraum.");
        gameObject.SetActive(false);
    }
}