using UnityEngine;
using UnityEngine.AI;

public class LevelManagerEL : LevelManager
{
    public GameObject Quiz;
    public GameObject uBlockly;
    public LevelChanger levelChanger;
    public TextBoxLogic TextBoxLogic;
    public GameObject Poster_funny;

    //UI
    public GameObject PanelButtonUI;
    public GameObject MenuPanel; // Settings and Inventory
    public GameObject hatchUI1;
    public GameObject hatchUI2;
    public GameObject puzzleUI;

    //player
    AgentScript agentScript;
    public GameObject player;
    public NavMeshAgent agent;

    QuizManager quizManager;

    [SerializeField] GameObject facility_manager;

    [SerializeField] GameObject[] currentRoomNumbers;


    void Awake() // Awake gets called before start. Finding Objects in Awake is best practice
    {
        agentScript = player.GetComponent<AgentScript>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Quiz.SetActive(false);

        int previousLevel = PlayerPrefs.GetInt("previousScene");
        //Debug.Log("previous scene: " + previousLevel);
        currentRoomNumbers[previousLevel].SetActive(true);


        TextBoxLogic.setText(TextBoxLogic.Texts["security_dialogue0"]);
    }

    //handles the collision of mouse (player) with objects named with specified tag
    public override void AgentCollision(string myCollider)
    {
        if (myCollider != "")
        {
            agentScript.stopAtCurrentPosition();
        }
        switch (myCollider)
        {
            case "quizButton":
                quizManager = new QuizManager();
                // we could check if the Quiz was already solved with: quizmanager.checkQuizSolved(string name)
                quizManager.setCurrentQuizName("passwordQuiz");
                Instantiate(Quiz);
                agent.isStopped = true;
                break;

            case "facility_manager":
                if (PlayerPrefs.GetInt("VentilationFixed") == 1)
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["facility_manager*03"]);
                }
                else
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["facility_manager*00"]);
                }
                break;

            case "elevator":
                if (PlayerPrefs.GetInt("talkedToAsr").Equals(1) && PlayerPrefs.GetInt("ElevatorPanelBlockedAgain").Equals(1) && PlayerPrefs.GetInt("previousScene").Equals(1)) // cant go back to FP-Scene after the dialog with asr
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["ElevatorDoorToFpBlocked"]);
                }
                else
                {
                    levelChanger.FadeToLevel(PlayerPrefs.GetInt("previousScene"));
                }
                break;

            case "elevatorButtons":
                quizManager = new QuizManager();
                if (PlayerPrefs.GetInt("talkedToAsr").Equals(1) && PlayerPrefs.GetInt("ElevatorPanelBlockedAgain").Equals(1) && PlayerPrefs.GetInt("previousScene").Equals(1) && PlayerPrefs.GetInt("LiftQuestionTwoAnswered").Equals(0)) // cant go back to FP-Scene after the dialog with asr
                {
                    quizManager.setCurrentQuizName("passwordQuiz2");
                    Instantiate(Quiz);
                    agent.isStopped = true;
                }
                // only start panel puzzle if the building plan is found
                else if (PlayerPrefs.GetInt("ElevatorPanelBlockedAgain").Equals(1) && PlayerPrefs.GetInt("BuildingPlanFound").Equals(0))
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["ElevatorPanelBlockedAgainTextBox"]);
                }
                else if (PlayerPrefs.GetInt("ElevatorPanelBlockedAgain").Equals(1) && PlayerPrefs.GetInt("BuildingPlanFound").Equals(1))
                {
                    agent.isStopped = true;
                    puzzleUI.SetActive(true);
                }

                else if (PlayerPrefs.GetInt("LiftCablesCorrect").Equals(1) && PlayerPrefs.GetInt("LiftStatesCorrect").Equals(0))
                {
                    quizManager.setCurrentQuizName("passwordQuiz");
                    Instantiate(Quiz);
                    agent.isStopped = true;
                }
                else if (quizManager.checkQuizSolved("passwordQuiz"))
                {
                    PanelButtonUI.SetActive(true);
                    agent.isStopped = true;
                }
                // elevator panel has not yet been activated
                else if (PlayerPrefs.GetInt("ElevatorPanelActive").Equals(0))
                {
                    if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + myCollider);
                }
                // player can now move freely through the building
                else if (PlayerPrefs.GetInt("ElevatorPanelActive").Equals(1))
                {
                    PanelButtonUI.SetActive(true);
                    agent.isStopped = true;
                }
                break;

            case "poster_funny":
                if (PlayerPrefs.GetInt("EmbarrassingPosterClicked").Equals(0))
                {
                    if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + myCollider);
                }
                if (PlayerPrefs.GetInt("EmbarrassingPosterClicked").Equals(1) && PlayerPrefs.GetInt("SSIDToNotes").Equals(1))
                {
                    if (TextBoxLogic.Texts.ContainsKey("poster_funny*01")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["poster_funny*01"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "poster_funny*01");
                }
                else if (PlayerPrefs.GetInt("EmbarrassingPosterClicked").Equals(1))
                {
                    Poster_funny.SetActive(true);
                    agent.isStopped = true;
                }
                break;

            case "hatch":
                if (PlayerPrefs.GetInt("ScrewdriverTaken").Equals(0))
                {
                    if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + myCollider);
                }
                // first time hatch is clicked (with screwdriver in inventory)
                else if (PlayerPrefs.GetInt("ScrewdriverTaken").Equals(1) && PlayerPrefs.GetInt("LiftCablesCorrect").Equals(0) && PlayerPrefs.GetInt("WirePuzzleInProgress").Equals(0))
                {
                    agent.isStopped = true;
                    hatchUI1.SetActive(true);
                }
                // subsequent clicks on hatch (skip screwdriver interaction)
                else if (PlayerPrefs.GetInt("ScrewdriverTaken").Equals(1) && PlayerPrefs.GetInt("LiftCablesCorrect").Equals(0) && PlayerPrefs.GetInt("WirePuzzleInProgress").Equals(1))
                {
                    hatchUI2.SetActive(true);
                    agent.isStopped = true;
                    if (PlayerPrefs.GetInt("WiringFilePrinted").Equals(0))
                    {
                        if (TextBoxLogic.Texts.ContainsKey("hatchPuzzle*03")) // Check if Collider exists for Texts
                        {
                            TextBoxLogic.setText(TextBoxLogic.Texts["hatchPuzzle*03"]);
                        }
                        else
                            Debug.Log("Collider not defined. Collider is: " + "hatchPuzzle*03");
                    }
                }
                // puzzle already solved
                else if (PlayerPrefs.GetInt("LiftCablesCorrect").Equals(1))
                {
                    if (TextBoxLogic.Texts.ContainsKey("hatchPuzzle*06")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["hatchPuzzle*06"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "hatchPuzzle*06");
                }
                break;

            // disabled in final version
            case "blocklyButton":
                uBlockly.SetActive(true);
                MenuPanel.SetActive(false);
                agent.isStopped = true;
                break;

            case "":
                //Debug.Log("Empty myCollider");
                break;

            //**Default**  
            default:
                if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                }
                else
                    Debug.Log("Collider not defined. Collider is: " + myCollider);
                break;
        }
        agentScript.myCollider = "";
    }

    public void ButtonClickedClose()
    {
        Poster_funny.SetActive(false);
        agent.isStopped = false;
    }
}

