using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PanelManager : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _agent;

    [SerializeField] private GameObject _menuPanel;
    private MenuPanelManager _menuPanelManager;
    private InventoryManager _inventoryManager;

    [SerializeField] private GameObject _textBox;
    private TextBoxLogic _textBoxLogic;

    public GameObject _buildingPDFButton;
    public GameObject _buildingPDFScreen;

    public GameObject _binaryPDFButton;
    public GameObject _binaryPDFScreen;

    public List<DragItem> _dragItems = new List<DragItem>();
    public List<DragSlot> _dragSlots = new List<DragSlot>();

    private Dictionary<DragSlot, DragItem> _correctPairings;
    public Dictionary<DragSlot, DragItem> correctPairings { get { return _correctPairings; } }

    private bool _isTaskCompleted = false;


    private void Awake()
    {
        _correctPairings = new Dictionary<DragSlot, DragItem>
        {
            [_dragSlots[0]] = _dragItems[0],
            [_dragSlots[1]] = _dragItems[1],
            [_dragSlots[2]] = _dragItems[2]
        };
    }

    private void OnEnable()
    {
        _menuPanelManager = _menuPanel.GetComponent<MenuPanelManager>();
        _menuPanelManager.SetMenuPanelActive(false);

        _inventoryManager = _menuPanel.GetComponentInChildren<InventoryManager>();

        _textBoxLogic = _textBox.GetComponent<TextBoxLogic>();

        _agent.isStopped = true;

        if (PlayerPrefs.GetInt("BuildingPlanFound").Equals(1))
        {
            _buildingPDFButton.SetActive(true);
        }
        else
        {
            _buildingPDFButton.SetActive(false);
        }

        if (PlayerPrefs.GetInt("BinaryPosterFound").Equals(1))
        {
            if (_textBoxLogic.Texts.ContainsKey("panelPuzzle*01b")) // Check if Collider exists for Texts
            {
                _textBoxLogic.setText(_textBoxLogic.Texts["panelPuzzle*01b"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "panelPuzzle*01b");

            _binaryPDFButton.SetActive(true);
        }
        else
        {
            if (_textBoxLogic.Texts.ContainsKey("panelPuzzle*01a")) // Check if Collider exists for Texts
            {
                _textBoxLogic.setText(_textBoxLogic.Texts["panelPuzzle*01a"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "panelPuzzle*01a");

            _binaryPDFButton.SetActive(false);
        }

        Debug.Log("CTC coroutine started");
        StartCoroutine(CheckTaskCompletion());
    }

    private void OnDisable()
    {
        _menuPanelManager.SetMenuPanelActive(true);

        _agent.isStopped = false;

        Debug.Log("CTC coroutine stopped");
        StopCoroutine(CheckTaskCompletion());
    }

    private IEnumerator CheckTaskCompletion()
    {
        while (!_isTaskCompleted)
        {
            int correctlyFilledSlots = 0;
            int filledSlots = 0;

            foreach (DragSlot dragSlot in _dragSlots)
            {
                if (dragSlot.isFilled) { filledSlots++; }
                if (dragSlot.isCorrectlyFilled) { correctlyFilledSlots++; }
            }

            // all slots filled?
            if (filledSlots >= _dragSlots.Count)
            {
                // all pairings correct?
                if (correctlyFilledSlots >= _dragSlots.Count)
                {
                    Debug.Log("TASK COMPLETED");
                    if (_textBoxLogic.Texts.ContainsKey("panelPuzzle*04")) // Check if Collider exists for Texts
                    {
                        _textBoxLogic.setText(_textBoxLogic.Texts["panelPuzzle*04"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "panelPuzzle*04");

                    AudioManager.instance.Play("correct");
                    _isTaskCompleted = true;
                    PlayerPrefs.SetInt("LiftStatesCorrect", 1);
                    PlayerPrefs.SetInt("ElevatorPanelBlockedAgain", 0);
                    PlayerPrefs.SetInt("ElevatorPanelActive", 1);
                    _inventoryManager.RefreshInventory();

                    if (PlayerPrefs.GetInt("SsdDepartmentOrdered").Equals(0))
                    {
                        GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Bestelle neue Teile für die Festplatte am Computer!");
                    }
                    if (PlayerPrefs.GetInt("SsdDepartmentOrdered").Equals(1))
                    {
                        GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Erkunde den Festplattenraum!");
                    }
                    
                    gameObject.SetActive(false);
                }
                else
                {
                    // Debug.Log("WRONG PAIRINGS: TASK NOT COMPLETED");
                }
            }
            else
            {
                // Debug.Log("TASK NOT COMPLETED");
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    public void ButtonClickedBuildingPDF()
    {
        _buildingPDFScreen.SetActive(true);
    }

    public void ButtonClickedBinaryPDF()
    {
        _binaryPDFScreen.SetActive(true);
    }

    public void BGBlockerClicked()
    {
        _buildingPDFScreen.SetActive(false);
        _binaryPDFScreen.SetActive(false);
    }

    public void ButtonClickedBack()
    {
        gameObject.SetActive(false);
    }
}