using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Wire : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    [SerializeField] private GameObject _textBox;
    private TextBoxLogic _textBoxLogic;

    public LineRenderer lineRenderer { get; private set; }
    private Image _image;
    private Canvas _canvas;
    private bool _isDragging = false;
    private HatchManager _hatchManager;

    public bool isConnected = false;
    public bool isCorrect = false;


    private void Awake()
    {
        _textBoxLogic = _textBox.GetComponent<TextBoxLogic>();

        _image = GetComponent<Image>();
        lineRenderer = GetComponent<LineRenderer>();
        _canvas = GetComponentInParent<Canvas>();
        _hatchManager = GetComponentInParent<HatchManager>();
    }

    private void Update()
    {
        if (_isDragging)
        {
            // redraw wire while dragging
            Vector2 movePosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                        _canvas.transform as RectTransform,
                        Input.mousePosition,
                        _canvas.worldCamera,
                        out movePosition);
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1,
              _canvas.transform.TransformPoint(movePosition));
        }
        else
        {
            // don't draw anything if wires are not successfully connected
            if (!isConnected)
            {
                lineRenderer.SetPosition(0, Vector3.zero);
                lineRenderer.SetPosition(1, Vector3.zero);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _hatchManager.currentHoveredWire = this;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _hatchManager.currentHoveredWire = null;
    }

    // implements interfaces required for drag n drop
    public void OnDrag(PointerEventData eventData)
    {

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!_hatchManager.isLeverDown)
        {
            if (_textBoxLogic.Texts.ContainsKey("hatchPuzzle*07")) // Check if Collider exists for Texts
            {
                _textBoxLogic.setText(_textBoxLogic.Texts["hatchPuzzle*07"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "hatchPuzzle*07");
            return;
        }

        // check if wire is already connected
        if (isConnected) { return; }

        _isDragging = true;
        _hatchManager.currentDraggedWire = this;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!_hatchManager.isLeverDown) { return; }

        // did we successfully connect two different wires which aren't already connected otherwise?
        if (_hatchManager.currentHoveredWire != null && _hatchManager.currentHoveredWire != this && !_hatchManager.currentHoveredWire.isConnected)
        {
            List<Wire> result;
            // are the _correct_ wires connected?
            if (_hatchManager.correctWires.TryGetValue(this, out result))
            {
                if (result.Contains(_hatchManager.currentHoveredWire))
                {
                    isCorrect = true;
                    _hatchManager.currentHoveredWire.isCorrect = true;
                }
            }
            // both wires are now connected
            isConnected = true;
            _hatchManager.currentHoveredWire.isConnected = true;
            // set wire colors
            Color startColor = _image.color;
            startColor.a = 1f;
            Color endColor = _hatchManager.currentHoveredWire._image.color;
            endColor.a = 1f;
            lineRenderer.startColor = startColor;
            lineRenderer.endColor = endColor;

            AudioManager.instance.Play("drag_drop");
        }
        _isDragging = false;
        _hatchManager.currentDraggedWire = null;
    }
}