using UnityEngine;
using UnityEngine.AI;

public class LevelManagerFP : LevelManager
{
    public LevelChanger levelChanger;

    public GameObject Book;

    //UI
    public GameObject Cloud_CloseUp, Handyverbot_CloseUp, Schweigen_CloseUP, interior, interior_broken;

    //player
    AgentScript agentScript;
    public GameObject player;
    public NavMeshAgent agent;
    public TextBoxLogic TextBoxLogic;

    private QuizManager quizManager;
    private uBlocklyLevelManager blocklyLevelManager;


    private void Awake()
    {
        agentScript = player.GetComponent<AgentScript>();
        agent = player.GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        quizManager = new QuizManager();
        blocklyLevelManager = new uBlocklyLevelManager();
        TextBoxLogic.setText(TextBoxLogic.Texts["talkToAsr*01"]);
        if (PlayerPrefs.GetInt("talkedToAsr").Equals(0))
        {
            interior_broken.SetActive(true);
            interior.SetActive(false);
        }
        else
        {
            interior_broken.SetActive(false);
            interior.SetActive(true);
        }
    }

    //handles the collision of mouse (player) with objects named with specified tag
    public override void AgentCollision(string myCollider)
    {
        if (myCollider != "")
        {
            agentScript.stopAtCurrentPosition();
        }
        switch (myCollider)
        {
            case "exitSign":
                //Debug.Log("Exit");
                levelChanger.FadeToLevel(3);
                break;

            case "CloudErklaerung":
                print("1");
                Cloud_CloseUp.SetActive(true);
                agent.isStopped = true;
                break;

            case "handyverbot":
                Debug.Log("handyverbot");
                Handyverbot_CloseUp.SetActive(true);
                agent.isStopped = true;
                break;

            case "schweigen_ist_gold":
                Schweigen_CloseUP.SetActive(true);
                agent.isStopped = true;
                break;

            case "worker_0":
                if (quizManager.checkQuizSolved("fpQuiz"))
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["fp_worker_0_dialogue_solved"]);
                }
                else
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["fp_worker_0_dialogue_unsolved"]);
                }
                break;

            case "worker_1":
                if (blocklyLevelManager.checkUblocklyLevelSolved(4))
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["fp_worker_1_dialogue_solved"]);
                }
                else
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["fp_worker_1_dialogue_unsolved"]);
                }
                break;

            case "worker_2":
                TextBoxLogic.setText(TextBoxLogic.Texts["fp_worker_2_dialogue_0"]);
                break;

            case "reception_area":
                if (PlayerPrefs.GetInt("AntiVirusBookTaken").Equals(1))
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["reception_roboter*AntiVirusBookTaken*00"]);
                }
                else if (PlayerPrefs.GetInt("TalkedToRR_1").Equals(1))
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["reception_roboter*alreadyTalked"]);
                }
                else
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["reception_roboter*01"]);
                }
                break;

            case "info_panel":
                if (PlayerPrefs.GetInt("TalkedToRR_1").Equals(0))
                {
                    if (TextBoxLogic.Texts.ContainsKey("sortingPuzzleTalkToRRFirst")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["sortingPuzzleTalkToRRFirst"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "sortingPuzzleTalkToRRFirst");

                    break;
                }
                if (PlayerPrefs.GetInt("SortalgorithmFixed").Equals(0))
                {
                    agent.isStopped = true;
                    // sorting puzzle is activated via textbox
                    if (TextBoxLogic.Texts.ContainsKey("sortingPuzzle*01")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["sortingPuzzle*01"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "sortingPuzzle*01");

                    break;
                }
                else
                {
                    if (TextBoxLogic.Texts.ContainsKey("sortingPuzzleSolved")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["sortingPuzzleSolved"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "sortingPuzzleSolved");

                    break;
                }

            case "shelf_05":
                if (PlayerPrefs.GetInt("AntiVirusBookTaken").Equals(1))
                {
                    if (TextBoxLogic.Texts.ContainsKey("antiVirusBookAlreadyTaken")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["antiVirusBookAlreadyTaken"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "antiVirusBookAlreadyTaken");

                    break;
                }
                if (PlayerPrefs.GetInt("SearchedForAntivirusprogram").Equals(1))
                {
                    // found anti virus program/book
                    PlayerPrefs.SetInt("AntiVirusBookTaken", 1);

                    if (TextBoxLogic.Texts.ContainsKey("antiVirusBookTaken")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["antiVirusBookTaken"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "antiVirusBookTaken");

                    GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Erfolg! Wir sollten Bericht erstatten.");

                    break;
                }
                if (PlayerPrefs.GetInt("SortalgorithmFixed").Equals(0))
                {
                    if (TextBoxLogic.Texts.ContainsKey("unsortedShelf")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["unsortedShelf"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "unsortedShelf");

                    break;
                }
                else
                {
                    agent.isStopped = true;
                    // binary search puzzle starts via textbox
                    if (TextBoxLogic.Texts.ContainsKey("searchPuzzle")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["searchPuzzle"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "searchPuzzle");

                    break;
                }

            case "shelf_01":
            case "shelf_02":
            case "shelf_03":
            case "shelf_04":
            case "shelf_06":
                if (PlayerPrefs.GetInt("AntiVirusBookTaken").Equals(1))
                {
                    if (TextBoxLogic.Texts.ContainsKey("antiVirusBookAlreadyTaken")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["antiVirusBookAlreadyTaken"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "antiVirusBookAlreadyTaken");

                    break;
                }
                if (PlayerPrefs.GetInt("SearchedForAntivirusprogram").Equals(1))
                {
                    if (TextBoxLogic.Texts.ContainsKey("antiVirusBookNotTaken")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["antiVirusBookNotTaken"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "antiVirusBookNotTaken");

                    break;
                }
                if (PlayerPrefs.GetInt("SortalgorithmFixed").Equals(0))
                {
                    if (TextBoxLogic.Texts.ContainsKey("unsortedShelf")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["unsortedShelf"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "unsortedShelf");

                    break;
                }
                else
                {
                    agent.isStopped = true;
                    // binary search puzzle starts via textbox
                    if (TextBoxLogic.Texts.ContainsKey("searchPuzzle")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["searchPuzzle"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "searchPuzzle");

                    break;
                }

            case "shelf_side_01":
            case "shelf_side_02":
                Book.GetComponentInChildren<BookManager>().openBook("BubbleSort");
                Instantiate(Book);
                PlayerPrefs.SetInt("BubbleSortBookRead", 1);
                break;

            case "shelf_side_03":
            case "shelf_side_04":
                Book.GetComponentInChildren<BookManager>().openBook("BinaereSuche");
                Instantiate(Book);
                PlayerPrefs.SetInt("BinarySearchBookRead", 1);
                break;

            case "shelf_side_05":
            case "shelf_side_06":
                Book.GetComponentInChildren<BookManager>().openBook("Netiquette");
                Instantiate(Book);
                break;

            case "shelf_side_07":
            case "shelf_side_08":
            case "shelf_side_09":
            case "shelf_side_10":
            case "shelf_side_11":
            case "shelf_side_12":
                if (TextBoxLogic.Texts.ContainsKey("just_a_shelf")) // Check if Collider exists for Texts
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["just_a_shelf"]);
                }
                else
                    Debug.Log("Collider not defined. Collider is: " + "just_a_shelf");

                break;

            case "":
                //Debug.Log("Empty myCollider");
                break;

            //**Default**  
            default:
                if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                }
                else
                    Debug.Log("Collider not defined. Collider is: " + myCollider);
                break;
        }
        agentScript.myCollider = "";
    }
}

