using System;
using UnityEngine;
using UnityEngine.UI;

public class SearchItem : MonoBehaviour, IComparable<SearchItem>, IComparable<int>
{
    private int _id;


    public int CompareTo(SearchItem other)
    {
        return _id.CompareTo(other._id);
    }

    public int CompareTo(int other)
    {
        return _id.CompareTo(other);
    }

    private void Awake()
    {
        _id = int.Parse(GetComponentInChildren<Text>().text);
    }

    public void ShowText(bool show)
    {
        GetComponentInChildren<Text>().text = show ? _id.ToString() : "?";
    }

    public void SetOutline(bool enabled)
    {
        GetComponent<Outline>().enabled = enabled;
    }

    public void SetOutlineColor(Color color)
    {
        GetComponent<Outline>().effectColor = color;
    }
}