using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class SearchPuzzleManager : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _agent;

    [SerializeField] private GameObject _menuPanel;
    private MenuPanelManager _menuPanelManager;

    [SerializeField] private GameObject _textBox;
    private TextBoxLogic _textBoxLogic;

    [SerializeField] private GameObject _book;
    [SerializeField] private GameObject _binarySearchBookButton;

    [SerializeField] private List<SearchItem> _searchItems = new List<SearchItem>();

    [SerializeField] private List<SearchItem> _defaultItems = new List<SearchItem>();

    [SerializeField] private GameObject _gesuchteZahl;

    [SerializeField] private GameObject _leftButton;
    [SerializeField] private GameObject _rightButton;

    private int _searchNumber = 55;


    private void Start()
    {
        _defaultItems.AddRange(_searchItems);

        // enable outline for all items and hide text
        foreach (SearchItem item in _searchItems)
        {
            item.SetOutline(true);
            item.ShowText(false);
        }

        int middle = _searchItems.Count / 2;

        // re-enable text for middle item
        _searchItems[middle].ShowText(true);

        // change outline color for middle item to green
        _searchItems[middle].SetOutlineColor(Color.green);

        if (_searchItems[middle].CompareTo(_searchNumber) == 0)
        {
            RightAnswerGiven();
        }

        _gesuchteZahl.GetComponent<Text>().text = _searchNumber.ToString();
    }

    private void OnEnable()
    {
        _menuPanelManager = _menuPanel.GetComponent<MenuPanelManager>();
        _menuPanelManager.SetMenuPanelActive(false);

        _textBoxLogic = _textBox.GetComponent<TextBoxLogic>();

        _agent.isStopped = true;

        if (PlayerPrefs.GetInt("BinarySearchBookRead").Equals(1))
        {
            _binarySearchBookButton.SetActive(true);
        }
        else
        {
            _binarySearchBookButton.SetActive(false);
        }
    }

    private void OnDisable()
    {
        _menuPanelManager.SetMenuPanelActive(true);

        _agent.isStopped = false;
    }

    private void SearchStep(bool left)
    {
        AudioManager.instance.Play("Click");

        // get middle item index
        int middle = _searchItems.Count / 2;

        // remove items right or left of middle item, disable their outlines and fade them out
        if (left)
        {
            for (int i = middle; i < _searchItems.Count; i++)
            {
                _searchItems[i].SetOutline(false);
                _searchItems[i].GetComponent<CanvasGroup>().alpha = 0.5f;
            }

            _searchItems.RemoveRange(middle, _searchItems.Count - middle);
        }
        else
        {
            for (int i = 0; i <= middle; i++)
            {
                _searchItems[i].SetOutline(false);
                _searchItems[i].GetComponent<CanvasGroup>().alpha = 0.5f;
            }

            _searchItems.RemoveRange(0, middle + 1);
        }

        middle = _searchItems.Count / 2;

        // change outline color of new middle item to green and enable its outline and text
        _searchItems[middle].SetOutlineColor(Color.green);
        _searchItems[middle].SetOutline(true);
        _searchItems[middle].ShowText(true);

        if (_searchItems.Count == 0 || (_searchItems.Count == 1 && _searchItems[0].CompareTo(_searchNumber) != 0))
        {
            WrongAnswerGiven();
            return;
        }

        if (_searchItems[middle].CompareTo(_searchNumber) == 0)
        {
            RightAnswerGiven();
            return;
        }

        // set left or right button non-interactable if current middle item is the first or last item
        if (middle == 0)
        {
            _leftButton.GetComponent<Button>().interactable = false;
        }
        else if (middle == _searchItems.Count - 1)
        {
            _rightButton.GetComponent<Button>().interactable = false;
        }
    }

    private void RightAnswerGiven()
    {
        Debug.Log("Found " + _searchNumber);
        AudioManager.instance.Play("correct");
        PlayerPrefs.SetInt("SearchedForAntivirusprogram", 1);

        if (_textBoxLogic.Texts.ContainsKey("searchPuzzleRightAnswer")) // Check if Collider exists for Texts
        {
            _textBoxLogic.setText(_textBoxLogic.Texts["searchPuzzleRightAnswer"]);
            GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Suche im zweiten Bücherregal unten rechts nach der Datei.");
        }
        else
            Debug.Log("Collider not defined. Collider is: " + "searchPuzzleRightAnswer");
    }

    private void WrongAnswerGiven()
    {
        Debug.Log("Not found " + _searchNumber);
        AudioManager.instance.Play("wrong_answer");

        if (_textBoxLogic.Texts.ContainsKey("searchPuzzleWrongAnswer")) // Check if Collider exists for Texts
        {
            _textBoxLogic.setText(_textBoxLogic.Texts["searchPuzzleWrongAnswer"]);
        }
        else
            Debug.Log("Collider not defined. Collider is: " + "searchPuzzleWrongAnswer");
    }

    private void Reset()
    {
        _searchItems.Clear();
        _searchItems.AddRange(_defaultItems);

        // enable outline for all items, reset alpha and text status
        foreach (SearchItem item in _searchItems)
        {
            item.SetOutlineColor(Color.yellow);
            item.SetOutline(true);
            item.GetComponent<CanvasGroup>().alpha = 1f;
            item.ShowText(false);
        }

        int middle = _searchItems.Count / 2;

        // change outline color for middle item to green
        _searchItems[middle].SetOutlineColor(Color.green);

        // enable text for middle item
        _searchItems[middle].ShowText(true);

        // unblock buttons
        _leftButton.GetComponent<Button>().interactable = true;
        _rightButton.GetComponent<Button>().interactable = true;
    }

    public void ButtonClickedLeft() => SearchStep(true);

    public void ButtonClickedRight() => SearchStep(false);

    public void ButtonClickedReset() => Reset();

    public void ButtonClickedBack()
    {
        gameObject.SetActive(false);
    }

    public void ButtonClickedBinarySearchBook()
    {
        _book.GetComponentInChildren<BookManager>().openBook("BinaereSuche");
        Instantiate(_book);
    }
}