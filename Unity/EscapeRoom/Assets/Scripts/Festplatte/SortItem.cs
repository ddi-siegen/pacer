using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SortItem : MonoBehaviour, IComparable<SortItem>
{
    private bool _isLerping = false;
    public bool isLerping { get { return _isLerping; } }

    private int _id;
    private Vector2 _defaultPosition;

    private float _lerpDuration = 0.5f;


    public int CompareTo(SortItem other)
    {
        return _id.CompareTo(other._id);
    }

    private void Awake()
    {
        _id = int.Parse(GetComponentInChildren<Text>().text);
        _defaultPosition = GetComponent<RectTransform>().anchoredPosition;
    }

    public void Reset()
    {
        GetComponent<RectTransform>().anchoredPosition = _defaultPosition;
    }

    public void MoveTo(Vector2 targetPosition)
    {
        StartCoroutine(LerpTo(targetPosition));
    }

    private IEnumerator LerpTo(Vector2 targetPosition)
    {
        _isLerping = true;

        float time = 0;
        Vector2 startPosition = gameObject.GetComponent<RectTransform>().anchoredPosition;
        while (time < _lerpDuration)
        {
            gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(startPosition, targetPosition, time / _lerpDuration);
            time += Time.deltaTime;
            yield return null;
        }
        gameObject.GetComponent<RectTransform>().anchoredPosition = targetPosition;

        _isLerping = false;
    }

    public void SetOutline(bool enabled)
    {
        GetComponent<Outline>().enabled = enabled;
    }

    public void SetOutlineColor(Color color)
    {
        GetComponent<Outline>().effectColor = color;
    }
}