using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class SortingPuzzleManager : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _agent;

    [SerializeField] private GameObject _menuPanel;
    private MenuPanelManager _menuPanelManager;

    [SerializeField] private GameObject _textBox;
    private TextBoxLogic _textBoxLogic;

    [SerializeField] private GameObject _book;
    [SerializeField] private GameObject _bubbleSortBookButton;

    private int _stepsNeeded = 0;
    private int _stepsOptimal = 21;

    [SerializeField] private Text _stepsNeededText;
    [SerializeField] private Text _stepsOptimalText;

    [SerializeField] private List<SortItem> _sortItems = new List<SortItem>();
    [SerializeField] private List<Text> _itemILabels = new List<Text>();

    private List<SortItem> _correctItems = new List<SortItem>();
    private List<SortItem> _defaultItems = new List<SortItem>();

    private SortItem _currentItem1;
    private SortItem _currentItem2;

    private Color _defaultColor = new Color32(0x32, 0x32, 0x32, 0xFF);

    private Button[] _buttons;

    // bookshelves in FP room
    [SerializeField] private GameObject[] _shelves;

    private void Start()
    {
        _defaultItems.AddRange(_sortItems);

        _correctItems.AddRange(_sortItems);
        _correctItems.Sort();

        _currentItem1 = _sortItems[0];
        _currentItem2 = _sortItems[1];

        _currentItem1.SetOutline(true);
        _currentItem2.SetOutline(true);

        _itemILabels[0].color = Color.yellow;
        _itemILabels[1].color = Color.yellow;

        _buttons = this.GetComponentsInChildren<Button>();

        _stepsNeededText.text = _stepsNeeded.ToString();
        _stepsOptimalText.text = _stepsOptimal.ToString();

        _stepsNeededText.color = Color.green;
    }

    private void OnEnable()
    {
        _menuPanelManager = _menuPanel.GetComponent<MenuPanelManager>();
        _menuPanelManager.SetMenuPanelActive(false);

        _textBoxLogic = _textBox.GetComponent<TextBoxLogic>();

        _agent.isStopped = true;

        if (PlayerPrefs.GetInt("BubbleSortBookRead").Equals(1))
        {
            if (_textBoxLogic.Texts.ContainsKey("sortingPuzzle*03b")) // Check if Collider exists for Texts
            {
                _textBoxLogic.setText(_textBoxLogic.Texts["sortingPuzzle*03b"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "sortingPuzzle*03b");

            _bubbleSortBookButton.SetActive(true);
        }
        else
        {
            if (_textBoxLogic.Texts.ContainsKey("sortingPuzzle*03a")) // Check if Collider exists for Texts
            {
                _textBoxLogic.setText(_textBoxLogic.Texts["sortingPuzzle*03a"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "sortingPuzzle*03a");

            _bubbleSortBookButton.SetActive(false);
        }
    }

    private void OnDisable()
    {
        _menuPanelManager.SetMenuPanelActive(true);

        _agent.isStopped = false;
    }

    public void NextItems()
    {
        AudioManager.instance.Play("Click");

        _stepsNeeded++;
        _stepsNeededText.text = _stepsNeeded.ToString();

        if (_stepsNeeded > _stepsOptimal)
        {
            _stepsNeededText.color = Color.red;
        }

        if (_sortItems.SequenceEqual(_correctItems))
        {
            // Debug.Log("List sorted!");
            StartCoroutine(ProcessCorrectSolution());
            return;
        }

        _currentItem1.SetOutline(false);
        _currentItem2.SetOutline(false);

        _itemILabels[_sortItems.IndexOf(_currentItem1)].color = _defaultColor;
        _itemILabels[_sortItems.IndexOf(_currentItem2)].color = _defaultColor;

        if (_sortItems.IndexOf(_currentItem2) == _sortItems.Count - 1)
        {
            _currentItem1 = _sortItems[0];
            _currentItem2 = _sortItems[1];
        }
        else
        {
            _currentItem1 = _sortItems[_sortItems.IndexOf(_currentItem2)];
            _currentItem2 = _sortItems[_sortItems.IndexOf(_currentItem1) + 1];
        }

        _currentItem1.SetOutline(true);
        _currentItem2.SetOutline(true);

        _itemILabels[_sortItems.IndexOf(_currentItem1)].color = Color.yellow;
        _itemILabels[_sortItems.IndexOf(_currentItem2)].color = Color.yellow;
    }

    public void SwapItems()
    {
        StartCoroutine(BlockButtons(true));

        Vector2 tempPos = _currentItem1.GetComponent<RectTransform>().anchoredPosition;
        Vector2 tempPos2 = _currentItem2.GetComponent<RectTransform>().anchoredPosition;

        _currentItem2.MoveTo(tempPos);
        _currentItem1.MoveTo(tempPos2);

        SortItem tempItem = _currentItem1;
        _currentItem1 = _currentItem2;
        _currentItem2 = tempItem;

        _sortItems[_sortItems.IndexOf(_currentItem1)] = _currentItem2;
        _sortItems[_sortItems.IndexOf(tempItem)] = _currentItem1;

        NextItems();
    }

    private IEnumerator ProcessCorrectSolution()
    {
        while (!_sortItems.All(item => item.isLerping == false)) { yield return null; }
        // prevents race condition
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(BlockButtons(false));

        AudioManager.instance.Play("correct");
        PlayerPrefs.SetInt("SortalgorithmFixed", 1);

        // change sprites of bookshelves to sorted
        foreach (GameObject shelf in _shelves)
        {
            shelf.GetComponent<SpriteChanger>().ChangeSprite(sorted: true);
        }

        // do some fancy animation
        float duration = 0.25f;

        _itemILabels[_sortItems.IndexOf(_currentItem1)].color = _defaultColor;
        _itemILabels[_sortItems.IndexOf(_currentItem2)].color = _defaultColor;

        foreach (SortItem item in _sortItems)
        {
            item.SetOutline(false);
            item.SetOutlineColor(Color.green);
        }

        int iterations = 2;
        for (int i = 0; i < iterations; i++)
        {
            foreach (SortItem item in _sortItems)
            {
                item.SetOutline(true);
                yield return new WaitForSeconds(duration);
                item.SetOutline(false);
            }
            if (i < iterations - 1)
            {
                duration -= 0.1f;
            }
        }

        iterations = 4;
        for (int i = 0; i < iterations; i++)
        {
            foreach (SortItem item in _sortItems)
            {
                item.SetOutline(true);
            }
            yield return new WaitForSeconds(duration);

            if (i < iterations - 1)
            {
                foreach (SortItem item in _sortItems)
                {
                    item.SetOutline(false);
                }
            }
            yield return new WaitForSeconds(duration);
        }

        if (_textBoxLogic.Texts.ContainsKey("sortingPuzzle*04")) // Check if Collider exists for Texts
        {
            _textBoxLogic.setText(_textBoxLogic.Texts["sortingPuzzle*04"]);
        }
        else
            Debug.Log("Collider not defined. Collider is: " + "sortingPuzzle*04");

        GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Suche die Antivirendatei!");
    }

    private IEnumerator BlockButtons(bool reenable)
    {
        foreach (Button button in _buttons) { button.interactable = false; }

        // prevents race condition
        yield return new WaitForSeconds(0.1f);

        while (!_sortItems.All(item => item.isLerping == false)) { yield return null; }

        if (reenable)
        {
            foreach (Button button in _buttons) { button.interactable = true; }
        }
    }

    private void Reset()
    {
        // Reset to initial state
        _currentItem1.SetOutline(false);
        _currentItem2.SetOutline(false);

        _itemILabels[_sortItems.IndexOf(_currentItem1)].color = _defaultColor;
        _itemILabels[_sortItems.IndexOf(_currentItem2)].color = _defaultColor;

        _sortItems.Clear();
        _sortItems.AddRange(_defaultItems);

        foreach (SortItem item in _sortItems) { item.Reset(); }

        _currentItem1 = _sortItems[0];
        _currentItem2 = _sortItems[1];

        _currentItem1.SetOutline(true);
        _currentItem2.SetOutline(true);

        _itemILabels[0].color = Color.yellow;
        _itemILabels[1].color = Color.yellow;

        _stepsNeeded = 0;

        _stepsNeededText.text = _stepsNeeded.ToString();
        _stepsNeededText.color = Color.green;
    }

    public void ButtonClickedSwapYes() => SwapItems();

    public void ButtonClickedSwapNo() => NextItems();

    public void ButtonClickedReset() => Reset();

    public void ButtonClickedBack()
    {
        gameObject.SetActive(false);
    }

    public void ButtonClickedBubbleSortBook()
    {
        _book.GetComponentInChildren<BookManager>().openBook("BubbleSort");
        Instantiate(_book);
    }
}