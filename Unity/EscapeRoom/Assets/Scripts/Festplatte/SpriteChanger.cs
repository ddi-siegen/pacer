using UnityEngine;

public class SpriteChanger : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    [SerializeField] private Sprite _spriteUnsorted;
    [SerializeField] private Sprite _spriteSorted;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();

        _spriteRenderer.sprite = PlayerPrefs.GetInt("SortalgorithmFixed") == 1 ? _spriteSorted : _spriteUnsorted;
    }

    public void ChangeSprite(bool sorted)
    {
        _spriteRenderer.sprite = sorted ? _spriteSorted : _spriteUnsorted;
    }
}