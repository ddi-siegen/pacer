using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class AgentScript : abstractCharacterClass
{

    //[SerializeField] Transform target;

    public NavMeshAgent playerAgent;

    Animator animator;

    RaycastHit2D hit;

    Vector3 mousePosition;

    [HideInInspector]
    public string myCollider;

    public LevelManager LevelManager;

    public bool movementPossible = true;

    //symbol to display where player clicked
    public GameObject mouseSymbol;
    public Animator mouseAnimator;

    int currentSceneIndex;
    Scene currentScene;
    Vector3 clickedMousePos;

    // Start is called before the first frame update
    void Start()
    {
        FixRoatation2D();
        animator = GetComponent<Animator>();

        // Create a temporary reference to the current scene.
        currentScene = SceneManager.GetActiveScene();

        // Retrieve the index of this scene.
        currentSceneIndex = currentScene.buildIndex;

    }

    // Update is called once per frame
    void Update()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //can be used for objects for passing it doesn't

        if (Input.GetMouseButtonDown(0))
        {
            hit = Physics2D.Raycast(mousePosition, Vector2.zero);
            clickedMousePos = mousePosition;
            // Check if the mouse was clicked over a UI element
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //Debug.Log("Clicked on the UI");

            }
            else if (playerAgent.isStopped == false)
            {
                // mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                //display mouseSymbol in order to see where the Player clicked
                mouseSymbol.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);

                //triggers the click animation
                mouseAnimator.SetTrigger("Active");

                //call move function
                Move();

                if (hit.collider != null)
                {
                    //assign the chosen myCollider (by mouseclick) to the variable
                    myCollider = hit.collider.gameObject.name;

                    //Debug.Log("Collider by click: " + myCollider);
                }
                else
                {
                    //Debug.Log("No myCollider");
                }
            }
        }

        Vector3 robotPos = new Vector3(transform.position.x, transform.position.y, 0);
        Vector3 clickMouseVector = new Vector3(clickedMousePos.x, clickedMousePos.y, 0);
        float distanceToClick = Vector3.Distance(robotPos, clickMouseVector);
        if (distanceToClick < 0.3)
        {
            stopAtCurrentPosition();
        }
        if (myCollider != null && distanceToClick < 2.5f && hit.collider != null && !playerAgent.isStopped)
        {
            if (Vector3.Distance(robotPos, hit.collider.gameObject.transform.position) < 3f) // prevent bug abuse (clicks on object and then next to player to trigger event)
            {
                //Debug.Log("Target Position: " + hit.collider.gameObject.transform.position);
                try
                {
                    LevelManager.AgentCollision(myCollider);
                }
                catch (NullReferenceException)
                {
                   // Debug.Log("NullReferenceException: myCollider = " + myCollider);
                }
            }
        }

        //stand
        if (playerAgent.velocity.x == 0)
        {
            animator.SetInteger("Direction", 0);
        }
        //if moved to left
        else if (playerAgent.velocity.x < 0)
        {
            animator.SetInteger("Direction", 1);
            playerAgent.GetComponent<SpriteRenderer>().flipX = true;
        }
        //if moved to right
        else if (playerAgent.velocity.x > 0)
        {
            animator.SetInteger("Direction", 2);
            playerAgent.GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    //fixes rotation, see NavMesh Documentation
    void FixRoatation2D()
    {
        playerAgent.updateRotation = false;
        playerAgent.updateUpAxis = false;
    }
    //moves player to desired destination
    private void Move()
    {
        if (Input.GetMouseButtonDown(0))
        {
            playerAgent.SetDestination(mousePosition + new Vector3(0, 1.2f, 0));
        }
    }

    public void stopAtCurrentPosition()
    {
        Vector3 robotPos = new Vector3(transform.position.x, transform.position.y, 0);
        playerAgent.SetDestination(new Vector3(robotPos.x, robotPos.y, 0));
    }

    public override void teleport(Vector3 vector)
    {
        Vector3 offset = new Vector3(-6.3867f, -2.85f, 0f); // we need an offset for the targetet position, because of the navmesh coordinates
        switch (PlayerPrefs.GetInt("currentScene")) // each scene got its own offset because navmeshs are different
        {
            case (0): offset = new Vector3(6.38670444f, 2.85048246f, 0f); break; // CPU
            case (1): offset = new Vector3(6.38670444f, 2.85048246f, 0f); break;  // Storage
            case (2): offset = new Vector3(-6.38f, -2.85f, 0f); break;  // Elevator
            case (3): offset = new Vector3(-6.38f, -2.85f, 0f); break; // HeadOffice
        }
        playerAgent.enabled = false;
        playerAgent.transform.position = vector + offset; // offset navmesh in headoffice
        // each scene got a different offset so maybe we need to check for current scene later
        playerAgent.enabled = true;
    }

    public override void walkToPosition(Vector3 vector)
    {
        Vector3 offset = new Vector3(-6.3867f, -2.85f, 0f); // we need an offset for the targetet position, because of the navmesh coordinates
        switch (PlayerPrefs.GetInt("currentScene")) // each scene got its own offset because navmeshs are different
        {
            case (0): offset = new Vector3(6.38670444f, 2.85048246f, 0f); break; // CPU
            case (1): offset = new Vector3(6.38670444f, 2.85048246f, 0f); break;  // Storage
            case (2): offset = new Vector3(9.41585445f,4.54008532f,46.6930923f); break;  // Elevator
            case (3): offset = new Vector3(-6.38f, -2.85f, 0f); break; // HeadOffice
        }
        playerAgent.SetDestination(vector + offset);
    }
}