using UnityEngine.Audio;
using UnityEngine;
using System;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    //sound array
    public Sound[] sounds;

    //background Sound List
    private List<string> backgroundSounds;

    public static AudioManager instance;

    private string currentBackgroundMusic = "soundtrack_01";
    private bool isBackgroundMusicPlaying = true;

    void Awake()
    {
        //destroy manager if two in game
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        //so that audiomanager gets dragged into new scene, solve looping music problem
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

        //add all Sounds to backgroundSounds where isBackgroundSound is checked
        backgroundSounds = new List<string>();
        foreach (Sound s in sounds)
        {
            if (s.isBackgroundSound)
            {
                backgroundSounds.Add(s.name);
            }
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        Play("music");
    }

    public void Play(string name)
    {
        // find in sounds array, find where sound's name is equal to name, stored in variable s
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Audiomanager -> Sound: " + name + " not found!");
            return;
        }
        s.source.Play();
    }

    public void Stop(string name)
    {
        // find in sounds array, find where sound's name is equal to name, stored in variable s
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Audiomanager -> Sound: " + name + " not found!");
            return;
        }
        s.source.Stop();
    }

    public void StopAll()
    {
        //stop all sounds
        foreach (Sound s in sounds)
        {
            s.source.Stop();
        }
        isBackgroundMusicPlaying = false;
    }


    public void SetVolume(string name, float volume)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Audiomanager -> Sound: " + name + " not found!");
            return;
        }
        else
        {
            Debug.Log("Name" + name + " Volume: " + volume);
            s.source.volume = volume;
        }
    }

    public void SetBackgroundSoundsMuted(bool muted)
    {
        foreach (String name in backgroundSounds)
        {
            Sound s = Array.Find(sounds, Sound => Sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Audiomanager -> Sound: " + name + " not found!");
                return;
            }
            s.source.volume = muted ? 0f : 1f;
        }
    }

    public void SetMusicMuted(bool muted)
    {
        SetVolume("music", muted ? 0f : 0.33f);
    }

    public void SetMusic(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Audiomanager -> Sound: " + name + " not found!");
            return;
        }
        else
        {
            Stop("music");
            Sound music = Array.Find(sounds, Sound => Sound.name == "music");
            music.source.clip = s.clip;
            music.source.pitch = s.pitch;
            music.source.loop = s.loop;
            Play("music");
            currentBackgroundMusic = name;
            isBackgroundMusicPlaying = true;
            print("music in background set to: " + isBackgroundMusicPlaying);
        }
    }

    public string getCurrentBackgroundMusic()
    {
        return currentBackgroundMusic;
    }
    public bool getIsBackgroundMusicPlaying()
    {
        return isBackgroundMusicPlaying;
    }
}
