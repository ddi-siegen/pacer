using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RawImageScroller: MonoBehaviour
{
    [SerializeField] private Vector2 moveSpeed;
    [SerializeField] private RawImage img;


    void Update()
    {
        img.uvRect = new Rect(img.uvRect.x + moveSpeed.x * Time.deltaTime, img.uvRect.y + moveSpeed.y * Time.deltaTime, img.uvRect.width, img.uvRect.height);
    }

    public void setSpeed(Vector2 speed)
    {
        moveSpeed = speed;
    }
}
