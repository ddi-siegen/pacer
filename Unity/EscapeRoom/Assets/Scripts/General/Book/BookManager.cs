using UnityEngine.UI;
using UnityEngine;
using UnityEngine.AI;


public class BookManager : MonoBehaviour
{
    [SerializeField] Sprite[] uBlocklyBook;

    [SerializeField] Sprite[] BubbleSortBook;

    [SerializeField] Sprite[] DatenschutzBook;

    [SerializeField] Sprite[] BinaereSucheBook;

    [SerializeField] Sprite[] Lizenzen;

    [SerializeField] Sprite[] Netiquette;

    [SerializeField] GameObject NextPageButton, PrevPageButton, CurrentPage, CloseButton, parentGameObject;

    public Sprite[] currentBook;

    [SerializeField] private bool agentWasStoppedBefore = false;

    private int currentIndex = 0;

    public void openBook(string bookName)
    {
        switch (bookName)
        {
            case "uBlockly":
                currentBook = uBlocklyBook;
                currentIndex = 0;
                CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
                break;
            case "BubbleSort":
                currentBook = BubbleSortBook;
                currentIndex = 0;
                CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
                break;
            case "Datenschutz":
                currentBook = DatenschutzBook;
                currentIndex = 0;
                CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
                break;
            case "BinaereSuche":
                currentBook = BinaereSucheBook;
                currentIndex = 0;
                CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
                break;
            case "Lizenzen":
                currentBook = Lizenzen;
                currentIndex = 0;
                CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
                break;
            case "Netiquette":
                currentBook = Netiquette;
                currentIndex = 0;
                CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
                break;
                
            default:
                break;
        }
        activate(true);
    }

    public void activate(bool boolean)
    {
        agentWasStoppedBefore = GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped;
        GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = true;
        NextPageButton.SetActive(boolean);
        PrevPageButton.SetActive(boolean);
        CurrentPage.SetActive(boolean);
        CloseButton.SetActive(boolean);
        checkButtons();
    }

    public void closeBook()
    {
        if (!agentWasStoppedBefore) // only reactivate robot if he was not stopped already before the book got opened
        {
            GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = false;
        }
        Destroy(parentGameObject);
    }

    public void NextPage()
    {
        if (currentIndex < currentBook.Length - 1)
        {
            currentIndex++;
            CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
            AudioManager.instance.Play("buch_blättern");
        }
        checkButtons();
    }
    public void PrevPage()
    {
        if (currentIndex > 0)
        {
            currentIndex--;
            CurrentPage.GetComponent<Image>().sprite = currentBook[currentIndex];
            AudioManager.instance.Play("buch_blättern");
        }
        checkButtons();
    }
    public void checkButtons() // hide Buttons if there are no prev or next Pages
    {
        if (currentIndex == 0)
        {
            PrevPageButton.SetActive(false);
        }
        else
        {
            PrevPageButton.SetActive(true);
        }
        if (currentIndex == currentBook.Length - 1)
        {
            NextPageButton.SetActive(false);
        }
        else
        {
            NextPageButton.SetActive(true);
        }
    }
}


