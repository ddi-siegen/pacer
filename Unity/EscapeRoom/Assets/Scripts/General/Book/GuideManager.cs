using UnityEngine.UI;
using UnityEngine;
using UnityEngine.AI;

public class GuideManager : MonoBehaviour
{
    [SerializeField] GameObject[] guideBook;

    [SerializeField] GameObject NextPageButton, PrevPageButton, CloseButton, parentGameObject;

    private bool agentWasStoppedBefore = false;

    private int currentIndex = 0;

    public void openGuide()
    {
        currentIndex = 0;
        activate(true);
    }

    public void activate(bool boolean)
    {
        agentWasStoppedBefore = GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped;
        GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = true;
        NextPageButton.SetActive(boolean);
        PrevPageButton.SetActive(boolean);
        CloseButton.SetActive(boolean);
        checkButtons();
    }

    public void closeGuide()
    {
        if (!agentWasStoppedBefore) // only reactivate robot if he was not stopped already before the book got opened
        {
            GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = false;
        }
        Destroy(parentGameObject);
    }

    public void NextPage()
    {
        if (currentIndex < guideBook.Length - 1)
        {
            currentIndex++;
            hideAllPages();
            guideBook[currentIndex].SetActive(true);
            AudioManager.instance.Play("buch_blättern");
        }
        checkButtons();
    }
    public void PrevPage()
    {
        if (currentIndex > 0)
        {
            currentIndex--;
            hideAllPages();
            guideBook[currentIndex].SetActive(true);
            AudioManager.instance.Play("buch_blättern");
        }
        checkButtons();
    }

    public void goToPage(int page)
    {
        currentIndex = page;
        hideAllPages();
        guideBook[currentIndex].SetActive(true);
        AudioManager.instance.Play("buch_blättern");
        checkButtons();
    }

    public void hideAllPages()
    {
        for (int i = 0; i < guideBook.Length; i++)
        {
            guideBook[i].SetActive(false);
            // reactivate all Spoiler blockers
            foreach (Transform child in guideBook[i].transform)
            {
                child.gameObject.SetActive(true);
            }
        }
    }

    public void checkButtons() // hide Buttons if there are no prev or next Pages
    {
        if (currentIndex == 0)
        {
            PrevPageButton.SetActive(false);
        }
        else
        {
            PrevPageButton.SetActive(true);
        }
        if (currentIndex == guideBook.Length - 1)
        {
            NextPageButton.SetActive(false);
        }
        else
        {
            NextPageButton.SetActive(true);
        }
    }
}