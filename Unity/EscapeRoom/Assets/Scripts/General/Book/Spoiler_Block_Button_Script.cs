using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spoiler_Block_Button_Script : MonoBehaviour
{   
    public void setUpBUtton(Vector3 position, int height, int weight) // 600 weigt, 130 height is fine
    {
        gameObject.transform.position = position;
        gameObject.transform.localScale = new Vector3(weight, height, 1);
    }

    public void removeButton()
    {
        gameObject.SetActive(false);
    }

}
