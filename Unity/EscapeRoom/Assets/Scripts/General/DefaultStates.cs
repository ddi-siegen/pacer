using UnityEngine;
using System.Collections.Generic;
public class DefaultStates
{
    private bool resetEveryTime = false; // if this is true the states will reset to default everytime we start the game
    private Dictionary<string, int> defaultPrefs = new Dictionary<string, int>();
    private Dictionary<string, string> defaultPrefsStrings = new Dictionary<string, string>();
    public DefaultStates() // sets everything to Default
    {   
        defaultPrefs.Clear();
        
        // Characternames (default)
        defaultPrefsStrings.Add("characterNameDog", "Hundi"); 
        defaultPrefsStrings.Add("characterNameRobot", "Robi");
        defaultPrefsStrings.Add("currentNotification", "");

        defaultPrefsStrings.Add("currDogScene", "HO"); // Scene where the Dog is at the moment

        // For Elevator 
        defaultPrefs.Add("previousScene"                 , 2);  // because we start in Headoffice
        defaultPrefs.Add("currentScene"                  , 2);  // because we start in Headoffice

        //For Settings
        defaultPrefs.Add("SoundMuted", 0);   // | 0 = sound is on | 1 = sound off |
        defaultPrefs.Add("MusicMuted", 0);   // | 0 = music is on | 1 = music off |

        // Default Message from Doggo in MessageApp
        defaultPrefsStrings.Add("MessageStorage", "{\"Allmessages\":[{\"content\":\"15:32:22 PM: Das war ne super Party gestern. Hoffentlich hat keiner Fotos gemacht ;)\",\"isNew\":false,\"isEditable\":false,\"icon\":\"doggo\"}]}");   // | 0 = music is on | 1 = music off |


        defaultPrefs.Add("InventoryNotification", 0);// | 0 = no notification | 1 = notification |

        defaultPrefs.Add("SmartphoneButtonNotification", 0);// | 0 = no notification | 1 = notification |
        defaultPrefs.Add("SmartphoneNotesNotification", 0);// | 0 = no notification | 1 = notification |

        // From Mattias
        // Interpretation of Values
        defaultPrefs.Add("ChapterOneActive"             , 0);   // | 0 = no | 1 = yes | set to 1 to skip first conversation in HO at the beginning of the game
        defaultPrefs.Add("AlreadyPlayed"                , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("EnteredElevatorFirst"         , 0);   // | 0 = no | 1 = yes | only 0 if player didnt visit elevator at all
        defaultPrefs.Add("ElevatorPanelActive"          , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SmartphoneTaken"              , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("HatchClicked"                 , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("WirePuzzleInProgress"         , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("ScrewdriverTaken"             , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("PasswordPosterClicked"        , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("PasswordFileClicked"          , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SSIDToNotes"                  , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("EmbarrassingPosterClicked"    , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("WiringFilePrinted"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("PrinterPWCorrect"             , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("LiftCablesCorrect"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("LiftQuestionAnswered"         , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("ElevatorPanelBlockedAgain"    , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("VentilationFixed"             , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("LiftQuestionTwoAnswered"      , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("creditCardTaken"              , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("PasswordScreenViewed"         , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("talkedToAsr"                  , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("BuildingPlanFound"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("BinaryPosterFound"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("LiftStatesCorrect"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("cpuComputerLoggedIn"          , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("correctOnlineShopSelected"    , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("TalkedToCpuBoss_1"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("PlugClicked"                  , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("HologramNoticeReceived"       , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("REDpwTaken"                   , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("QuizMachineUnlocked"          , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("CreditCardCodeCorrect"        , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("MotoroilDrunken"              , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SsdDepartmentOrdered"         , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("MotoroilReceived"             , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("ColleaguesSpiedOn"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("ArUsed"                       , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("CpuBossPasswordInserted"      , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("PlugInserted"                 , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SystemRestarted"              , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("ChapterTwoActive"             , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("NoMoreDrawer"                 , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("TalkedToCpuBoss_2"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("TalkedToCore"                 , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SsdPossible"                  , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SsdFirstTime"                 , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("IsCoreServed"                 , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("TalkedToWorkerThree"          , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SearchedForAntivirusprogram"  , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("SortalgorithmFixed"           , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("NetworkRiddleReceived"        , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("CoresInIdle"                  , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("CorrectNumberDialed"          , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("ChapterThreeActive"           , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("AirVentOpenable"              , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("DoggoOpenable"                , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("DoggoChipInserted"            , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("DoggoProgramDone"             , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("DoggoRemotePossible"          , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("DoggoRemote"                  , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("AirVentCleaned"               , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("PCPuzzleSolved"               , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("BubbleSortBookRead"           , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("ReprimandedByAsr"             , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("BinarySearchBookRead"         , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("AntiVirusBookTaken"           , 0);   // | 0 = no | 1 = yes |
        defaultPrefs.Add("quizMachineMoney"             , 1);   // | starts with 7 for testing -> needs to be changed to 0 for final game


        if (resetEveryTime)
        {
            Debug.Log("States are now back to default Values because boolean resetEveryTime is true!");
            setToDefault(); // set them to default
        }
    }
    public void setToDefault()
    {
        PlayerPrefs.DeleteAll();
        foreach (KeyValuePair<string, int> entry in defaultPrefs)
        {
            PlayerPrefs.SetInt(entry.Key, entry.Value);
        }
        foreach (KeyValuePair<string, string> entry in defaultPrefsStrings)
        {
            PlayerPrefs.SetString(entry.Key, entry.Value);
        }

    }
    public void printDefaultPrefs()
    {
        string log = "PlayerPrefs already initialized. Here are the Values (click on this log and see all values at the bottom): \n\n";
        foreach (KeyValuePair<string, int> entry in defaultPrefs)
        {
            log += entry.Key + " = " + PlayerPrefs.GetInt(entry.Key) + "\n";
        }
        foreach (KeyValuePair<string, string> entry in defaultPrefsStrings)
        {
            log += entry.Key + " = " + PlayerPrefs.GetString(entry.Key) + "\n";
        }
        Debug.Log(log);
    }

}