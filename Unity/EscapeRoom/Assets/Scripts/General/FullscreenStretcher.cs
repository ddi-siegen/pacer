using UnityEngine;
using UnityEngine.UI;

public class FullscreenStretcher : MonoBehaviour
{
    void Start()
    {
        Vector3 maxPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        float worldSpaceWidth = maxPoint.x * 2;
        float worldSpaceHeight = maxPoint.y * 2;

        Vector3 spriteBoundsSize = gameObject.GetComponent<Image>().sprite.bounds.size;

        float scaleFactorX = worldSpaceWidth / spriteBoundsSize.x;
        float scaleFactorY = worldSpaceHeight / spriteBoundsSize.y;

        gameObject.transform.localScale = new Vector3(scaleFactorX, scaleFactorY, 1);
    }
}