using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelChanger : MonoBehaviour
{
    public Animator animator;
    private int levelToLoad;

    public void Start()
    {
    }

    public void FadeToLevel(int levelIndex)
    {
        //play sound from audiomanager if there is one in the scene
        try
        {
            AudioManager.instance.Play("Raumwechsel");
        }
        catch (System.Exception e)
        {
            Debug.Log("Couldnt find AudioManager; warning: " + e);
        }
        //Debug.Log("Button pressed loading level: " + levelIndex);
        levelToLoad = levelIndex;
        //triggers the Fade Out parameter in unity animator
        animator.SetTrigger("FadeOut");
        saveCurrenScene(levelIndex);
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    public void instantlyGoToLevel(int levelIndex)
    {
        saveCurrenScene(levelIndex);
        SceneManager.LoadScene(levelIndex);
    }

    public void saveCurrenScene(int levelIndex)
    {
        if (levelIndex != 4) // only save if it is not the startingscreen
        {
            PlayerPrefs.SetInt("currentScene", levelIndex);
        }
    }
    public void goToLevelWithElevatorAnimation (int level)
    {
        try 
        { 
            FindObjectOfType<Elevator_Buttons>(true).GoToLevel(level); 
        }
        catch (Exception)
        {
            Debug.Log("Cant find Elevator_Buttons GameObject");
        }
        
    }
}
