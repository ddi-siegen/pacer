using UnityEngine;

public abstract class LevelManager : MonoBehaviour // abstract class for our LevelManagers for each room. So we dont need to define all Levelmanager on each Robot.
{
    public abstract void AgentCollision(string collider);
}