using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


public class InventoryManager : MonoBehaviour
{
    [Header("General")]
    public Transform background;
    private GameObject robot;
    private NavMeshAgent agent;

    [SerializeField] GameObject TextFieldImage_newItem, TextFieldText_newItem;
    public bool inventoryOpen = false;

    [Header("Button Sprites")]
    //change button colour
    public Sprite clickedButtonImage;
    public Sprite notifyButtonImage;
    public Sprite regularButtonImage;
    public Button inventoryButton;

    [Header("Background")]
    //inventory sprites
    public GameObject inventoryBackground;

    [Header("Items")]
    public Dictionary<string, Item> Items = new Dictionary<string, Item>();

    [Header("Icons CreditCard")]
    //Icons
    public Sprite creditCardIcon;
    public GameObject creditCardSlot;

    [Header("Icons ScrewDriver")]
    public Sprite screwDriverIcon;
    public GameObject screwDriverSlot;

    [Header("Icons PDF")]
    public Sprite PDFIcon;
    public GameObject PDFSlot;

    [Header("Icons Gebauedeplan")]
    public Sprite buildingPlanIcon;
    public GameObject buildingPlanSlot;
    public GameObject buildingPlanZoom;

    [Header("Icons trash")]
    public Sprite binaryPosterIcon;
    public GameObject binaryPosterSlot;
    public GameObject binaryPosterZoom;

    float shakeDuration = 1f;
    float shakeMagnitude = 4f;

    [Header("Settings")]
    public GameObject settings;
    private SettingsManager settingsManager;

    [Header("MenuPanel")]
    public GameObject menuPanel;
    private MenuPanelManager menuPanelManager;

    [Header("Smartphone")]
    private GameObject smartphone;
    public GameObject smartphoneZoomedIn;
    public Button smartphoneButton;

    [Header("PDF")]
    public GameObject PDFZoom;

    [SerializeField] private GameObject bgBlocker;

    public GameObject InventoryCloseButton;

    private bool agentWasStoppedBefore = false;

    private int isInAnimation = 0;
    private float fillAmount = 0;
    private float animationSpeed = 3f;
    private float fullNotificationDuration = 5f;


    // Start is called before the first frame update
    void Start()
    {
        //find gameobjects without the need to drag them into the inscpector
        if (SceneManager.GetActiveScene().name != "StartScreen")
        {
            robot = GameObject.Find("/Character/Robot");
            agent = robot.GetComponent(typeof(NavMeshAgent)) as NavMeshAgent;
        }

        if (PlayerPrefs.GetInt("InventoryNotification").Equals(1))
        {
            inventoryButton.image.sprite = notifyButtonImage;
        }

        settingsManager = settings.GetComponent<SettingsManager>();
        menuPanelManager = menuPanel.GetComponent<MenuPanelManager>();

        AddToDictionary("smartphone", new string[] { "" });
        AddToDictionary("tool_case", new string[] { "" });
        AddToDictionary("pdf", new string[] { "" });
        AddToDictionary("creditCard", new string[] { "" });
        AddToDictionary("buildingPlan", new string[] { "" });
        AddToDictionary("trash", new string[] { "" });


        //find smartphone in list of objects, needs to be placed at this exact position
        smartphone = GameObject.Find("/Interior/smartphone");

        RefreshInventory();
    }

    // Update is called once per frame
    void Update()
    {
       if (isInAnimation == 1) // animation for full notication 
        {
            TextFieldImage_newItem.SetActive(true);
            fillAmount += animationSpeed;
            if (fillAmount > 100)
            {
                fillAmount = 100;
                TextFieldText_newItem.gameObject.SetActive(true);
                isInAnimation = 0;
            }
            TextFieldImage_newItem.GetComponent<Image>().fillAmount = fillAmount / 100f;
        }
        else if (isInAnimation == 2)
        {
            TextFieldText_newItem.gameObject.SetActive(false);
            fillAmount -= animationSpeed;
            if (fillAmount < 0)
            {
                fillAmount = 0;
                isInAnimation = 0;
            }
            TextFieldImage_newItem.GetComponent<Image>().fillAmount = fillAmount / 100f;
        }
    }

    public void RefreshInventory()
    {
        try
        {
            if (PlayerPrefs.GetInt("creditCardTaken").Equals(1))
            {
                creditCardSlot.SetActive(true);
            }
            else if (PlayerPrefs.GetInt("creditCardTaken") == 0)
            {
                creditCardSlot.SetActive(false);
            }

            if (PlayerPrefs.GetInt("WiringFilePrinted").Equals(1) && PlayerPrefs.GetInt("LiftCablesCorrect").Equals(0))
            {
                PDFSlot.SetActive(true);
            }
            else
            {
                PDFSlot.SetActive(false);
            }

            if (PlayerPrefs.GetInt("ScrewdriverTaken").Equals(1) && PlayerPrefs.GetInt("WirePuzzleInProgress").Equals(0))
            {
                screwDriverSlot.SetActive(true);
            }
            else
            {
                screwDriverSlot.SetActive(false);
            }

            if (PlayerPrefs.GetInt("BuildingPlanFound").Equals(1) && PlayerPrefs.GetInt("LiftStatesCorrect").Equals(0))
            {
                buildingPlanSlot.SetActive(true);
            }
            else
            {
                buildingPlanSlot.SetActive(false);
            }

            if (PlayerPrefs.GetInt("BinaryPosterFound").Equals(1))
            {
                binaryPosterSlot.SetActive(true);
            }
            else if (PlayerPrefs.GetInt("BinaryPosterFound") == 0)
            {
                binaryPosterSlot.SetActive(false);
            }

            if (PlayerPrefs.GetInt("SmartphoneTaken").Equals(1))
            {
                smartphoneButton.gameObject.SetActive(true);
                smartphone.SetActive(false);
            }
            else if (PlayerPrefs.GetInt("SmartphoneTaken") == 0)
            {
                smartphoneButton.gameObject.SetActive(false);
            }

        }
        catch (System.NullReferenceException)
        {
            Debug.Log("Item not in Scene, Player Prefs Smartphone: " + PlayerPrefs.GetInt("SmartphoneTaken") + "Player Prefs Screwdriver: " + PlayerPrefs.GetInt("ScrewdriverTaken") + "Player Prefs PDF Inventory: " + PlayerPrefs.GetInt("WiringFilePrinted") + "Player Prefs CreditCard Inventory: " + PlayerPrefs.GetInt("creditCardTaken"));
        }
    }

    private void AddToDictionary(string key, string[] neededStates) // Add Text to Dictionary 
    {
        Items.Add(key, new Item(key, neededStates));
    }

    public void FillInventory(string key)
    {
        //inventar slot mit zugeh�rigen funktionen bef�llen
        //wenn bestimmter Zustand erf�llt ist
        //einmal inventory awake
        //if else

        if (key == "smartphone")
        {
            //make Smartphone Sprite not visible
            smartphone.SetActive(false);
            //activate Smartphone Button in MenuPanel
            smartphoneButton.gameObject.SetActive(true);

            //smartphone picked up
            PlayerPrefs.SetInt("SmartphoneTaken", 1);
            GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Repariere den Drucker! Hierzu benötigt dieser Wlan.");

            //play Pick Up Sound ggf vor if setzen beim n�chsten Mal
            AudioManager.instance.Play("Collect");

#if UNITY_ANDROID || UNITY_IPHONE
                Handheld.Vibrate();
#endif
        }
        //add screw driver to inventory after hatch in elevator clicked and tool box clicked afterwards
        if (key == "tool_case_done" && PlayerPrefs.GetInt("ScrewdriverTaken").Equals(0))
        {
            //play shaking
            Play();
            //change Inventory Icon
            inventoryButton.image.sprite = notifyButtonImage;
            PlayerPrefs.SetInt(("InventoryNotification"), 1);

            Debug.Log("ScrewDriver put into Inventory");
            screwDriverSlot.SetActive(true);
            showNewItemTextNotification("Schraubenzieher");
            //screwDriver picked up
            PlayerPrefs.SetInt("ScrewdriverTaken", 1);

            //play Pick Up Sound ggf vor if setzen beim n�chsten Mal
            AudioManager.instance.Play("Collect");

#if UNITY_ANDROID || UNITY_IPHONE
                Handheld.Vibrate();
#endif
        }

        if (key == "pdf")
        {
            //play shaking
            Play();
            //change Inventory Icon
            inventoryButton.image.sprite = notifyButtonImage;
            PlayerPrefs.SetInt(("InventoryNotification"), 1);

            PDFSlot.SetActive(true);
            showNewItemTextNotification("PDF-Ausdruck");

            //play Pick Up Sound ggf vor if setzen beim n�chsten Mal
            AudioManager.instance.Play("Collect");
#if UNITY_ANDROID || UNITY_IPHONE
                Handheld.Vibrate();
#endif
        }

        if (key == "buildingPlan")
        {
            //play shaking
            Play();
            //change Inventory Icon
            inventoryButton.image.sprite = notifyButtonImage;
            PlayerPrefs.SetInt(("InventoryNotification"), 1);
            buildingPlanSlot.SetActive(true);
            showNewItemTextNotification("Gebaeudeplan");

            PlayerPrefs.SetInt("BuildingPlanFound", 1);

            //play Pick Up Sound ggf vor if setzen beim n�chsten Mal
            AudioManager.instance.Play("Collect");
#if UNITY_ANDROID || UNITY_IPHONE
                Handheld.Vibrate();
#endif
        }

        if (key == "trash")
        {
            //play shaking
            Play();
            //change Inventory Icon
            inventoryButton.image.sprite = notifyButtonImage;
            PlayerPrefs.SetInt(("InventoryNotification"), 1);

            PlayerPrefs.SetInt("BinaryPosterFound", 1);
            binaryPosterSlot.SetActive(true);
            showNewItemTextNotification("Poster");

            //play Pick Up Sound ggf vor if setzen beim n�chsten Mal
            AudioManager.instance.Play("Collect");
#if UNITY_ANDROID || UNITY_IPHONE
                Handheld.Vibrate();
#endif
        }

        if (key == "creditCard")
        {
            //play shaking
            Play();
            //change Inventory Icon
            inventoryButton.image.sprite = notifyButtonImage;
            PlayerPrefs.SetInt(("InventoryNotification"), 1);
            creditCardSlot.SetActive(true);
            showNewItemTextNotification("Kreditkarte");

            PlayerPrefs.SetInt("creditCardTaken", 1);

            //play Pick Up Sound ggf vor if setzen beim n�chsten Mal
            AudioManager.instance.Play("Collect");
#if UNITY_ANDROID || UNITY_IPHONE
                Handheld.Vibrate();
#endif
        }
    }

    //Notify with tinted Inventory Icon
    public void InventoryNotification()
    {
        //play shaking
        Play();
        //change Inventory Icon
        inventoryButton.image.sprite = notifyButtonImage;
        PlayerPrefs.SetInt(("InventoryNotification"), 1);

        //play Pick Up Sound ggf vor if setzen beim n�chsten Mal
        AudioManager.instance.Play("Collect");
#if UNITY_ANDROID || UNITY_IPHONE
            Handheld.Vibrate();
#endif
    }


    public void ScrewDriverSlotClicked()
    {
        AudioManager.instance.Play("Maus");
        if (PlayerPrefs.GetInt("currentScene").Equals(3))
        {
            //disable Hatch U1 if opened
            if (GameObject.Find("/UI/Hatch/HatchUI1").activeInHierarchy == true)
            {
                GameObject.Find("/UI/Hatch/HatchUI1").SetActive(false);

                InventoryButton();

                FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(false);
                agent.isStopped = true;

                //as long as verkabelungs pdf not clicked print button, continue to show this textbox
                if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("hatchPuzzle*01")) // Check if Collider exists for Texts
                {
                    FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["hatchPuzzle*01"]);
                }
                else
                    Debug.Log("Collider not defined. Collider is: " + "hatchPuzzle*01");
                // once hatch is open -> puzzle is in progress (skips the screwdriver interaction on subsequent tries)
                PlayerPrefs.SetInt("WirePuzzleInProgress", 1);
                RefreshInventory();
                //activate HatchUI2 and puzzle within
                GameObject.Find("/UI/Hatch/HatchUI2").SetActive(true);
            }
        }
    }

    public void PDFSlotClicked()
    {
        AudioManager.instance.Play("Maus");
        bgBlocker.SetActive(true);
        PDFZoom.SetActive(true);
    }

    public void BuildingPlanSlotClicked()
    {
        AudioManager.instance.Play("Maus");
        bgBlocker.SetActive(true);
        buildingPlanZoom.SetActive(true);
    }

    public void BinaryPosterSlotClicked()
    {
        AudioManager.instance.Play("Maus");
        bgBlocker.SetActive(true);
        binaryPosterZoom.SetActive(true);
    }

    public void CloseZoom()
    {
        AudioManager.instance.Play("Maus");
        bgBlocker.SetActive(false);
        buildingPlanZoom.SetActive(false);
        binaryPosterZoom.SetActive(false);
        PDFZoom.SetActive(false);
    }

    public void InventoryButton()
    {
        AudioManager.instance.Play("Maus");
        if (settingsManager.settingsOpen == false)
        {
            if (inventoryOpen == false)
            {
                InventoryCloseButton.SetActive(true);
                agentWasStoppedBefore = agent.isStopped;
                agent.isStopped = true;
                inventoryButton.image.sprite = clickedButtonImage;
                inventoryBackground.SetActive(true);
                inventoryOpen = true;
                AudioManager.instance.Play("Maus");

            }
            else if (inventoryOpen == true)
            {
                inventoryOpen = false;
                InventoryCloseButton.SetActive(false);
                inventoryButton.image.sprite = regularButtonImage;
                PlayerPrefs.SetInt(("InventoryNotification"), 0);
                inventoryBackground.SetActive(false);
                //if PDF opened make it unactive
                CloseZoom();
                if (!agentWasStoppedBefore) // only reactivate agent if he was not already stopped before inventory
                {
                    agent.isStopped = false;
                }
                GameObject.Find("Robot").GetComponent<AgentScript>().stopAtCurrentPosition();
                AudioManager.instance.Play("Maus");
            }
        }
    }

    public void ChangeButtonImage()
    {
        inventoryButton.image.sprite = clickedButtonImage;
    }

    //method for shaking the inventory button
    public void Play()
    {
        StartCoroutine(Shake());
    }

    IEnumerator Shake()
    {
        float elapsedTime = 0;
        Vector3 initialPosition = inventoryButton.transform.position;

        while (elapsedTime < shakeDuration)
        {
            inventoryButton.transform.position = initialPosition + (Vector3)Random.insideUnitCircle * shakeMagnitude;
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        inventoryButton.transform.position = initialPosition;
    }

    public void showNewItemTextNotification(string text)
    {
        StartCoroutine(showNewItemTextCoroutine());
        TextFieldText_newItem.GetComponent<TextMeshProUGUI>().text = text + " wurde aufgenommen!";
    }

    public IEnumerator showNewItemTextCoroutine()
    {
        isInAnimation = 1;
        yield return new WaitForSecondsRealtime(fullNotificationDuration);
        isInAnimation = 2;
    }

    private void OnDestroy()
    {
        StopCoroutine(showNewItemTextCoroutine());
    }
}
