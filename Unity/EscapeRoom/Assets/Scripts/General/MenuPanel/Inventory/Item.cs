using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Item
{
  
    public string key;
    public string[] neededStates;



    public Item(string key, string[] neededStates)
    {
        this.key = key;
        this.neededStates = neededStates;
       
    }
  
}
