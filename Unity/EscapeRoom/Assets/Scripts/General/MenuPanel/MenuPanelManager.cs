using UnityEngine;
using UnityEngine.UI;

public class MenuPanelManager : MonoBehaviour
{
    [SerializeField] private GameObject notification;

    [SerializeField] private Image menuPanelRenderer;
    [SerializeField] private Image inventoryBackground;
    [SerializeField] private Image smartphoneNotificationIcon;
    [SerializeField] private Button guideButton;
    [SerializeField] private Button inventoryButton;
    [SerializeField] private Button settingsButton;
    [SerializeField] private Button smartphoneButton;
    [SerializeField] private Sprite regularButtonImage;

    [SerializeField] private GameObject guide;


    public bool isActive { get; private set; } = false;

    public void OpenGuide()
    {
        guide.GetComponentInChildren<GuideManager>().openGuide();
        Instantiate(guide);
    }

    //isActive = true --> activate MenuPanel | isActive = false --> deactivate MenuPanel
    public void SetMenuPanelActive(bool isActive)
    {
        guideButton.gameObject.SetActive(isActive);
        inventoryButton.gameObject.SetActive(isActive);
        menuPanelRenderer.enabled = isActive;
        settingsButton.gameObject.SetActive(isActive);
        smartphoneButton.gameObject.SetActive((PlayerPrefs.GetInt("SmartphoneTaken") == 1) && isActive);
        smartphoneNotificationIcon.GetComponent<Image>().enabled = (PlayerPrefs.GetInt("SmartphoneTaken") == 1) && isActive;
        notification.GetComponent<Notification_Script>().display(isActive);

        //notification.GetComponent<Notification_Script>().activateNotification(isActive);
        this.isActive = isActive;
    }

    //close inventory background & smartphone close up if openend
    public void CloseInventoryBackground()
    {
        inventoryBackground.enabled = false;
        //change Sprite, otherwise Button would be blue
        inventoryButton.image.sprite = regularButtonImage;
        PlayerPrefs.SetInt(("InventoryNotification"), 0);
    }
}
