using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class musicApp : MonoBehaviour
{
    [SerializeField] GameObject musicElemPrefab, musicGrid;
    [SerializeField] TextMeshProUGUI currentPlayingSongTitle;
    private QuizManager quizManager;
    private AudioManager audioManager;
    [SerializeField] GameObject musicPlayer_Playbutton, musicPlayer_Pausebutton;
    [SerializeField] RawImageScroller muicVisualizerAnimator;
    private List<GameObject> songButtons;

    void Start()
    {
        quizManager = new QuizManager();
        audioManager = FindObjectOfType<AudioManager>();
        songButtons = new List<GameObject>();
        muicVisualizerAnimator.setSpeed(new Vector2(0.1f, 0.0f));

        addSongButtons();
    }

    public void initMusicPlayerButtons()
    {
        print("initMusicPlayer");
        audioManager = FindObjectOfType<AudioManager>();
        if (audioManager.getIsBackgroundMusicPlaying())
        {
            musicPlayer_Playbutton.SetActive(false);
            musicPlayer_Pausebutton.SetActive(true);
            currentPlayingSongTitle.text = audioManager.getCurrentBackgroundMusic();
            musicPlayer_Pausebutton.GetComponent<Button>().onClick.AddListener(() => { pauseSong(audioManager.getCurrentBackgroundMusic()); });
            muicVisualizerAnimator.setSpeed(new Vector2(0.1f, 0.0f));
        }
        else
        {
            musicPlayer_Playbutton.SetActive(true);
            musicPlayer_Pausebutton.SetActive(false);
            musicPlayer_Playbutton.GetComponent<Button>().onClick.AddListener(() => { playSong(audioManager.getCurrentBackgroundMusic()); });
            currentPlayingSongTitle.text = "Song pausiert";
            muicVisualizerAnimator.setSpeed(new Vector2(0.0f, 0.0f));
        }
    }

    /// <summary>Adds all songs which contain "soundtrack_0" + X from audioManager to a grid.</summary>
    private void addSongButtons()
    {
        foreach (Sound song in audioManager.sounds)
        {
            if (song.name.Contains("soundtrack_0")) //only display pacer_soundtracks and filter out other sounds
            {
                if (isSongPaid(song.name) || song.name.Contains("soundtrack_01"))
                {
                    addSongToPlayerPrefs(song.name); //adds only if not already exists
                    GameObject musicButton = (GameObject)Instantiate(musicElemPrefab, musicGrid.transform); // instantiate a button for each song and set parent to grid
                    musicApp_musicButton script = musicButton.transform.Find("musicButtonScript").GetComponent<musicApp_musicButton>();
                    script.initButton(song.name, musicApp_musicButton.State.Pause, this);
                    songButtons.Add(musicButton);
                }
            }
        }
    }

    /// <summary>Adds a Song to PlayerPrefs, if its not already exists and store isPaid status.</summary>
    private void addSongToPlayerPrefs(string songName)
    {
        if (!PlayerPrefs.HasKey("PlayerPref_" + songName + "_isPaid")) // only add, if not already exists
        {
            PlayerPrefs.SetInt("PlayerPref_" + songName + "_isPaid", 0);
        }
    }

    /// <summary>Returns true if the given song is paid already.</summary>
    public Boolean isSongPaid(string songName)
    {
        return (PlayerPrefs.GetInt("PlayerPref_" + songName + "_isPaid") == 1);
    }

    /// <summary> Destroys every GameObject inside the given List. Clears List afterwards.</summary>
    private void destroyAllGameObjectsFromList(List<GameObject> list)
    {
        foreach (GameObject gameObject in list)
        {
            Destroy(gameObject);
        }
        list.Clear();
    }

    /// <summary> Play Song by name, change current active song, hide play button & display pause button.</summary>
    public void playSong(string songName, musicApp_musicButton script = null)
    {
        //also assign correct functions to pause and play button under the musicPlayer
        musicPlayer_Playbutton.GetComponent<Button>().onClick.RemoveAllListeners(); // remove all listeners from play button
        musicPlayer_Playbutton.GetComponent<Button>().onClick.AddListener(() => { playSong(songName, script); });
        musicPlayer_Pausebutton.GetComponent<Button>().onClick.RemoveAllListeners(); // remove all listeners from play button
        musicPlayer_Pausebutton.GetComponent<Button>().onClick.AddListener(() => { pauseSong(songName, script); });
        musicPlayer_Pausebutton.SetActive(true);
        musicPlayer_Playbutton.SetActive(false);

        foreach (GameObject button in songButtons) // set all other playing songs to paused
        {
            musicApp_musicButton tempButtonScript = button.transform.Find("musicButtonScript").GetComponent<musicApp_musicButton>();
            if (tempButtonScript.currState == musicApp_musicButton.State.Playing)
            {
                tempButtonScript.changeState(musicApp_musicButton.State.Pause);
            }
        }
        audioManager.SetMusic(songName); // only plying and not setting as default background music, because we want to be able to pause the song
        if (script != null)
        {
            script.changeState(musicApp_musicButton.State.Playing);
        }
        currentPlayingSongTitle.text = songName;
        muicVisualizerAnimator.setSpeed(new Vector2(0.1f, 0.0f));
    }



    /// <summary>Pause the song of this songButton.</summary>
    public void pauseSong(string songName, musicApp_musicButton script = null)
    {
        musicPlayer_Pausebutton.SetActive(false);
        musicPlayer_Playbutton.SetActive(true);
        currentPlayingSongTitle.text = "Song pausiert";
        musicPlayer_Playbutton.GetComponent<Button>().onClick.AddListener(() => { playSong(songName); });
        audioManager.StopAll();
        if (script != null)
        {
            script.changeState(musicApp_musicButton.State.Pause);
        }
        muicVisualizerAnimator.setSpeed(new Vector2(0.0f, 0.0f));
    }

    /// <summary>Close the MusicApp/summary>
    public void closeMusicApp()
    {
        print("Close MusicApp");
        gameObject.transform.parent.gameObject.SetActive(false);
    }
}