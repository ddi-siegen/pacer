using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;


public class musicApp_musicButton : MonoBehaviour
{
    [SerializeField] GameObject musicElem, playButton, pauseButton, buyButton;
    [SerializeField] TextMeshProUGUI songTitle, costText;
    [SerializeField] GameObject costIcon;
    private musicApp musicApp;
    public enum State
    {
        Playing, // currently playing this song - show pause button
        Pause, // currently paused - show play button
        Buyable // not bought yet - show buy button
    }
    public State currState;
    public string songName;

    /// <summary> Init Title and Cost of the Songbutton. Show Buy, Play or Pause Button depending on State. </summary>
    public void initButton(string title, State state, musicApp musicApp)
    {
        this.musicApp = musicApp;
        songTitle.text = title;
        currState = state;
        songName = title;
        switch (state)
        {
            case State.Playing:
                playButton.SetActive(false);
                pauseButton.SetActive(true);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { musicApp.pauseSong(title, this); }); // ad onclick-function to each button
                break;
            case State.Pause:
                playButton.SetActive(true);
                pauseButton.SetActive(false);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { musicApp.playSong(title, this); }); // ad onclick-function to each button
                break;
        }
    }

    /// <summary>Update Show Buy, Play or Pause Button depending on State. </summary>
    public void changeState(State state)
    {
        currState = state;
        switch (state)
        {
            case State.Playing:
                playButton.SetActive(false);
                pauseButton.SetActive(true);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { musicApp.pauseSong(songName, this); }); // ad onclick-function to each button
                break;
            case State.Pause:
                playButton.SetActive(true);
                pauseButton.SetActive(false);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { musicApp.playSong(songName, this); }); // ad onclick-function to each button
                break;
        }
    }
}
