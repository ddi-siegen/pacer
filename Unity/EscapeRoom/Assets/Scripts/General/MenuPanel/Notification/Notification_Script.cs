using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class Notification_Script : MonoBehaviour
{
    [Header("Notification")]
    [SerializeField] GameObject noitification, notification_backgroundImage, icon;
    [SerializeField] TextMeshProUGUI notification_text;
    // Start is called before the first frame update
    private Dictionary<string, string> currentTasks = new Dictionary<string, string>();

    private string currText;
    private int isInAnimation = 0;
    private float fillAmount = 0;
    private float animationSpeed = 3f;
    private float fullNotificationDuration = 4f;
    private AgentScript agentscript;

    void Start()
    {
        agentscript = GameObject.Find("Robot").GetComponent<AgentScript>();
        currText = PlayerPrefs.GetString("currentNotification");
        if (currText != "")
        {
            icon.SetActive(true);
        }
        else
        {
            activateNotification(false); // deactive notification at start
        }
    }

    public void display(bool show)
    {
        notification_backgroundImage.SetActive(show);
        icon.SetActive(show);
        if (show && notification_backgroundImage.GetComponent<Image>().fillAmount > 1) // we only wanto to activate text, after it gets trigerred by click or event
        {
            notification_text.gameObject.SetActive(show);
        }
    }

    void Update()
    {
        if (isInAnimation == 1) // animation for full notication 
        {
            fillAmount += animationSpeed;
            if (fillAmount > 100)
            {
                fillAmount = 100;
                notification_text.gameObject.SetActive(true);
                isInAnimation = 0;
            }
            notification_backgroundImage.GetComponent<Image>().fillAmount = fillAmount / 100f;
        }
        else if (isInAnimation == 2)
        {
            notification_text.gameObject.SetActive(false);
            fillAmount -= animationSpeed;
            if (fillAmount < 0)
            {
                fillAmount = 0;
                isInAnimation = 0;
            }
            notification_backgroundImage.GetComponent<Image>().fillAmount = fillAmount / 100f;
        }
    }

    public void activateNotification(bool boolean)
    {
        if (!boolean)
        {
            notification_text.gameObject.SetActive(boolean);
            notification_backgroundImage.SetActive(boolean);
            icon.SetActive(boolean);
        }
        else if (currText != "")
        {
            icon.SetActive(boolean);
        }
        {
            icon.SetActive(boolean);
        }
    }

    public void triggerByIconClick()
    {
        print("triggered");
        changeText(PlayerPrefs.GetString("currentNotification"));
        agentscript.stopAtCurrentPosition(); // prevent agent fron walking
    }

    public void changeText(string text)
    {
        PlayerPrefs.SetString("currentNotification", text);
        activateNotification(true);
        notification_text.text = text;
        currText = text;
        showFullNotification();

    }
    public void showFullNotification()
    {
        if (isInAnimation == 0)
        {
            notification_backgroundImage.SetActive(true);
            StartCoroutine(showFullNotificationWithDelay());
        }
    }
    public IEnumerator showFullNotificationWithDelay()
    {
        isInAnimation = 1;
        yield return new WaitForSecondsRealtime(fullNotificationDuration);
        isInAnimation = 2;
    }
    private void OnDestroy()
    {
        StopCoroutine(showFullNotificationWithDelay());
    }
}
