using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    [Header("General")]
    public bool settingsOpen = false;
    private GameObject robot;
    private NavMeshAgent agent;
    public GameObject exitGameScreen;

    [Header("MenuPanel Button Sprites")]
    //change button colour
    public Sprite clickedButtonImage;
    public Sprite regularButtonImage;
    public Button settingsButton;

    [Header("Settings Buttons")]
    public Button soundButton;
    public Button musicButton;
    public Sprite soundOnSprite;
    public Sprite soundOffSprite;
    public Sprite musicOnSprite;
    public Sprite musicOffSprite;

    [Header("Background")]
    public GameObject settingsBackground;
    public GameObject Close_Button;

    [Header("Inventory")]
    public GameObject inventory;
    private InventoryManager inventoryManager;

    [Header("MenuPanel")]
    public GameObject menuPanel;
    private MenuPanelManager menuPanelManager;

    private bool agentWasStoppedBefore = false;

    void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        // there is no agent/robot in StartScreen scene
        if (SceneManager.GetActiveScene().name != "StartScreen")
        {
            //find gameobjects without the need to drag them into the inscpector
            robot = GameObject.Find("/Character/Robot");
            agent = robot.GetComponent(typeof(NavMeshAgent)) as NavMeshAgent;
        }

        inventoryManager = inventory.GetComponent<InventoryManager>();
        menuPanelManager = menuPanel.GetComponent<MenuPanelManager>();

        if (PlayerPrefs.GetInt("SoundMuted").Equals(1))
        {
            soundButton.image.sprite = soundOffSprite;
            AudioManager.instance.SetBackgroundSoundsMuted(muted: true);
        }

        if (PlayerPrefs.GetInt("MusicMuted").Equals(1))
        {
            musicButton.image.sprite = musicOffSprite;
            AudioManager.instance.SetMusicMuted(muted: true);
        }
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void SettingsButton()
    {

        if (inventoryManager.inventoryOpen == false)
        {
            if (settingsOpen == false)
            {
                agentWasStoppedBefore = agent.isStopped;
                agent.isStopped = true;
                settingsButton.image.sprite = clickedButtonImage;
                settingsBackground.SetActive(true);
                Close_Button.SetActive(true);
                settingsOpen = true;
                AudioManager.instance.Play("Click");

            }
            else if (settingsOpen == true)
            {
                settingsOpen = false;
                settingsButton.image.sprite = regularButtonImage;
                settingsBackground.SetActive(false);
                Close_Button.SetActive(false);
                if (!agentWasStoppedBefore) // only reactivate agent if he was not already stopped before settings
                {
                    agent.isStopped = false;
                }
                AudioManager.instance.Play("Click");
                GameObject.Find("Robot").GetComponent<AgentScript>().stopAtCurrentPosition();
            }
        }
    }

    public void SettingsClose()
    {
        settingsOpen = false;
        settingsButton.image.sprite = regularButtonImage;
        settingsBackground.SetActive(false);
        Close_Button.SetActive(false);
        if (!agentWasStoppedBefore) // only reactivate agent if he was not already stopped before settings
        {
            agent.isStopped = false;
        }
        AudioManager.instance.Play("Click");
        GameObject.Find("Robot").GetComponent<AgentScript>().stopAtCurrentPosition();
    }

    public void SoundButton()
    {
        AudioManager.instance.Play("Maus");
        if (PlayerPrefs.GetInt("SoundMuted").Equals(0))
        {
            soundButton.image.sprite = soundOffSprite;
            PlayerPrefs.SetInt("SoundMuted", 1);
            AudioManager.instance.SetBackgroundSoundsMuted(muted: true);
        }
        else
        {
            soundButton.image.sprite = soundOnSprite;
            PlayerPrefs.SetInt("SoundMuted", 0);
            AudioManager.instance.SetBackgroundSoundsMuted(muted: false);
        }
    }

    public void MusicButton()
    {
        AudioManager.instance.Play("Maus");
        if (PlayerPrefs.GetInt("MusicMuted").Equals(0))
        {
            musicButton.image.sprite = musicOffSprite;
            PlayerPrefs.SetInt("MusicMuted", 1);
            AudioManager.instance.SetMusicMuted(muted: true);
        }
        else
        {
            musicButton.image.sprite = musicOnSprite;
            PlayerPrefs.SetInt("MusicMuted", 0);
            AudioManager.instance.SetMusicMuted(muted: false);
        }
    }

    public void ExitButton()
    {
        AudioManager.instance.Play("Maus");
        exitGameScreen.SetActive(true);
        //disable menuPanel when small window open
        menuPanelManager.SetMenuPanelActive(false);
    }

    //exit game yes
    public void YesButton()
    {
        AudioManager.instance.Play("Maus");
        //go to Startscreen again
        FindObjectOfType<LevelChanger>().FadeToLevel(4);
    }

    //exit game no
    public void NoButton()
    {
        AudioManager.instance.Play("Maus");
        exitGameScreen.SetActive(false);
        //enable menuPanel when small window is closed again
        menuPanelManager.SetMenuPanelActive(true);
    }
}

