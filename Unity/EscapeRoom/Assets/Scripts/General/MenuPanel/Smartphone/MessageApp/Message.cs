using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Message : MonoBehaviour
{

    public GameObject message;
    public GameObject messagesParent;
    [SerializeField] Sprite defaultIcon, doggoIcon, Icon_Robot, Icon_Security, Icon_Smartphone, Icon_Reception, Icon_Facility, Icon_Speaker;

    private List<GameObject> ListWithmessageObjects = new List<GameObject>();

    private MessageManager MessageManager;
    private void Awake()
    {
        MessageManager = new MessageManager();
    }

    private void Start()
    {
        updateAll();
    }
    public void updateAll() // remove all messages and then insert them again so index keeps correct  
    {
        foreach (GameObject child in ListWithmessageObjects)
        {
            GameObject.Destroy(child.gameObject);
        }
        ListWithmessageObjects.Clear();
        //addEmptymessage(); // oneEmptymessage so it wont appear totally empty
        MessageClass[] currentmessages = MessageManager.getmessages().ToArray();

        for (int i = 0; i < currentmessages.Length; i++) // first only insert new/unread messages, so the unread always appear ontop
        {
            if (currentmessages[i].isNew)
            {
                 AddNewmessage(i, currentmessages[i].content, currentmessages[i].isNew, currentmessages[i].isEditable, true, currentmessages[i].icon); // need solution to also safe if message was already read
            }
        }
                for (int i = 0; i < currentmessages.Length; i++) // then all other messages appear
        {
            if (!currentmessages[i].isNew)
            {
                 AddNewmessage(i, currentmessages[i].content, currentmessages[i].isNew, currentmessages[i].isEditable, true, currentmessages[i].icon); // need solution to also safe if message was already read
            }
        }
    }

    public void addEmptymessage()
    {
        int number = MessageManager.getmessages().Count;
        AddNewmessage(number);
    }
    //adds a new message field into the app
    public void AddNewmessage(int number, string content = "",  bool isNew = false, bool isEditable = true, bool init = false, String charName = "")
    {
        Sprite icon = defaultIcon;
        GameObject duplicate = Instantiate(message);
        duplicate.SetActive(true);
        //the duplicate is now the child of messages Parent
        duplicate.transform.SetParent(messagesParent.transform);
        //scale it properly
        duplicate.gameObject.transform.localScale = new Vector3(1, 1, 1);

        duplicate.transform.Find("Text").GetComponent<Text>().text = content;
        Image IconImage = duplicate.transform.Find("Character_Icon").GetComponent<Image>();
        switch (charName)
        {
            case "":
                icon = defaultIcon;
                break;
            case "defaultIcon":
                icon = defaultIcon;
                break;
            case "doggo":
                icon = doggoIcon;
                break;
            default:
                icon = defaultIcon;
                break;

        }
        IconImage.sprite = icon;
        duplicate.transform.Find("InputField").GetComponent<InputField>().onValueChanged.AddListener((data) => { UpdateInputField(data, number); });

        if (!isEditable)
        {
            duplicate.transform.Find("InputField").GetComponent<InputField>().interactable = false;
            duplicate.transform.Find("InputField").Find("DeleteButton").gameObject.SetActive(false);
        }
        else
        {
            duplicate.transform.Find("InputField").Find("DeleteButton").GetComponent<Button>().onClick.AddListener(() => { Destroy(duplicate); deleteAndReload(number); });
        }

        if (!init)
        {
            MessageManager.newmessage(content, isNew, isEditable, charName);
        }

        ListWithmessageObjects.Add(duplicate);
    }

    public void deleteAndReload(int index)
    {
        MessageManager.removemessage(index);
        updateAll();
    }

    void UpdateInputField(string content, int index)
    {
        MessageManager.changemessage(index, content);
    }
}

