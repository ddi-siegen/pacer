using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MessageClass
{
    public string content;
    public bool isNew;
    public bool isEditable;
    public string icon;

    public MessageClass( string content, bool isNew, bool isEditable, string icon)
    {
        this.content =  System.DateTime.Now.ToString("HH:mm:ss tt") + ": " + content;
        this.isNew = isNew;
        this.isEditable = isEditable;
        this.icon = icon;
    }
}

[System.Serializable]
public class MessageStorage
{
    public List<MessageClass> Allmessages = new List<MessageClass>();
}


public class MessageManager
{
    MessageStorage MessageStorage = new MessageStorage();

    void LoadSavedData()
    {
        if (PlayerPrefs.HasKey("MessageStorage"))
        {
            string saveJason = PlayerPrefs.GetString("MessageStorage");
            MessageStorage = JsonUtility.FromJson<MessageStorage>(saveJason);
        }
        else
        {
            Debug.Log("no saved Data");
        }
    }
    void SaveData()
    {
        string saveJason = JsonUtility.ToJson(MessageStorage);
        PlayerPrefs.SetString("MessageStorage", saveJason);
        PlayerPrefs.Save();
    }

    public void newmessage(string content, bool isNew, bool isEditable, string icon)
    {
        LoadSavedData();
        MessageStorage.Allmessages.Add(new MessageClass(content, isNew, isEditable, icon));
        SaveData();
    }
    public List<MessageClass> getmessages()
    {
        LoadSavedData();
        return MessageStorage.Allmessages;
    }
    public void removemessage (int index)
    {
        LoadSavedData();
        MessageStorage.Allmessages.RemoveAt(index);
        SaveData();
    }
    public void changemessage(int index, string content, bool isNew = false, bool isEditable = true, string charName = "")
    {
        LoadSavedData();
        MessageStorage.Allmessages[index] = new MessageClass(content, isNew, isEditable, charName);
        SaveData();
    }

    public void markAllmessagesAsRead()
    {
        LoadSavedData();
        foreach (MessageClass message in MessageStorage.Allmessages)
        {
            message.isNew = false;
        }
        SaveData();
    }
}
