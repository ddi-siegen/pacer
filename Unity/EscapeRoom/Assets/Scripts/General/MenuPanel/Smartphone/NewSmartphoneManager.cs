using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NewSmartphoneManager : MonoBehaviour
{
    [Header("General")]
    private bool smartphomeZoom = false;
    private GameObject robot;
    private NavMeshAgent agent;

    [Header("Smartphone Button Sprites")]
    //change button colour
    public Sprite clickedButtonImage;
    public Sprite regularButtonImage;
    public Button smartphoneButton;
    public GameObject notfiySmartphoneImage;
    public GameObject notifyNotesImage;
    public GameObject messageAppButton;

    [Header("Smartphone Zoom")]
    public GameObject smartphoneZoomImage;

    [Header("Apps")]
    public GameObject NotesApp, MessageApp, MusicApp;

    private bool agentWasStoppedBefore = false;

    // Start is called before the first frame update
    void Start()
    {
        // there is no agent/robot in StartScreen scene
        if (SceneManager.GetActiveScene().name != "StartScreen")
        {
            //find gameobjects without the need to drag them into the inscpector
            robot = GameObject.Find("/Character/Robot");
            agent = robot.GetComponent(typeof(NavMeshAgent)) as NavMeshAgent;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //activate notification on smartphone button if player pref is set to 1
        if (PlayerPrefs.GetInt("SmartphoneButtonNotification").Equals(1))
        {
            notfiySmartphoneImage.SetActive(true);
        }

        //activate notification on smartphone notes app if player pref is set to 1
        if (PlayerPrefs.GetInt("SmartphoneNotesNotification").Equals(1))
        {
            notifyNotesImage.SetActive(true);
        }
    }

    public void SmartphoneButton()
    {
        if (smartphomeZoom == false)
        {
            agentWasStoppedBefore = agent.isStopped;
            agent.isStopped = true;
            smartphoneButton.image.sprite = clickedButtonImage;
            smartphoneZoomImage.SetActive(true);
            smartphomeZoom = true;
            notfiySmartphoneImage.SetActive(false);
            PlayerPrefs.SetInt("SmartphoneButtonNotification", 0);
            AudioManager.instance.Play("Click");

            // check if we are in the Festplatte scene and we were not reprimanded by the Asr before
            if (SceneManager.GetActiveScene().name == "Festplatte" && PlayerPrefs.GetInt("ReprimandedByAsr").Equals(0))
            {
                GameObject.Find("/UI/Canvas/AngryAsrOverlay").SetActive(true);

                if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("angryAsr*01")) // Check if Collider exists for Texts
                {
                    FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["angryAsr*01"]);
                }
                else
                    Debug.Log("Collider not defined. Collider is: " + "angryAsr*01");

                PlayerPrefs.SetInt("ReprimandedByAsr", 1);
            }
        }
        else if (smartphomeZoom == true)
        {
            smartphomeZoom = false;
            smartphoneButton.image.sprite = regularButtonImage;
            smartphoneZoomImage.SetActive(false);
            if (!agentWasStoppedBefore) // only reactivate agent if he was not already stopped before smartphone
            {
                agent.isStopped = false;
            }
            AudioManager.instance.Play("Click");

        }
    }

    public void SmartphoneZoomClose()
    {
        smartphomeZoom = false;
        smartphoneButton.image.sprite = regularButtonImage;
        smartphoneZoomImage.SetActive(false);
        if (!agentWasStoppedBefore) // only reactivate agent if he was not already stopped before smartphone
        {
            agent.isStopped = false;
        }
        AudioManager.instance.Play("Click");
        GameObject.Find("Robot").GetComponent<AgentScript>().stopAtCurrentPosition();
    }

    public void OpenNotesApp()
    {
        FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(false);
        NotesApp.SetActive(true);
        messageAppButton.SetActive(false);
        NotesApp.GetComponent<Notes>().GetComponent<Notes>().updateAll();
        notifyNotesImage.SetActive(false);
        PlayerPrefs.SetInt("SmartphoneNotesNotification", 0);
    }

    public void CloseNotesApp()
    {
        NoteManager noteManager = new NoteManager();
        noteManager.markAllNotesAsRead();
        messageAppButton.SetActive(true);
        NotesApp.SetActive(false);
        FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(true);
    }

    public void OpenMusicApp()
    {
        MusicApp.SetActive(true);
        MusicApp.GetComponentInChildren<musicApp>().initMusicPlayerButtons();
    }

    public void NotifyNewNote()
    {
        #if UNITY_ANDROID || UNITY_IPHONE
                    Handheld.Vibrate();
                    Handheld.Vibrate();
        #endif

        notfiySmartphoneImage.SetActive(true);
        PlayerPrefs.SetInt("SmartphoneButtonNotification", 1);

        notifyNotesImage.SetActive(true);
        PlayerPrefs.SetInt("SmartphoneNotesNotification", 1);
    }

       public void OpenMessageApp()
    {
        MessageApp.SetActive(true);
        MessageApp.GetComponent<Message>().GetComponent<Message>().updateAll();
        FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(false);
        //notifyNotesImage.SetActive(false);
        //PlayerPrefs.SetInt("SmartphoneNotesNotification", 0);
    }
      public void CloseMessageApp()
    {
        MessageManager messageManager = new MessageManager();
        messageManager.markAllmessagesAsRead();
        MessageApp.SetActive(false);
        FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(true);
    }

}
