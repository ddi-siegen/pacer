﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoteClass
{
    public string content;
    public bool isNew;
    public bool isEditable;

    public NoteClass( string content, bool isNew, bool isEditable)
    {
        this.content = content;
        this.isNew = isNew;
        this.isEditable = isEditable;
    }
}

[System.Serializable]
public class NoteStorage
{
    public List<NoteClass> AllNotes = new List<NoteClass>();
}


public class NoteManager
{
    NoteStorage noteStorage = new NoteStorage();

    void LoadSavedData()
    {
        if (PlayerPrefs.HasKey("NoteStorage"))
        {
            string saveJason = PlayerPrefs.GetString("NoteStorage");
            noteStorage = JsonUtility.FromJson<NoteStorage>(saveJason);
        }
        else
        {
            Debug.Log("no saved Data");
        }
    }
    void SaveData()
    {
        string saveJason = JsonUtility.ToJson(noteStorage);
        PlayerPrefs.SetString("NoteStorage", saveJason);
        PlayerPrefs.Save();
    }

    public void newNote(string content, bool isNew, bool isEditable)
    {
        LoadSavedData();
        noteStorage.AllNotes.Add(new NoteClass(content, isNew, isEditable));
        SaveData();
    }
    public List<NoteClass> getNotes()
    {
        LoadSavedData();
        return noteStorage.AllNotes;
    }
    public void removeNote (int index)
    {
        LoadSavedData();
        noteStorage.AllNotes.RemoveAt(index);
        SaveData();
    }
    public void changeNote(int index, string content, bool isNew = false, bool isEditable = true)
    {
        LoadSavedData();
        noteStorage.AllNotes[index] = new NoteClass(content, isNew, isEditable);
        SaveData();
    }

    public void markAllNotesAsRead()
    {
        LoadSavedData();
        foreach (NoteClass note in noteStorage.AllNotes)
        {
            note.isNew = false;
        }
        SaveData();
    }
}
