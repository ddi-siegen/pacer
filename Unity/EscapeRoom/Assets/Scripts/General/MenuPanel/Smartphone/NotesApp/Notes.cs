using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Notes : MonoBehaviour
{

    public GameObject note;
    public GameObject notesParent;
    [SerializeField] Sprite noteReadCircle;
    [SerializeField] Sprite noteNotReadCircle;
    private List<GameObject> ListWithNoteObjects = new List<GameObject>();

    private NoteManager noteManager;
    private void Awake()
    {
        noteManager = new NoteManager();
    }

    private void Start()
    {
        updateAll();
    }
    public void updateAll() // remove all Notes and then insert them again so index keeps correct  
    {
        foreach (GameObject child in ListWithNoteObjects)
        {
            GameObject.Destroy(child.gameObject);
        }
        ListWithNoteObjects.Clear();
        //addEmptyNote(); // oneEmptyNote so it wont appear totally empty
        NoteClass[] currentNotes = noteManager.getNotes().ToArray();

        for (int i = 0; i < currentNotes.Length; i++) // first only insert new/unread notes, so the unread always appear ontop
        {
            if (currentNotes[i].isNew)
            {
                 AddNewNote(i, currentNotes[i].content, currentNotes[i].isNew, currentNotes[i].isEditable, true); // need solution to also safe if note was already read
            }
        }
                for (int i = 0; i < currentNotes.Length; i++) // then all other notes appear
        {
            if (!currentNotes[i].isNew)
            {
                 AddNewNote(i, currentNotes[i].content, currentNotes[i].isNew, currentNotes[i].isEditable, true); // need solution to also safe if note was already read
            }
        }
    }

    public void addEmptyNote()
    {
        int number = noteManager.getNotes().Count;
        AddNewNote(number);
    }
    //adds a new note field into the app
    public void AddNewNote(int number, string content = "", bool isNew = false, bool isEditable = true, bool init = false)
    {
        GameObject duplicate = Instantiate(note);
        duplicate.SetActive(true);
        //the duplicate is now the child of notes Parent
        duplicate.transform.SetParent(notesParent.transform);
        //scale it properly
        duplicate.gameObject.transform.localScale = new Vector3(1, 1, 1); 
        

        duplicate.transform.Find("InputField").GetComponent<InputField>().text = content;
        duplicate.transform.Find("InputField").GetComponent<InputField>().onValueChanged.AddListener((data) => { UpdateInputField(data, number); });

        if (!isEditable)
        {
            duplicate.transform.Find("InputField").GetComponent<InputField>().interactable = false;
            duplicate.transform.Find("InputField").Find("DeleteButton").gameObject.SetActive(false);
        }
        else
        {
            duplicate.transform.Find("InputField").Find("DeleteButton").GetComponent<Button>().onClick.AddListener(() => { Destroy(duplicate); deleteAndReload(number); });
        }

        if (!init)
        {
            noteManager.newNote(content, isNew, isEditable);
        }

        if (isNew)
        {
            duplicate.transform.Find("Circle").GetComponent<Image>().sprite = noteNotReadCircle;
        }
        else
        {
            duplicate.transform.Find("Circle").GetComponent<Image>().sprite = noteReadCircle;
        }
        ListWithNoteObjects.Add(duplicate);
    }

    public void deleteAndReload(int index)
    {
        noteManager.removeNote(index);
        updateAll();
    }

    void UpdateInputField(string content, int index)
    {
        noteManager.changeNote(index, content);
    }
}

