using UnityEngine.UI;

public class PlatformAwareScrollRect : ScrollRect
{
    public float scrollSpeed = 1f;

    protected override void Awake()
    {
        base.Awake();

        this.scrollSensitivity = scrollSpeed * scaleByPlatform();
    }

    private float scaleByPlatform()
    {
        #if UNITY_STANDALONE_WIN || UNITY_WEBGL
            return 50;
        #elif UNITY_STANDALONE_OSX || UNITY_STANDALONE_LINUX
            return 4;        
        #else
            return 1;
        #endif
    }
}