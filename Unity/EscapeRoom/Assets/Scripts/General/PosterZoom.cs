using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PosterZoom : MonoBehaviour
{

    public NavMeshAgent agent;

    public void ClosePoster()
    {

        GameObject obj = this.transform.parent.gameObject;

        obj.SetActive(false);
        agent.isStopped = false;
    }
}
