using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using UnityEngine.AI;

public class QuizMachineManager : MonoBehaviour
{
    [SerializeField] TextAsset[] MoodleXmlFiles;
    [SerializeField] GameObject gameElemPrefab, quizGrid;

    [SerializeField] GameObject musicElemPrefab, musicGrid;
    [SerializeField] GameObject moodleQuizPrefab;

    [SerializeField] TextMeshProUGUI currentPlayingSongTitle, currentMoney;

    [SerializeField] TextMeshProUGUI popUpWindow_buyText;
    [SerializeField] GameObject popUpWindow, popUpWindow_buy, popUpWindow_notEnoughMoney, popUpWindow_buyButton;

    private GameObject moodleQuiz;
    private QuizManager quizManager;
    private AudioManager audioManager;

    [SerializeField] GameObject musicPlayer_Playbutton, musicPlayer_Pausebutton;
    [SerializeField] RawImageScroller muicVisualizerAnimator;
    private List<GameObject> quizButtons, songButtons;

    public int money; // start money is defined in defaultStates and gets initialized in Start()

    public int defaultSongPrice = 2;

    void Start()
    {
      quizManager = new QuizManager();
      audioManager = FindObjectOfType<AudioManager>();
      songButtons = new List<GameObject>();
      quizButtons = new List<GameObject>();

      GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = true;

      initAllMoodleQuiz();
      addQuizButtons();
      addSongButtons();

      updateMoney(PlayerPrefs.GetInt("quizMachineMoney"));
      currentPlayingSongTitle.text = "Standard-Song";

      if (audioManager.getIsBackgroundMusicPlaying())
        {
            musicPlayer_Playbutton.SetActive(false);
            musicPlayer_Pausebutton.SetActive(true);
            currentPlayingSongTitle.text = audioManager.getCurrentBackgroundMusic();
            musicPlayer_Pausebutton.GetComponent<Button>().onClick.AddListener(() => { pauseSong(audioManager.getCurrentBackgroundMusic()); });
            muicVisualizerAnimator.setSpeed(new Vector2(0.1f, 0.0f));
        }
        else
        {
            musicPlayer_Playbutton.SetActive(true);
            musicPlayer_Pausebutton.SetActive(false);
            musicPlayer_Playbutton.GetComponent<Button>().onClick.AddListener(() => { playSong(audioManager.getCurrentBackgroundMusic()); });
            currentPlayingSongTitle.text = "Song pausiert";
            muicVisualizerAnimator.setSpeed(new Vector2(0.0f, 0.0f));
        }
    }

    /// <summary>Reloads alls quizButtons inside the grid to refresh values.</summary>
    public void refreshQuizButtons()
    {
      destroyAllGameObjectsFromList(quizButtons);
      addQuizButtons();
    }
    
    /// <summary>Adds a Button for every MC-Quiz to a grid. The Button starts the Quiz.</summary>
    private void addQuizButtons()
    {
        foreach (QuizClass quiz in quizManager.getAllQuizzes())
        {
            GameObject quizButton = (GameObject)Instantiate(gameElemPrefab, quizGrid.transform); // instantiate a button for each quiz and set parent to grid
            quizButton.GetComponent<Button>().onClick.AddListener(() => { startQuiz(quiz.name); }); // ad onclick-function to each button
            quizButton.GetComponent<gameButtonScript>().setToSolved(quiz.isSolved); // show correct icon
            quizButton.GetComponentInChildren<TextMeshProUGUI>().text = quiz.name; // set the text of the button to the name of the quiz
            quizButtons.Add(quizButton);
        }
    }

    /// <summary>Adds all songs which contain "soundtrack_0" + X from audioManager to a grid.</summary>
    private void addSongButtons()
    {
        foreach (Sound song in audioManager.sounds)
        {
            if (song.name.Contains("soundtrack_0")) //only display pacer_soundtracks and filter out other sounds
            {
                addSongToPlayerPrefs(song.name); //adds only if not already exists
                GameObject musicButton = (GameObject)Instantiate(musicElemPrefab, musicGrid.transform); // instantiate a button for each song and set parent to grid
                musicButtonScript script = musicButton.transform.Find("musicButtonScript").GetComponent<musicButtonScript>();
                if (isSongPaid(song.name) || song.name.Contains("soundtrack_01"))
                {
                    script.initButton(song.name, defaultSongPrice, musicButtonScript.State.Pause, this);
                }
                else
                {
                    script.initButton(song.name, defaultSongPrice, musicButtonScript.State.Buyable, this);
                }
                songButtons.Add(musicButton);
            }
        }
    }

    /// <summary>Adds a Song to PlayerPrefs, if its not already exists and store isPaid status.</summary>
    private void addSongToPlayerPrefs(string songName)
    {
        if (!PlayerPrefs.HasKey("PlayerPref_"+songName+"_isPaid")) // only add, if not already exists
        {
            PlayerPrefs.SetInt("PlayerPref_"+songName+"_isPaid", 0);
        }
    }

    /// <summary>Returns true if the given song is paid already.</summary>
    public Boolean isSongPaid(string songName)
    {
        return (PlayerPrefs.GetInt("PlayerPref_"+songName+"_isPaid") == 1);
    }

    /// <summary>Starts the given MC-Quiz by name.</summary>
    private void startQuiz(string quizName)
    {
        quizManager.setCurrentQuizName(quizName);
        moodleQuiz = Instantiate(moodleQuizPrefab);
        moodleQuiz.GetComponentInChildren<GameManager>().startedByQuizMachine = true;
    }

    /// <summary> Must only be called once. Adds all Quizzes from MoodleXMLs to Json in Playerprefs.</summary>
    private void initAllMoodleQuiz()
    {
        if (quizManager.getQuizByName("Moodle_Quiz_0") != null) // check if moodle quiz are already loaded
        {
            print("Moodle Quiz already loaded");
        }
        else
        {
            for (int i = 0; i < MoodleXmlFiles.Length; i++) // for each xml file
            {
                QuizClass quiz = new QuizClass("Moodle_Quiz_" + i);
                print("Moodle_Quiz_" + i);
                quiz.questions.AddRange(new XMLimport(MoodleXmlFiles[i]).questions); //append all questions from the xml file to the quiz
                quizManager.QuizStorage.AllQuiz.Add(quiz);
                quizManager.SaveData();
            }
        }
    }

    /// <summary> Destroys every GameObject inside the given List. Clears List afterwards.</summary>
    private void destroyAllGameObjectsFromList(List<GameObject> list)
    {
        foreach (GameObject gameObject in list)
        {
            Destroy(gameObject);
        }
        list.Clear();
    }

    /// <summary> Play Song by name, change current active song, hide play button & display pause button.</summary>
    public void playSong (string songName, musicButtonScript script = null)
    {
        //also assign correct functions to pause and play button under the musicPlayer
        musicPlayer_Playbutton.GetComponent<Button>().onClick.RemoveAllListeners(); // remove all listeners from play button
        musicPlayer_Playbutton.GetComponent<Button>().onClick.AddListener(() => { playSong(songName,script); });
        musicPlayer_Pausebutton.GetComponent<Button>().onClick.RemoveAllListeners(); // remove all listeners from play button
        musicPlayer_Pausebutton.GetComponent<Button>().onClick.AddListener(() => { pauseSong(songName,script); });
        musicPlayer_Pausebutton.SetActive(true);
        musicPlayer_Playbutton.SetActive(false);

        foreach (GameObject button in songButtons) // set all other playing songs to paused
        {
            musicButtonScript tempButtonScript = button.transform.Find("musicButtonScript").GetComponent<musicButtonScript>();
            if (tempButtonScript.currState == musicButtonScript.State.Playing)
            {
                tempButtonScript.changeState(musicButtonScript.State.Pause);
            }
        }
        audioManager.SetMusic(songName); // only plying and not setting as default background music, because we want to be able to pause the song
        if (script != null) //set this song to playing if called by musicButton
        {
            script.changeState(musicButtonScript.State.Playing);
        }
        currentPlayingSongTitle.text = songName;
        muicVisualizerAnimator.setSpeed(new Vector2(0.1f, 0.0f));
    }

    /// <summary> Triggers the PopUpWindows to confirm.</summary>
    public void showBuyPopUpWindow (string songName, int cost, musicButtonScript script)
    {
        print("Buy Song: "+ songName);
        if (money-cost >= 0)
        {
            popUpWindow.SetActive(true);
            popUpWindow_buy.SetActive(true);
            popUpWindow_notEnoughMoney.SetActive(false);
            popUpWindow_buyButton.GetComponent<Button>().onClick.RemoveAllListeners();
            popUpWindow_buyButton.GetComponent<Button>().onClick.AddListener(() => buySong(songName, cost, script));
            popUpWindow_buyText.text = "Möchtest Du den Song '"+songName+"' erwerben? Danach hast Du noch "+(money-cost).ToString()+" Punkte übrig.";
        }
        else 
        {
            popUpWindow.SetActive(true);
            popUpWindow_buy.SetActive(false);
            popUpWindow_notEnoughMoney.SetActive(true);
        }
    }

    /// <summary>Buys Song if enough money is left. Changes State of the song if successful.</summary>
    public void buySong (string songName, int cost, musicButtonScript script)
    {
        print("Buy Song: "+ songName);
        if (money-cost >= 0)
        {
            updateMoney(money-cost);
            PlayerPrefs.SetInt("PlayerPref_"+songName+"_isPaid", 1);
            script.changeState(musicButtonScript.State.Pause);
            popUpWindow.SetActive(false);
        }
    }

    /// <summary>Pause the song of this songButton.</summary>
    public void pauseSong (string songName, musicButtonScript script = null)
    {
        musicPlayer_Pausebutton.SetActive(false);
        musicPlayer_Playbutton.SetActive(true);
        currentPlayingSongTitle.text = "Song pausiert";
        audioManager.StopAll();
        if (script != null) //set this song to paused if called by musicButton
        {
            script.changeState(musicButtonScript.State.Pause);
        }
        muicVisualizerAnimator.setSpeed(new Vector2(0.0f, 0.0f));
        musicPlayer_Playbutton.GetComponent<Button>().onClick.AddListener(() => { playSong(songName); });
    }

    /// <summary>Updates the money and saves it to PlayerPrefs.</summary>
    private void updateMoney(int newMoney)
    {
        money = newMoney;
        currentMoney.text = money.ToString();
        PlayerPrefs.SetInt("quizMachineMoney", money);
    }

    /// <summary>Updates Money and Buttons after a Quiz got solved</summary>
    public void quizSolved()
    {
        updateMoney(money + 1);
        refreshQuizButtons();
    }

    /// <summary>Close the quizMachine/summary>
    public void closeQuizMachine()
    {
        GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = false;
        print("Close Quiz Machine");
        Destroy(gameObject.transform.parent.gameObject);
    }
    public void closeBuyPopUpWindow()
    {
        popUpWindow.SetActive(false);
    }

}