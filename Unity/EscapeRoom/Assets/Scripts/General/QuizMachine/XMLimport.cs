using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Xml.Linq;
using System.IO;

class XMLimport
{
    public List<QuestionClass> questions = new List<QuestionClass>();
    public XMLimport(TextAsset FILENAME)
    {
        ReadXML(FILENAME);
    }
    public void ReadXML(TextAsset filename)
    {
        XDocument doc = XDocument.Load(new MemoryStream(filename.bytes));
        doc.Descendants("quiz").Select(x => ReadKeys(x)).ToList(); // quiz is the root of the moodle xml
    }
    public List<QuestionClass> ReadKeys(XElement single)
    {
        int index = 0;
        List<int> correctAnswerIndex = new List<int>();
        String currentquestion = "";
        foreach (XElement question in single.Descendants("question")) // iterate threw all question elements in moodle xml
        {
            switch (question.Attribute("type").Value)
            {
                case "multichoice": case "truefalse": // truefalse and multichoice same xml structure
                    index = 0;
                    correctAnswerIndex = new List<int>(); // reset list each time
                    List<string> answers = new List<string>();
                    currentquestion = RemoveHTMLTagsCharArray((String)question.Element("questiontext").Element("text")); //  <question type="multichoice"><name>  <text>Welcher dieser Dinosaurier war ein Fleischfresser?</text> </name>
                    foreach (XElement answer in question.Descendants("answer")) // iterate all answers inside each question
                    {
                        answers.Add(RemoveHTMLTagsCharArray((String)answer.Element("text")));
                        if (int.Parse((String)answer.Attribute("fraction")) > 0) // There are no Single-Choice Questions in Moodle. Only Multi-Choice where you give one Answer 100% of the Point
                        {
                            correctAnswerIndex.Add(index); // add all indexes of correct answers to list
                        }
                        index++;
                    }
                    questions.Add(new QuestionClass(currentquestion, answers, correctAnswerIndex)); // create new Question Object in List
                    break;
            }
        }
        return questions;
    }
    public static string RemoveHTMLTagsCharArray(string html) // remove the html tags from strings: https://www.nilebits.com/blog/2010/01/how-to-remove-html-tags-from-string-in-c/
    {                                                         // possible problems: answer is inside <answer> -> then we need a better solution
        char[] charArray = new char[html.Length];
        int index = 0;
        bool isInside = false;

        for (int i = 0; i < html.Length; i++)
        {
            char left = html[i];

            if (left == '<')
            {
                isInside = true;
                continue;
            }

            if (left == '>')
            {
                isInside = false;
                continue;
            }

            if (!isInside)
            {
                charArray[index] = left;
                index++;
            }
        }

        return new string(charArray, 0, index);
    }
}
