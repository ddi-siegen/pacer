using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;


public class gameButtonScript: MonoBehaviour
{
    [SerializeField] GameObject solvedIcon, notSolvedIcon, rewardText;

    public bool isSolved = false;
  
    public void setToSolved(bool isSolved)
    {
        this.isSolved = isSolved;
        if (isSolved)
        {
            solvedIcon.SetActive(true);
            notSolvedIcon.SetActive(false);
            rewardText.SetActive(false);
        }
        else
        {
            solvedIcon.SetActive(false);
            notSolvedIcon.SetActive(true);
            rewardText.SetActive(true);
        }
    }
}
