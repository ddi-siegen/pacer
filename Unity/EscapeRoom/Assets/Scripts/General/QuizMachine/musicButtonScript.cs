using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;


public class musicButtonScript : MonoBehaviour
{
    [SerializeField] GameObject musicElem, playButton, pauseButton, buyButton;
    [SerializeField] TextMeshProUGUI songTitle, costText;
    [SerializeField] GameObject costIcon;
    private QuizMachineManager quizMachineManager;
    public enum State
    {
        Playing, // currently playing this song - show pause button
        Pause, // currently paused - show play button
        Buyable // not bought yet - show buy button
    }
    public State currState;
    public string songName;
    public int songCost;

    /// <summary> Init Title and Cost of the Songbutton. Show Buy, Play or Pause Button depending on State. </summary>
    public void initButton(string title, int cost, State state, QuizMachineManager quizMachineManager)
    {
        this.quizMachineManager = quizMachineManager;
        songTitle.text = title;
        costText.text = "-" + cost.ToString();
        currState = state;
        songName = title;
        songCost = cost;
        switch (state)
        {
            case State.Playing:
                playButton.SetActive(false);
                pauseButton.SetActive(true);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { quizMachineManager.pauseSong(title, this); }); // ad onclick-function to each button
                break;
            case State.Pause:
                playButton.SetActive(true);
                pauseButton.SetActive(false);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { quizMachineManager.playSong(title, this); }); // ad onclick-function to each button
                break;
            case State.Buyable:
                playButton.SetActive(false);
                pauseButton.SetActive(false);
                buyButton.SetActive(true);
                costIcon.SetActive(true);
                costText.gameObject.SetActive(true);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { quizMachineManager.showBuyPopUpWindow(title, songCost, this); }); // ad onclick-function to each button
                break;
        }
    }

    /// <summary>Update Show Buy, Play or Pause Button depending on State. </summary>
    public void changeState(State state)
    {
        currState = state;
        switch (state)
        {
            case State.Playing:
                playButton.SetActive(false);
                pauseButton.SetActive(true);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { quizMachineManager.pauseSong(songName, this); }); // ad onclick-function to each button
                break;
            case State.Pause:
                playButton.SetActive(true);
                pauseButton.SetActive(false);
                buyButton.SetActive(false);
                costIcon.SetActive(false);
                costText.gameObject.SetActive(false);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { quizMachineManager.playSong(songName, this); }); // ad onclick-function to each button
                break;
            case State.Buyable:
                playButton.SetActive(false);
                pauseButton.SetActive(false);
                buyButton.SetActive(true);
                costIcon.SetActive(true);
                costText.gameObject.SetActive(true);
                musicElem.GetComponent<Button>().onClick.RemoveAllListeners();
                musicElem.GetComponent<Button>().onClick.AddListener(() => { quizMachineManager.showBuyPopUpWindow(songName, songCost, this); }); // ad onclick-function to each button
                break;
        }
    }
}
