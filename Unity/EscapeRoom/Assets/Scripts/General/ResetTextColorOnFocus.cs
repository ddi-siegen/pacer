using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResetTextColorOnFocus : MonoBehaviour, ISelectHandler
{
    [SerializeField] private Text _textComponent;
    [SerializeField] private Image _imageComponent;

    private Color32 _textDefaultColor;
    private Color32 _imageDefaultColor;

    private void Awake()
    {
        if (_textComponent != null)
        {
            _textDefaultColor = _textComponent.color;
        }

        if (_imageComponent != null)
        {
            _imageDefaultColor = _imageComponent.color;
        }
    }

    public void OnSelect(BaseEventData data)
    {
        if (_textComponent != null)
        {
            _textComponent.color = _textDefaultColor;
        }

        if (_imageComponent != null)
        {
            _imageComponent.color = _imageDefaultColor;
        }
    }
}
