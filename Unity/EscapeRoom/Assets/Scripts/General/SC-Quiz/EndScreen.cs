using UnityEngine;
using TMPro;
public class EndScreen : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI finalScoreText;
    ScoreKeeper scoreKeeper;

    void Awake() // Awake gets called before start. Finding Objects in Awake is best practice
    {
        scoreKeeper = FindObjectOfType<ScoreKeeper>();
    }

    public void ShowFinalScore()
    {
        QuizManager quizManager = new QuizManager();

        int score = scoreKeeper.CalculateScore();
        if (score > 80)
        {
            finalScoreText.text = "Herzlichen Glueckwunsch!\nDu hast " + score + " % richtig und das Quiz geschafft";
            quizManager.setCurrentQuizToSolved();
        }
        else if (score > 50)
        {
            finalScoreText.text = "Fast geschaftt!\nDu hast immerhin" + score + " % richtig und beim nächsten Versuch schaffst du es bestimmt";
        }
        else
        {
            finalScoreText.text = "Nicht geschaftt!\nDu hast " + score + " % richtig. Untersuche Plakate und Poster um die notwendigen Informationen zu erhalten.";
        }
    }



}
