using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject skipTimerButton;
    public GameObject SCQuiz; // parent element to close the Quiz

    Quiz quiz;
    EndScreen endScreen;

    NavMeshAgent agent;

    public bool startedByQuizMachine = false;

    void Awake() // Awake gets called before start. Finding Objects in Awake is best practice
    {
        quiz = FindObjectOfType<Quiz>();
        endScreen = FindObjectOfType<EndScreen>();
        agent = GameObject.Find("Robot").GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        quiz.gameObject.SetActive(true);
        endScreen.gameObject.SetActive(false);
    }

    void Update()
    {
        if (quiz.isComplete)
        {
            skipTimerButton.SetActive(false);
            quiz.gameObject.SetActive(false);
            endScreen.gameObject.SetActive(true);
            endScreen.ShowFinalScore();
        }
    }
    public void endGame()
    {
        QuizManager quizManager = new QuizManager();
        //quiz.gameObject.SetActive(true);
        endScreen.gameObject.SetActive(false);
        Destroy(SCQuiz);
        agent.isStopped = false;
        if (quizManager.checkQuizSolved(quizManager.getCurrentQuizName()))
        {
            doStuffDependingOnQuiz(quizManager.getCurrentQuizName());
        }
    }
    public void repeat()
    {
        SCQuiz.SetActive(true);
    }
    /* this will not work because our scene is more than just the quiz
        also we dont want to replay (user can just press button again)
    public void OnReplayLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    */
    private void doStuffDependingOnQuiz(string quizName)
    {
        if (!startedByQuizMachine)
        {
            switch (quizName)
            {
                case "passwordQuiz":
                    PlayerPrefs.SetInt("LiftQuestionAnswered", 1);
                    FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["ElevatorFirstTimeUsageDialog*00"]);
                    break;
                case "passwordQuiz2":
                    PlayerPrefs.SetInt("LiftQuestionTwoAnswered", 1);
                    FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["ElevatorSecondTimeUsageDialog*00"]);
                    break;

                default:
                    break;
            }
        }
        else
        {
            try
            {
                GameObject.Find("QuizMachine_Manager").GetComponent<QuizMachineManager>().quizSolved();
            }
            catch (System.Exception e)
            {
                Debug.Log("Quiz was not started from QuizMachine.");
            }
        }
    }

}
