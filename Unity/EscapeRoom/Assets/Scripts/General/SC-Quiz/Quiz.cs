using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class Quiz : MonoBehaviour
{
    [Header("XML File (assets/MoodleXML_export)")]
    public TextAsset xmlfile;

    [Header("Questions")] // Headers help to keep Unity UI well-arranged
    TextMeshProUGUI questionText;
    [SerializeField] Button questionbutton;
    QuestionClass currentquestion;
    List<QuestionClass> questions;

    [Header("Answers")]
    [SerializeField] GameObject[] answerButtons; // Array of GameObjects with our answerButtons. Initialized for each Question in CreateButtons()
    [SerializeField] GameObject buttonGroupLeft;
    [SerializeField] GameObject buttonGroupRight;
    [SerializeField] GameObject answerButtonPrefab;
    int[] correctAnswersIndexes; // holds the index of the correct answer

    [Header("Button Colors")]
    [SerializeField] Sprite defaultAnswerSprite;
    [SerializeField] Sprite correctAnswerSprite;
    [SerializeField] Sprite selectedAnswerSprite;

    [Header("Timer")]
    [SerializeField] Image timerImage;
    Timer timer;

    [Header("Scoring")]
    [SerializeField] TextMeshProUGUI scoreText;
    ScoreKeeper scoreKeeper;

    [Header("ProgressBar")]
    [SerializeField] Slider progressBar;

    public bool isComplete;

    private string quizName;

    private bool questionActive = false;

    private List<int> selectedAnswersIdexes = new List<int>();
    void Awake() // Awake gets called before start. Finding Objects in Awake is best practice
    {
        Reset();
    }
    public void Reset()
    {
        this.gameObject.SetActive(true);
        timer = FindObjectOfType<Timer>(); // init timer script
        scoreKeeper = FindObjectOfType<ScoreKeeper>(); // init scorekeeper script

        QuizManager quizManager = new QuizManager();
        quizName = quizManager.getCurrentQuizName();
        this.questions = quizManager.getQuizByName(quizName).questions;
        //questions = new XMLimport(xmlfile).questions; // Import List with Question Object from XML
        progressBar.maxValue = questions.Count;
        progressBar.value = 0;
        questionText = questionbutton.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Update()
    {
        timerImage.fillAmount = timer.fillFraction; // Update Timerimage
        if (timer.loadNextQuestion) // loads next question. Look up Timer.cs for more insight
        {
            if (progressBar.value == progressBar.maxValue) // true after last question answered
            {
                isComplete = true; // once true GameManager will bring us to the EndScreen
                return;
            }
            questionActive = false;
            GetNextQuestion();
            timer.loadNextQuestion = false;
        }
        else if (!timer.isAnsweringQuestion && !questionActive) // only happens if user is not answering in time
        {
            DisplayAnswer();
            SetButtonState(false);
        }
    }

    public void OnAnswerSelected(int index) // index depending on the clicked Answerbutton (onclick-function)
    {
        if (selectedAnswersIdexes.Contains(index)) // if button already clicked -> remove it from the list and reset button sprite
        {
            selectedAnswersIdexes.Remove(index);
            answerButtons[index].GetComponent<Image>().sprite = defaultAnswerSprite;
        }
        else                                // else add button to list
        {
            selectedAnswersIdexes.Add(index);
            answerButtons[index].GetComponent<Image>().sprite = selectedAnswerSprite;
        }
    }

    void DisplayAnswer()
    {
        if (currentquestion != null)
        {
            //compare correct answers to selected answers
            int[] selectedAnswersIdexesArray = selectedAnswersIdexes.ToArray(); //get all indexes of the selected buttons
            correctAnswersIndexes = currentquestion.getcorrectAnswersIndexes(); // get all indexes of the correct answers
            var compare = correctAnswersIndexes.Except(selectedAnswersIdexesArray);
            var compare2 = selectedAnswersIdexesArray.Except(correctAnswersIndexes);
            if (compare.Count() == 0 && compare2.Count() == 0) // if both arrays contain the same data there will be no rest (order doesnt matter) -> rest.length == 0
            {
                //They are the same
                scoreKeeper.IncrementCorrectAnswers(); // We could also increment depending on how many % of correct answers are right. Right now a player only gets points if everything is correct
                questionText.text = "Super! Richtige Antwort";
                for (int i = 0; i < answerButtons.Length; i++) // we just need to set the selected answers to green
                {
                    if (selectedAnswersIdexesArray.Contains(i))
                    {
                        Image buttonImage = answerButtons[i].GetComponent<Image>();
                        buttonImage.sprite = correctAnswerSprite;
                    }
                }
            }
            else
            {
                //They are different
                questionText.text = "Leider nicht komplett richtig.";
                for (int i = 0; i < answerButtons.Length; i++) // we need to set all correct answers to green and keep selected answers orange
                {                                              // since we already have all selected answers marked - we just need to override correct ones
                    if (correctAnswersIndexes.Contains(i))
                    {
                        Image buttonImage = answerButtons[i].GetComponent<Image>();
                        buttonImage.sprite = correctAnswerSprite;
                    }
                }
            }
            questionActive = true;
            scoreText.text = "Score: " + scoreKeeper.CalculateScore() + "%";
        }
    }

    void DisplayQuestion()
    {
        questionText.text = currentquestion.getQuestion(); // gets the current Question and displays it in the UI

        for (int i = 0; i < answerButtons.Length; i++) // set Buttontext for all 4 possible answers
        {
            TextMeshProUGUI buttonText = answerButtons[i].GetComponentInChildren<TextMeshProUGUI>(); //get the TextMeshProUGUI element of each button
            buttonText.text = currentquestion.getAnswer(i);
        }
    }

    void SetButtonState(bool state)  //enables/disables interactablity of all answerbuttons
    {
        for (int i = 0; i < answerButtons.Length; i++)
        {
            Button button = answerButtons[i].GetComponent<Button>();
            button.interactable = state;
        }
    }

    void GetNextQuestion()
    {
        if (questions.Count > 0)
        {
            GetRandomQuestion();                  // gets the next random Question
            CreateButtons();                      // create an Answerbutton for each Answer
            SetButtonState(true);                 // enables Buttons again
            SetDefaultButtonSprites();            // reset Button colors
            DisplayQuestion();                    // sets Texts for Buttons and Question
            progressBar.value++;                  // update Progressbar
            scoreKeeper.IncrementQuestionsSeen(); // Increment Counter
            selectedAnswersIdexes = new List<int>(); // reset selectedAnswers
        }
    }
    void GetRandomQuestion()
    {
        int index = UnityEngine.Random.Range(0, questions.Count); // picks a Rondom number depending of questions inside the list
        currentquestion = questions[index];

        if (questions.Contains(currentquestion))
        {
            questions.Remove(currentquestion); // remove the picked question from the list so we wont get it twice
        }
    }
    void SetDefaultButtonSprites() // resets all button colors to default
    {
        for (int i = 0; i < answerButtons.Length; i++)
        {
            Image buttonImage = answerButtons[i].GetComponent<Image>();
            buttonImage.sprite = defaultAnswerSprite;
        }
    }
    void CreateButtons() //delete old buttons and create a new one for each answer - height depending on answer count
    {   //delete old button first
        foreach (GameObject button in answerButtons)
        {
            Destroy(button);
        }
        answerButtons = new GameObject[currentquestion.getAnswerCount()];
        // create new buttons
        for (int i = 0; i < currentquestion.getAnswerCount(); i++)
        {
            int closureIndex = i; // Prevents the closure problem -> https://answers.unity.com/questions/1376530/add-listeners-to-array-of-buttons.html
            GameObject button = (GameObject)Instantiate(answerButtonPrefab);
            if (i % 2 == 0) // if even put button in left button group
            {
                button.transform.SetParent(buttonGroupLeft.transform);//Setting button parent to our buttonGroupLeft with grid layout   
            }
            else // if not even put button on right button group
            {
                button.transform.SetParent(buttonGroupRight.transform);
            }

            button.GetComponent<Button>().onClick.AddListener(() => { OnAnswerSelected(closureIndex); }); // ad onclick-function to each button
            answerButtons[i] = button;
        }
        /* solved with two vertical groups instead
        // height of buttons depending on count. Text gets resized automatically by settings in prefab
        int buttonheight = 200;
        switch (currentquestion.getAnswerCount())
        {
            case (1): case (2): buttonheight = 400; break;
            case (3): case (4): buttonheight = 300; break;
            case (5): case (6): buttonheight = 225; break;
            case (7): case (8): buttonheight = 175; break;
            case (9): case (10): buttonheight = 150; break;
            case (11): case (12): buttonheight = 120; break;
            case (13): case (14): buttonheight = 100; break;
        }
        buttonGroupLeft.GetComponent<GridLayoutGroup>().cellSize = new Vector2(950, buttonheight);
        buttonGroupLeft.GetComponent<GridLayoutGroup>().spacing = new Vector2(-350, -buttonheight/2);
        */
    }
}
