using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class QuestionClass
{
    // Textarea defines the minimum an maximum lines in the inspector where we can edit the string with unity
    public string question = "Enter new question text here";  // SerializeField allows us to see the question from the inspector but not from other scripts
    public string[] answers;

    public int [] correctAnswersIndexes;
    public QuestionClass(string question, List<string> answers, List<int> correctAnswersIndexes)
    {
        this.question = question;
        this.answers = answers.ToArray();
        this.correctAnswersIndexes = correctAnswersIndexes.ToArray();
    }
    // Getters
    public string getQuestion()
    {
        return question;
    }
    public int[] getcorrectAnswersIndexes()
    {
        return correctAnswersIndexes;
    }
    public string getAnswer(int index)
    {
        return answers[index];
    }
    public int getAnswerCount()
    {
        return answers.Length;
    }
}

[System.Serializable]
public class QuizClass
{
    public List<QuestionClass> questions = new List<QuestionClass>();
    public bool isSolved = false;

    public string name;
 
    public QuizClass(string name)
    {
        this.name = name;
    }
}

[System.Serializable]
public class QuizStorage
{
    public string currentQuiz; // default - set this to the desired Quiz before activate the Quiz Gameobject
    public List<QuizClass> AllQuiz = new List<QuizClass>();
}

public class QuizManager
{
    public QuizStorage QuizStorage = new QuizStorage();

    public QuizManager() // here we define the preconfigured MC-Quizes
    {
        if (!PlayerPrefs.HasKey("TheQuizStorage")) // init levelData if not already saved - if you change values here you have to start a new game to delte old PlayerPrefs
        {
            QuizClass passwordQuiz = new QuizClass("passwordQuiz");
            passwordQuiz.questions.Add(new QuestionClass("Welches der folgenden Passwörter ist am sichersten?", new List<string>{"1234", "13.01.1994", "Ig+h2igIm?", "UniversitätSiegen"}, new List<int>{2}));
            passwordQuiz.questions.Add(new QuestionClass("Wofür steht das 's' in https?", new List<string>{"sant", "sauber", "super", "sicher"}, new List<int>{3}));
            passwordQuiz.questions.Add(new QuestionClass("Zu den persönlichen Daten gehören", new List<string>{"Kontaktdaten", "Kommunikationsdaten", "Kennwörter", "Surfverhalten"}, new List<int>{0,1,2,3})); // all correct
            passwordQuiz.questions.Add(new QuestionClass("Ich darf ungefragt Fotos von Personen machen", new List<string>{"Falsch", "Richtig", "... wenn diese nur 'hübsches Beiwerk' einer Panoramaaufnahme sind"}, new List<int>{0,2}));
            QuizStorage.AllQuiz.Add(passwordQuiz);

            QuizClass passwordQuiz2 = new QuizClass("passwordQuiz2");
            passwordQuiz2.questions.Add(new QuestionClass("Mit einem Passwortmanager muss ich mir weniger Passwörter merken und bin trotzdem sicherer unterwegs.", new List<string>{"Falsch", "Richtig"}, new List<int>{1}));
            passwordQuiz2.questions.Add(new QuestionClass("Aus wie vielen Bits besteht ein Byte?", new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", }, new List<int> { 7 }));
            passwordQuiz2.questions.Add(new QuestionClass("Wie viele Kilobytes ergeben ein Megabyte?", new List<string> { "512", "1024", "24", "256" }, new List<int> { 1 }));
            passwordQuiz2.questions.Add(new QuestionClass("Als Lizenz bezeichnet man eine Besitzurkunde an fremden Werken", new List<string> { "Richtig", "Falsch"}, new List<int> { 1 }));
            
            QuizStorage.AllQuiz.Add(passwordQuiz2);

            QuizClass fpQuiz = new QuizClass("fpQuiz");
            fpQuiz.questions.Add(new QuestionClass("Zu den Netiquette-Regeln gehört:", new List<string>{"Höflich bleiben", "Den eigenen Willen durchsetzen", "Kulturelle Vielfalt berücksichtigen", "Viele Abkürzungswörter benutzen"}, new List<int>{0,2}));
            fpQuiz.questions.Add(new QuestionClass("Welche dieser Algorithmen können zum Sortieren von beispielsweise Zahlen verwendet werden?", new List<string>{"Quicksort", "Insertionsort", "Mergesort", "Picksort"}, new List<int>{0,1,2}));
            fpQuiz.questions.Add(new QuestionClass("Beim Bubblesort werden benachbarte Werte verglichen", new List<string> { "Wahr", "Falsch" }, new List<int> { 0 }));
            QuizStorage.AllQuiz.Add(fpQuiz);
    
            SaveData();
        }
    }

    void LoadSavedData()
    {
        if (PlayerPrefs.HasKey("TheQuizStorage"))
        {
            string saveJason = PlayerPrefs.GetString("TheQuizStorage");
            QuizStorage = JsonUtility.FromJson<QuizStorage>(saveJason);
        }
        else
        {
            Debug.Log("no saved TheQuizStorage");
        }
    }
    public void SaveData()
    {
        string saveJason = JsonUtility.ToJson(QuizStorage);
        PlayerPrefs.SetString("TheQuizStorage", saveJason);
        PlayerPrefs.Save();
        //Debug.Log(saveJason);
    }
    public void addQuiz(QuizClass quiz)
    {
        LoadSavedData();
        QuizStorage.AllQuiz.Add(quiz);
        SaveData();
    }

    public QuizClass getQuizByName(string name, TextAsset xmlfile = null)
    {
        LoadSavedData();
        bool found = false;
        for (int i = 0; i < QuizStorage.AllQuiz.Count; i++)
        {
            if (QuizStorage.AllQuiz[i].name == name)
            { 
                found = true;
                return  QuizStorage.AllQuiz[i];
            }
        }
        if (!found)
        {
            Debug.Log("ERROR: THERE IS NO QUIZ WITH THE NAME: " + name);
        }
        return null;
    }

    public List<QuizClass> getAllQuizzes()
    {
        LoadSavedData();
        return QuizStorage.AllQuiz;
    }
    
    public string getCurrentQuizName()
    {
        LoadSavedData();
        return QuizStorage.currentQuiz;
    }

    public void setCurrentQuizName(string name)
    {
        LoadSavedData();
        QuizStorage.currentQuiz = name;
        SaveData();
    }

    public bool checkQuizSolved(string name)
    {
        LoadSavedData();
        return getQuizByName(name).isSolved;
    }

    public void setCurrentQuizToSolved()
    {
        LoadSavedData();
        getQuizByName(QuizStorage.currentQuiz).isSolved = true;
        SaveData();
    }
}
