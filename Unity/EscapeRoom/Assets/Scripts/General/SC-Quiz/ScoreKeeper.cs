using UnityEngine;

public class ScoreKeeper : MonoBehaviour
{
    int correctAnswers = 0; // correct answers until this point
    int questionsSeen = 0;

    public int getCorrectAnswers()
    {
        return correctAnswers;
    }
    public void IncrementCorrectAnswers()
    {
        correctAnswers++;
    }
    public int getQuestionsSeen()
    {
        return questionsSeen;
    }
    public void IncrementQuestionsSeen()
    {
        questionsSeen++;
    }
    public int CalculateScore() // percentage of correct answers
    {
        return Mathf.RoundToInt(correctAnswers / (float)questionsSeen * 100); // cast to float and then round back to integer 
    }
}
