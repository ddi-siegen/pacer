using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] float timetoCompleteQuestion = 30f;
    [SerializeField] float timetoShowCorrectAnswer = 10f;

    [SerializeField] GameObject skipTimerButton;


    public bool loadNextQuestion;
    public float fillFraction;
    public bool isAnsweringQuestion;
    float timerValue;
    void Update()
    {
        UpdateTimer();
    }
    public void CancelTimer()
    {
        timerValue = 0; // triggers first else statement in UpdateTimer()
    }

    void UpdateTimer()
    {
        timerValue -= Time.deltaTime; // reduce timerValue each update

        if (isAnsweringQuestion)
        {
            if (timerValue > 0) // fillfraction update if user is answering and time is left
            {
                fillFraction = timerValue / timetoCompleteQuestion;
                skipTimerButton.SetActive(true);
            }
            else
            {
                isAnsweringQuestion = false;
                timerValue = timetoShowCorrectAnswer;
                skipTimerButton.SetActive(false);
            }
        }
        else
        {
            if (timerValue > 0) // fillfraction update if user is not answering
            {
                fillFraction = timerValue / timetoShowCorrectAnswer; // this time fillFraction depending on how long we want to show the correct answer
            }
            else // time to load next question. Inside Quiz.cs we will call the next question once loadNextQuestion ist true
            {
                isAnsweringQuestion = true;
                timerValue = timetoCompleteQuestion;
                loadNextQuestion = true;
            }
        }
    }


}
