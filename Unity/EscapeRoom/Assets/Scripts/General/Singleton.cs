using UnityEngine;
public class Singleton : MonoBehaviour
{

    //sound array

    public static Singleton instance;

    public LevelChanger levelchanger;

    // Start is called before the first frame update
    void Awake()
    {
        //destroy manager if two in game
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        DefaultStates defaultState = new DefaultStates();
        levelchanger.instantlyGoToLevel(4); // go to Startscreen at the beginning of the game
    }

}
