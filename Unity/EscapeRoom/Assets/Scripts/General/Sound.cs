using UnityEngine.Audio;
using UnityEngine;

//with this, the array can be shown in inspector
[System.Serializable]

public class Sound
{
    //reference to audio clip
    public AudioClip clip;

    public string name;

    //creates slider in Inspector
    [Range(0f,1f)]
    public float volume;

    //creates slider in Inspector
    [Range(.1f, 3f)]
    public float pitch;

    public bool loop;

    public bool isBackgroundSound;

    //variable is public, but wont show in Inspector
    [HideInInspector]
    public AudioSource source;
}
