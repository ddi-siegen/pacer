using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CharacterClass
{
    public string name, innerTextboxColor, outterTextBoxColor;
    public Sprite icon;

    public CharacterClass(string name, Sprite icon = null, string innerTextboxColor = "", string outterTextBoxColor = "")
    {
        this.name = name;
        this.innerTextboxColor = innerTextboxColor;
        this.outterTextBoxColor = outterTextBoxColor;
        this.icon = icon;
    }
}