using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextBoxClass
{
    public string collider;
    public string text;

    public Sprite icon;

    public string innerTextboxColor;
    
    public string outterTextBoxColor;

    public string characterName;

    public string nextTextbox;

    public string[] neededStatesFor0;

    public string[] neededStatesFor1;

    public string[] settingStatesAfterRead;

    public Action doAfterTextbox;

    public Action doBeforeTextbox;

    public TextBoxClass(string collider, Sprite icon, string innerTextboxColor, string outterTextBoxColor, string characterName, string[] neededStatesFor0, string[] neededStatesFor1, string text, string[] settingStatesAfterRead, string nextTextbox, Action doAfterTextbox, Action doBeforeTextbox)
    {
        this.text = text;
        this.icon = icon;
        this.characterName = characterName;
        this.collider = collider;
        this.innerTextboxColor = innerTextboxColor;
        this.outterTextBoxColor = outterTextBoxColor;
        if (neededStatesFor0 != null)
        {
            this.neededStatesFor0 = neededStatesFor0;
        }
        else
        {
            this.neededStatesFor0 = new string[0];
        }
        if (neededStatesFor1 != null)
        {
            this.neededStatesFor1 = neededStatesFor1;
        }
        else
        {
            this.neededStatesFor1 = new string[0];
        }
        if (settingStatesAfterRead != null)
        {
            this.settingStatesAfterRead = settingStatesAfterRead;
        }
        else
        {
            this.settingStatesAfterRead = new string[0];
        }
        this.nextTextbox = nextTextbox;

        if(doAfterTextbox != null)
        {
            this.doAfterTextbox = doAfterTextbox;
        }
        else
        {
            this.doAfterTextbox = delegate {};
        }
        if (doBeforeTextbox != null)
        {
            this.doBeforeTextbox = doBeforeTextbox;
        }
        else
        {
            this.doBeforeTextbox = delegate { };
        }

    }

  
    public bool gotNeedStatesToBe1() // checks for all neededSted in PlayerPrefs if value is not 0
    {
        bool allTrue = true;
        foreach (string state in neededStatesFor1)
        {
            if (PlayerPrefs.GetInt(state) == 0)
            {
                allTrue = false;
                Debug.Log("Not all requirements (states) need for this Event are met");
            }
        }
        return allTrue; // if all are != 0 it will return true
    }

    public bool gotNeedStatesToBe0() // checks for all neededSted in PlayerPrefs if value is not 1
    {
        bool allTrue = true;
        foreach (string state in neededStatesFor0)
        {
            if (PlayerPrefs.GetInt(state) == 1)
            {
                allTrue = false;
                Debug.Log("Not all requirements (states) need for this Event are met");
            }
        }
        return allTrue; // if all are != 0 it will return true
    }

    public void SetsettingStatesAfterRead() // sets PlayPrefs of all keys in our array to 1
    {
         foreach (string setting in settingStatesAfterRead)
        {
            PlayerPrefs.SetInt(setting, 1);
        }
    }
}