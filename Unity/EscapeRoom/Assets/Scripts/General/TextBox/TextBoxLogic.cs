using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.AI;

public class TextBoxLogic : MonoBehaviour
{

    [Header("Textbox Elements")]
    [SerializeField] GameObject ButtonForward, textboxtext, textboxBackgroundImage, textboxBackgroundBubble, txt_namebox, IconField;
    public Sprite Icon_Doggo, Icon_Robot, Icon_Security, Icon_Smartphone, Icon_Reception, Icon_Angry_Reception, Icon_Facility, Icon_Speaker;

    GameObject player;
    [Header("UBlockly")]
    [SerializeField] GameObject uBlockly_maze;

    [SerializeField] GameObject Quiz;

    QuizManager quizManager;
    NavMeshAgent playerNavMesh;

    private int charCount = 160; // Amount of Chars that fit inside the Textbox

    public bool TextBoxActive = false;

    private bool agentWasStoppedBefore = false;
    private bool menuPanelWasHiddenBefore = false;

    private List<string> textSplits = new List<string>(); // This List contains all strings with charCount chars, which we create from the Text
    private int currentPos = 0; //current position in textSplits (like an index)
    public Dictionary<string, TextBoxClass> Texts = new Dictionary<string, TextBoxClass>(); // Dictionary (better search performance than iterate list) with all Textelements
    public TextBoxClass currentTextBoxClass;

    Dictionary<string, CharacterClass> characters;

    public void Awake()
    {

        characters = new Dictionary<string, CharacterClass>(); 
        // add all characters
        characters.Add("dog", new CharacterClass(PlayerPrefs.GetString("characterNameDog"), Icon_Doggo, "C7C7FF", "286ac9")); // innerTextbox = Sprechblase | outterTextbox = Hintergrund (sollte der dunklere Farbton sein)
        characters.Add("robot", new CharacterClass(PlayerPrefs.GetString("characterNameRobot"), Icon_Robot, "ccedc3", "0f7048"));
        characters.Add("securityManager", new CharacterClass("Security-Manager", Icon_Security, "757575", "63380a"));
        characters.Add("durchsage", new CharacterClass("Durchsage", Icon_Speaker, "c1c1c1", "757575"));
        characters.Add("asr", new CharacterClass("Aufsichtsroboter", Icon_Reception, "8fe8d3", "f4e190"));
        characters.Add("angry_asr", new CharacterClass("Aufsichtsroboter", Icon_Angry_Reception, "ff0000", "000000"));
        characters.Add("aufzug", new CharacterClass("Aufzug", null, "ccedc3", "0f7048"));
        characters.Add("handy", new CharacterClass("Handy", Icon_Smartphone, "a8a8a8", "2d2d2d"));
        characters.Add("facility_manager", new CharacterClass("Facility-Manager", Icon_Facility, "a8a8a8", "2d2d2d"));
        characters.Add("empty", new CharacterClass("", null, null, null));

        // initialize all our Textboxes
        TextBoxTextsHO textBoxFromHO = new TextBoxTextsHO(characters, this);
        TextBoxTextsEL textBoxFromEL = new TextBoxTextsEL(characters, this);
        TextBoxTextsFP textBoxFromFP = new TextBoxTextsFP(characters, this);
        TextBoxTextsCPU textBoxFromCPU = new TextBoxTextsCPU(characters, this);

        // adding them to our Main dict here
        textBoxFromHO.Texts.ToList().ForEach(x => Texts.Add(x.Key, x.Value));
        textBoxFromEL.Texts.ToList().ForEach(x => Texts.Add(x.Key, x.Value));
        textBoxFromFP.Texts.ToList().ForEach(x => Texts.Add(x.Key, x.Value));
        textBoxFromCPU.Texts.ToList().ForEach(x => Texts.Add(x.Key, x.Value));

        //find player
        player = GameObject.Find("Robot");
        playerNavMesh = player.GetComponent<NavMeshAgent>();
    }

    public void setText(TextBoxClass TextBoxClass) // Splits the textelement of a given text object an store it in textSplitsList
    {
        menuPanelWasHiddenBefore = !FindObjectOfType<MenuPanelManager>().isActive;
        agentWasStoppedBefore = player.GetComponent<NavMeshAgent>().isStopped;
        if (TextBoxClass.gotNeedStatesToBe1() && TextBoxClass.gotNeedStatesToBe0()) // only if all needed States are != 0
        {
            // Load Icon and characterName to textbox
            Image IconImage = IconField.GetComponent<Image>();
            IconImage.sprite = TextBoxClass.icon;
            TextMeshProUGUI m_nameboxText = txt_namebox.GetComponentInChildren<TextMeshProUGUI>();
            m_nameboxText.text = TextBoxClass.characterName;
            currentTextBoxClass = TextBoxClass; // save current TextBoxClass object so we can access it from the other methods
            string text = TextBoxClass.text;

            //set colors
            Color color1;
            Color color2;
            ColorUtility.TryParseHtmlString("#"+currentTextBoxClass.outterTextBoxColor, out color1);
            ColorUtility.TryParseHtmlString("#"+currentTextBoxClass.innerTextboxColor, out color2);
            textboxBackgroundImage.GetComponent<Image>().color  = color1;
            textboxBackgroundBubble.GetComponent<Image>().color = color2;
            while (text.Length > 0)
            {
                int charsToNextEmpty = 0;
                if (text.Length > charCount)
                {
                    while (text[charCount - charsToNextEmpty] != ' ' && charsToNextEmpty < 50) // shorten charCount until an empty char is found so we dont skip inside words
                    {
                        charsToNextEmpty++;
                    }
                    textSplits.Add(text.Substring(0, charCount - charsToNextEmpty));
                    text = text.Substring(charCount - charsToNextEmpty);
                }
                else
                {
                    textSplits.Add(text);
                    text = "";
                }
            }
            updateText(); // Display the Text
        }
    }

    public void getNextText() // display the next TextSplit (called by next button)
    {
        if (textSplits.Count - 1 > currentPos)
        {
            currentPos++;
            updateText();
        }
        else
        {
            currentTextBoxClass.SetsettingStatesAfterRead();
            activateTextBox(false); // close TextboxUI after text ended
        }
    }

    public void getPrevText() // display the previous TextSplit (called by previous button)
    {
        if (currentPos > 0)
        {
            currentPos--;
            updateText();
        }
        else
        {
            activateTextBox(false); // close TextboxUI if user clicks prev on first split
        }
    }

    public void updateText() // update the Text to the texsplit at the current position and display it
    {
        activateTextBox(true);
        TextMeshProUGUI m_TextboxText = textboxtext.GetComponentInChildren<TextMeshProUGUI>();
        m_TextboxText.text = textSplits[currentPos];
    }
    public void activateTextBox(bool boolean) // activate/deactivate TextboxUI
    {
        TextBoxActive = boolean;
        textboxtext.SetActive(boolean);
        txt_namebox.SetActive(boolean);
        textboxBackgroundImage.SetActive(boolean);
        IconField.SetActive(boolean);
        ButtonForward.SetActive(boolean);
        textboxBackgroundBubble.SetActive(boolean);

        if (!boolean) // if deactivate clear textsplits and reset position
        {
            if (!menuPanelWasHiddenBefore)
            {
                //FindObjectOfType<MenuPanelManager>().ActiveMenuPanel(true);
            }
            if (!agentWasStoppedBefore)
            {
                player.GetComponent<NavMeshAgent>().isStopped = false;
            }
            textSplits.Clear();
            currentPos = 0;
            currentTextBoxClass.doAfterTextbox(); // do action after textbox
            if (currentTextBoxClass.nextTextbox != "" && Texts.ContainsKey(currentTextBoxClass.nextTextbox)) // display next textbox if there is any
            {
                setText(Texts[currentTextBoxClass.nextTextbox]);
            }
            else if (currentTextBoxClass.nextTextbox != "" && !Texts.ContainsKey(currentTextBoxClass.nextTextbox))
            {
                Debug.Log("Error: " + currentTextBoxClass.nextTextbox + " is not a valid Textboxid!");
            }
        }
        if (boolean) // if activated
        {
            //FindObjectOfType<MenuPanelManager>().ActiveMenuPanel(false);
            currentTextBoxClass.doBeforeTextbox();
            player.GetComponent<NavMeshAgent>().isStopped = true;
            
            if (currentTextBoxClass.icon == null) // only show textbox if there is any icon
            {
                IconField.SetActive(false);
            }
        }
    }
    public bool isTextBoxActive()
    {
        return TextBoxActive;
    }

    public void waitForSecondsUntilNextTextbox(float sec, string nextTextboxId) // we need this to trigger the coroutine because we cant trigget it directly from TextBoxTexts
    {
        StartCoroutine(CoroutineWaitForSecondsUntilNextTextbox(sec, nextTextboxId));
    }
    private IEnumerator CoroutineWaitForSecondsUntilNextTextbox(float sec, string nextTextboxId) // Coroutine that waits for sec until the nextTextboxId will be shown
    {

        if (Texts.ContainsKey(nextTextboxId)) // Check if textboxid exists
        {
            yield return new WaitForSeconds(sec);
            setText(Texts[nextTextboxId]);
        }
        else
        {
            Debug.Log("Error: Textboxid " + nextTextboxId + "doesnt exist!");
        }
    }

    /// <summary>
    /// Starts uBlockly with the given level
    /// </summary>
    public void startUBlockly(int level)
    {
        Instantiate(uBlockly_maze);
        uBlocklyLevelManager uBlocklyLevelManager = new uBlocklyLevelManager();
        uBlocklyLevelManager.setCurrentLevel(level);
        FindObjectOfType<BlocklyManager>(true).activate();
        FindObjectOfType<BlocklyManager>(true).setToCurrLevel();
        FindObjectOfType<MenuPanelManager>(true).SetMenuPanelActive(false);
    }

    public void startQuiz(string name)
    {
        quizManager = new QuizManager();
        quizManager.setCurrentQuizName(name);
        Instantiate(Quiz);
        player.GetComponent<NavMeshAgent>().isStopped = true;
    }

    public void refresh()
    {
        Texts.Clear();
        this.Awake();

    }
}