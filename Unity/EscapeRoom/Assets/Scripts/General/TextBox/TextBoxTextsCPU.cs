﻿using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class TextBoxTextsCPU
{
    public Dictionary<string, TextBoxClass> Texts = new Dictionary<string, TextBoxClass>(); // Dictionary (better search performance than iterate list) with all Textelements

    public TextBoxTextsCPU(Dictionary<string, CharacterClass> characters, TextBoxLogic textBoxLogic)
    {
        //  addToDictionary: 
        //                    key: string (if a gameobject with a 2D collider has the same name, it will trigger the textbox), 
        //                    text: string (text inside the textbox),
        //                    character: charachterClass (string name, optional: Sprite icon,optional: string innerTextboxColor, optional: string outterTextBoxColor)
        //                    neededStatesFor1:  string[] (optional - states that need to be 1 before we show the textbox),
        //                    neededStatesFor0:  string[] (optional - states that need to be 0 before we show the textbox),
        //                    settingStatesAfterRead:  string[] (optional - states that will be set to true after textbox is read),
        //                    nextTextbox: string (optional - key of Textbox that will be triggered after this textbox),   
        //                    doAfterTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is closed
        //                    doBeforeTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is opened

        // triggered by collider
        addToDictionary(key: "dog_bowl_CPU",
                      text: "Ring... Ring... Ring... Nimm ab das Ding!",
                      character: characters["handy"],
                      //neededStatesFor1: new string[] { "SmartphoneTaken" },
                      //neededStatesFor0: new string[] { "EnteredElevatorFirst" },
                      //settingStatesAfterRead: new string[] { "EnteredElevatorFirst" },
                      //doBeforeTextbox: delegate { AudioManager.instance.Play("phone_ring"); },
                      doAfterTextbox: delegate { AudioManager.instance.Stop("phone_ring"); },
                      nextTextbox: "get_dog_to_CPU");
        addToDictionary(key: "get_dog_to_CPU",
                      text: "Hey, du kannst mich doch nicht einfach im Büro lassen! Erkläre mir bitte den Weg zu dir und pass besser mit den Hundefallen auf!",
                      character: characters["dog"],
                      doAfterTextbox: delegate { textBoxLogic.startUBlockly(2); });

        addToDictionary(key: "plant_01",
                      text: "Eine Pflanze.",
                      character: characters["robot"]);
        addToDictionary(key: "plant_02",
                      text: "Eine Pflanze.",
                      character: characters["robot"]);
        /* already in dictionary (FP)
        addToDictionary(key: "plant_3.0",
                      text: "Eine Pflanze.",
                      character: characters["robot"]); */

        addToDictionary(key: "serverschrank_01",
                      text: "Ein Serverschrank ist die Basis für ein gutes und sicheres IT-Netzwerk.",
                      character: characters["robot"]);
        addToDictionary(key: "serverschrank_02",
                      text: "Ein Serverschrank ist die Basis für ein gutes und sicheres IT-Netzwerk.",
                      character: characters["robot"]);

        addToDictionary(key: "fridge",
                     text: "Hier gibt's was zu futtern.",
                     character: characters["robot"]);

        addToDictionary(key: "screens_01",
                      text: "Hier hat jemand 2000 extrastarke Magnete gekauft...",
                      character: characters["robot"]);
        addToDictionary(key: "screens_02",
                      text: "Hier ist ein Auszug aus den Sicherheitsbestimmungen geöffnet: CPU-Raum-Passwortstrategie: Verwenden Sie 2 etwa gleich lange Wörter und eine Zahl, die Sie sich merken können. Verwenden Sie bei einem Wort nur Großbuchstaben und bei dem anderen nur Kleinbuchstaben. Gehen Sie dann zeichenweise durch die 3 Begriffe und notieren Sie erst jeweils alle ersten, dann alle zweiten Zeichen usw. Aus CAT, red, 110 wird dann Cr1Ae1Td0.",
                      character: characters["robot"]);
        addToDictionary(key: "secure",
                       text: "Gut, dass Sie da sind! Hier läuft alles schief. Irgendwo scheint es ein Energieproblem zu geben und alle tun entweder nichts oder spielen verrückt!",
                       character: characters["securityManager"],
                       nextTextbox: "secure*01");
        addToDictionary(key: "secure*01",
                       text: "Wie können wir helfen?",
                       character: characters["robot"],
                       nextTextbox: "secure*02");
        addToDictionary(key: "secure*02",
                       text: "Um das Virus zu besiegen, muss diese Abteilung wieder laufen. Lokalisieren Sie den Energieengpass!",
                       character: characters["securityManager"]);

        addToDictionary(key: "buildingPlan*01",
                       text: "Jetzt können wir den Aufzug reparieren!",
                       character: characters["robot"]);

        addToDictionary(key: "control_panel",
                       text: "Vielleicht hat jemand einen Passworthinweis hinterlegt.",
                       neededStatesFor0: new string[] { "REDpwTaken" },
                       character: characters["robot"]);
        addToDictionary(key: "noEnergy",
                       text: "Der Computer hat noch keinen Strom. Wir müssen ihn irgendwie mit der Steckdose verbinden.",
                       character: characters["robot"]);

        addToDictionary(key: "wrongPasswordCPU*01",
                 text: "Der Passworthinweis ist bestimmt bei den Rechnern meiner Kolleg*innen zu finden!",
                 neededStatesFor1: new string[] { "REDpwTaken" },
                 character: characters["robot"]);
        addToDictionary(key: "bossPasswordCorrect",
                 text: "Den PC haben wir schon entsperrt.",
                 character: characters["robot"]);

        addToDictionary(key: "doggoHelpsWithPlug*01",
                 text: "Ich kann den Stecker wieder reinstecken. Aber dafür musst du mich steuern! Da ich nicht mehr so viel Öl im Tank habe, kannst du dafür nur 3 Befehle verwenden. Wähle diese also weise mithilfe der Anleitung!",
                 character: characters["dog"],
                 doAfterTextbox: delegate { textBoxLogic.startUBlockly(3); });

        addToDictionary(key: "PWNoteFound",
                       text: "Das könnte ein Passworthinweis für den Computer sein. Den speichere ich mir direkt in meiner Notizen-App.",
                       character: characters["robot"]);
        addToDictionary(key: "PWNoteFoundClickedAgain",
                       text: "Den Passworthinweis habe ich schon in der Notizen-App gespeichert. Den sollte ich mal am Computer ausprobieren.",
                       character: characters["robot"]);

        addToDictionary(key: "orderTool_wrongShopSelected00",
                       text: "Vorsicht!!! Dieser Shop verwendet unsicheres HTTP! Hier sollten wir niemals unsere Kreditkarte verwenden!",
                       character: characters["dog"]);
        addToDictionary(key: "orderTool_resendCode00",
                       text: "Dein Handy vibriert schon wieder. Lies doch endlich mal deine Nachrichten!",
                       character: characters["dog"]);
        addToDictionary(key: "orderTool_wrongCode00",
                       text: "Der Code ist falsch. Schau noch einmal genau hin!",
                       character: characters["dog"]);
        addToDictionary(key: "orderTool_success00",
                       text: "So, die neue SSD-Abteilung ist per Express bestellt, hoffentlich geht das wirklich schnell. Und hoffentlich arbeiten die wirklich leiser als die alten Kollegen.",
                       doAfterTextbox: delegate { GameObject.Find("OrderTool_manager").GetComponent<orderTool_manager>().activate(false); },
                       character: characters["robot"]);
    }

    private void addToDictionary(string key, string text, CharacterClass character, string[] neededStatesFor1 = null, string[] neededStatesFor0 = null, string[] settingStatesAfterRead = null, string nextTextbox = "", Action doAfterTextbox = null, Action doBeforeTextbox = null) // Add Text to Dictionary 
    {
        Texts.Add(key, new TextBoxClass(collider: key, icon: character.icon, innerTextboxColor: character.innerTextboxColor, outterTextBoxColor: character.outterTextBoxColor, characterName: character.name, neededStatesFor1: neededStatesFor1, neededStatesFor0: neededStatesFor0, text: text, settingStatesAfterRead: settingStatesAfterRead, nextTextbox: nextTextbox, doAfterTextbox: doAfterTextbox, doBeforeTextbox: doBeforeTextbox));
    }
}