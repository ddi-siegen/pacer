using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEditor;

public class TextBoxTextsEL
{
    public Dictionary<string, TextBoxClass> Texts = new Dictionary<string, TextBoxClass>(); // Dictionary (better search performance than iterate list) with all Textelements

    public TextBoxTextsEL(Dictionary<string, CharacterClass> characters, TextBoxLogic textBoxLogic)
    {
        //  addToDictionary: 
        //                    key: string (if a gameobject with a 2D collider has the same name, it will trigger the textbox), 
        //                    text: string (text inside the textbox),
        //                    character: charachterClass (string name, optional: Sprite icon,optional: string innerTextboxColor, optional: string outterTextBoxColor)
        //                    neededStatesFor1:  string[] (optional - states that need to be 1 before we show the textbox),
        //                    neededStatesFor0:  string[] (optional - states that need to be 0 before we show the textbox),
        //                    settingStatesAfterRead:  string[] (optional - states that will be set to true after textbox is read),
        //                    nextTextbox: string (optional - key of Textbox that will be triggered after this textbox),   
        //                    doAfterTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is closed
        //                    doBeforeTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is opened

        // Triggered by collider
        addToDictionary(key: "poster_funny",
                        text: "Oh Mann, dass das Poster, das uns beim Schmusen während der \"Mensch-und-Hund-Simulation\" zeigt, schon wieder hier hängt... peinlich!",
                        neededStatesFor0: new string[] { "EmbarrassingPosterClicked" },
                        nextTextbox: "poster_funny2",
                        character: characters["robot"]);
        addToDictionary(key: "poster_funny2",
                        text: "Sowas passiert, wenn man das Passwort, das jeder im Raum einsehen kann, auf dem Router verwendet. Dann sind deine Daten eben nicht sicher. Gut, dass sie kein Bild von dir beim Altölablassen hatten.",
                        character: characters["dog"],
                        settingStatesAfterRead: new string[] { "EmbarrassingPosterClicked" },
                        doAfterTextbox: delegate
                        {
                            //find inActive GameObject
                            GameObject.Find("/UI/Canvas/Poster_funny").SetActive(true);

                            //set Robot Inactive
                            GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = true;
                        });

        addToDictionary(key: "poster_funny*02",
                       text: "Jetzt fällt es mir wieder ein. Nachdem das Schmuseposter aufgehängt wurde, habe ich das WLAN-Passwort sicherer gemacht.",
                       nextTextbox: "poster_funny*03",
                       character: characters["robot"]);

        addToDictionary(key: "poster_funny*03",
                        text: "Besser spät als nie!",
                        character: characters["dog"]);

        addToDictionary(key: "hatch",
                        neededStatesFor0: new string[] { "ScrewdriverTaken" },
                        text: "Lass uns einen Schraubenzieher finden, damit sollten wir diese Klappe aufbekommen!",
                        character: characters["dog"],
                        settingStatesAfterRead: new string[] { "HatchClicked" });

        addToDictionary(key: "hatchPuzzle*01",
                        doBeforeTextbox: delegate { GameObject.FindObjectOfType<MenuPanelManager>().CloseInventoryBackground(); },
                        neededStatesFor1: new string[] { "ScrewdriverTaken" },
                        text: "Das kann doch nicht sein! Da ist NOCH ein Hindernis!",
                        character: characters["robot"],
                        nextTextbox: "hatchPuzzle*02");
        addToDictionary(key: "hatchPuzzle*02",
                        text: "Hier wird eben auf Sicherheit geachtet!",
                        character: characters["dog"],
                        nextTextbox: "hatchPuzzle*03");
        addToDictionary(key: "hatchPuzzle*03",
                        neededStatesFor0: new string[] { "WiringFilePrinted" },
                        text: "Auf deinem Computer gibt es doch eine Datei zur Verkabelung, oder? Vielleicht sollten wir uns diese erstmal ausdrucken und mitnehmen!",
                        character: characters["dog"]);
        addToDictionary(key: "hatchPuzzle*04",
                        text: "Das scheint gestimmt zu haben, jetzt müssen wir aber schnell in den Festplattenraum!",
                        character: characters["dog"],
                        doAfterTextbox: delegate { GameObject.Find("Hatch").transform.Find("HatchUI2").gameObject.SetActive(false); });
        addToDictionary(key: "hatchPuzzle*05",
                        text: "Hmm... ich glaube, da stimmt etwas nicht. Versuch es noch einmal!",
                        character: characters["dog"]);
        addToDictionary(key: "hatchPuzzle*06",
                        text: "Hier haben wir alles erledigt. Du solltest nicht unnötig an den Kabeln herumspielen!",
                        character: characters["dog"]);
        addToDictionary(key: "hatchPuzzle*07",
                        text: "Bist du lebensmüde?! Hast du nicht etwas Wichtiges vergessen?",
                        character: characters["dog"]);

        addToDictionary(key: "elevatorButtons",
                        character: characters["facility_manager"],
                        text: "Du kannst da nicht raus, wir müssen vielleicht erst ein Antivirensystem laufen lassen!");

        addToDictionary(key: "security_dialogue0",
                       text: "Ring... Ring... Ring... Nimm ab das Ding!",
                       character: characters["handy"],
                       neededStatesFor1: new string[] { "SmartphoneTaken" },
                       neededStatesFor0: new string[] { "EnteredElevatorFirst" },
                       settingStatesAfterRead: new string[] { "EnteredElevatorFirst" },
                       doBeforeTextbox: delegate { if (PlayerPrefs.GetInt("currentScene") == 3) AudioManager.instance.Play("phone_ring"); },
                       doAfterTextbox: delegate { if (PlayerPrefs.GetInt("currentScene") == 3) AudioManager.instance.Stop("phone_ring"); },
                       nextTextbox: "security_dialogue0.5");
        addToDictionary(key: "security_dialogue0.5",
                        text: "Wir haben versucht, den schnellen Weg über die Festplatte zu gehen. Da liegt noch eine Installationsdatei, aber es geht keiner ran! Hier funktioniert auch nichts mehr, wir brauchen Hilfe!",
                        nextTextbox: "security_dialogue1",
                        character: characters["securityManager"]);
        addToDictionary(key: "security_dialogue1",
                        text: PlayerPrefs.GetString("characterNameDog")+", wir kommen so nicht weiter, wir müssen sofort in den CPU-Raum!",
                        nextTextbox: "security_dialogue2",
                        character: characters["robot"]);
        addToDictionary(key: "security_dialogue2",
                        text: "Oh je, hoffentlich kommen wir heil aus der Sache raus.", nextTextbox: "security_dialogue3",
                        character: characters["dog"]);
        addToDictionary(key: "security_dialogue3",
                        text: "Als Höherbedienstete haben wir die beste Firewall. An uns kommt das Virus so schnell nicht ran!",
                        character: characters["robot"]);

        addToDictionary(key: "ElevatorFirstTimeUsageDialog*00",
                       text: "Diese Antwort ist korrekt! Bitte wählen Sie ein Stockwerk aus!",
                       character: characters["aufzug"],
                       nextTextbox: "ElevatorFirstTimeUsageDialog*01");
        addToDictionary(key: "ElevatorFirstTimeUsageDialog*01",
                       text: "In Ordnung, Stockwerk 0 erkannt, CPU-Raum.",
                       character: characters["aufzug"],
                       nextTextbox: "ElevatorFirstTimeUsageDialog*02",
                       doAfterTextbox: delegate { GameObject.Find("LevelChanger").GetComponent<LevelChanger>().goToLevelWithElevatorAnimation(1); });
        addToDictionary(key: "ElevatorFirstTimeUsageDialog*02",
                       text: "Wir haben doch noch gar keinen Knopf gedrückt?!",
                       character: characters["robot"],
                       nextTextbox: "ElevatorFirstTimeUsageDialog*03");
        addToDictionary(key: "ElevatorFirstTimeUsageDialog*03",
                       text: "Stockwerk 0 für den CPU-Raum? Das klingt nicht korrekt... Aber wir scheinen nach unten zur Festplattenabteilung zu fahren.",
                       character: characters["dog"],
                       nextTextbox: "ElevatorFirstTimeUsageDialog*04");
        addToDictionary(key: "ElevatorFirstTimeUsageDialog*04",
                       text: "Aber dann ist es doch komisch, dass wir keinerlei Lärm hören... Arbeitet da keiner mehr?",
                       settingStatesAfterRead: new string[] { "ElevatorPanelBlockedAgain" },
                       character: characters["robot"]);

        addToDictionary(key: "ElevatorPanelBlockedAgainTextBox",
                       text: "Wir sollten erst genau wissen, welches Stockwerk zu welchem Raum gehört. Vorher ändern wir hier nichts.",
                       character: characters["robot"]);

        addToDictionary(key: "ElevatorDoorToFpBlocked",
                       text: "Dort kommen wir erstmal nicht weiter.",
                       character: characters["robot"]);

        addToDictionary(key: "ElevatorSecondTimeUsageDialog*00",
                       text: "Diese Antwort ist korrekt! Bitte wählen Sie ein Stockwerk aus!",
                       character: characters["aufzug"],
                       nextTextbox: "ElevatorSecondTimeUsageDialog*01");
        addToDictionary(key: "ElevatorSecondTimeUsageDialog*01",
                        text: "In Ordnung, Stockwerk 5 erkannt, Netzteil-Raum!",
                        character: characters["aufzug"],
                        nextTextbox: "ElevatorSecondTimeUsageDialog*02",
                        doAfterTextbox: delegate { GameObject.Find("LevelChanger").GetComponent<LevelChanger>().goToLevelWithElevatorAnimation(0); });
        addToDictionary(key: "ElevatorSecondTimeUsageDialog*02",
                       text: "Was ist denn hier nur los?",
                       character: characters["robot"],
                       nextTextbox: "ElevatorSecondTimeUsageDialog*03");
        addToDictionary(key: "ElevatorSecondTimeUsageDialog*03",
                        text: "Der Aufzug scheint defekt zu sein, wo er uns wohl hinbringt?",
                        character: characters["dog"],
                        nextTextbox: "ElevatorSecondTimeUsageDialog*04");
        addToDictionary(key: "ElevatorSecondTimeUsageDialog*04",
                        text: "Vielleicht gibt es einen Weg, ihn zu reparieren...",
                        character: characters["robot"],
                        nextTextbox: "ElevatorSecondTimeUsageDialog*05");
        addToDictionary(key: "ElevatorSecondTimeUsageDialog*05",
                        text: "Aber erstmal müssen wir ein neues Festplatten-Team besorgen, um das Antivirensystem zu installieren und über alle Abteilungen laufen zu lassen.",
                        character: characters["dog"],
                        doAfterTextbox: delegate { GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Bestelle ein neues Festplatten-Team!"); });

        addToDictionary(key: "facility_manager*00",
                       text: "Irgendwas ist komisch mit dem Fahrstuhl. Ich komme aber leider nicht drauf. Irgendwie fühle ich mich bei diesen neuen Technologien oft total nutzlos ...",
                       character: characters["facility_manager"],
                       neededStatesFor0: new string[] {"VentilationFixed"},
                       nextTextbox: "facility_manager*01");
        addToDictionary(key: "facility_manager*01",
                       text: "Vielleicht kann ich das Problem ja lösen. Dafür könnten Sie den Lüftungsschacht reparieren, der ist kaputt.",
                       character: characters["robot"],
                       nextTextbox: "facility_manager*02");
        addToDictionary(key: "facility_manager*02",
                       text: "Das wäre super. Ich kümmere mich dafür um den Lüftungsschacht.",
                       character: characters["facility_manager"],
                       doAfterTextbox: delegate { PlayerPrefs.SetInt("VentilationFixed", 1);});

        addToDictionary(key: "facility_manager*03",
                       text: "Ich habe den Lüftungsschacht repariert. Vielleicht hilft das ja.",
                       neededStatesFor1: new string[] {"VentilationFixed"},
                       character: characters["facility_manager"]);

        addToDictionary(key: "panelPuzzle*01a",
                       text: "Hmm... sind das Binärzahlen? Wir müssen die vermutlich den richtigen Stockwerken zuordnen.",
                       character: characters["robot"],
                       nextTextbox: "panelPuzzle*02a");
        addToDictionary(key: "panelPuzzle*02a",
                       text: "Binärzahlen? Vielleicht sollten wir noch einmal den CPU-Raum durchsuchen, um eine Erklärung dafür zu finden.",
                       character: characters["dog"]);
        addToDictionary(key: "panelPuzzle*01b",
                       text: "Hmm... sind das Binärzahlen? Wir müssen die vermutlich den richtigen Stockwerken zuordnen.",
                       character: characters["robot"],
                       nextTextbox: "panelPuzzle*02b");
        addToDictionary(key: "panelPuzzle*02b",
                       text: "Binärzahlen? Da haben wir doch ein Poster gefunden. Gut, dass du gerne im Müll anderer Leute herumwühlst!",
                       character: characters["dog"],
                       nextTextbox: "panelPuzzle*03b");
        addToDictionary(key: "panelPuzzle*03b",
                       text: "...",
                       character: characters["robot"]);
        addToDictionary(key: "panelPuzzle*04",
                       text: "Jetzt passt es! Wir sollten den Aufzug nun korrekt benutzen können.",
                       character: characters["dog"]);
    }

    private void addToDictionary(string key, string text, CharacterClass character, string[] neededStatesFor1 = null, string[] neededStatesFor0 = null, string[] settingStatesAfterRead = null, string nextTextbox = "",  Action doAfterTextbox = null, Action doBeforeTextbox = null) // Add Text to Dictionary 
    {
        Texts.Add(key, new TextBoxClass(collider: key, icon: character.icon, innerTextboxColor: character.innerTextboxColor, outterTextBoxColor: character.outterTextBoxColor, characterName: character.name, neededStatesFor1: neededStatesFor1, neededStatesFor0: neededStatesFor0, text: text, settingStatesAfterRead: settingStatesAfterRead, nextTextbox: nextTextbox, doAfterTextbox: doAfterTextbox, doBeforeTextbox: doBeforeTextbox));
    }
}