using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class TextBoxTextsFP
{
    public Dictionary<string, TextBoxClass> Texts = new Dictionary<string, TextBoxClass>(); // Dictionary (better search performance than iterate list) with all Textelements

    public TextBoxTextsFP(Dictionary<string, CharacterClass> characters, TextBoxLogic textBoxLogic)
    {

        //  addToDictionary: 
        //                    key: string (if a gameobject with a 2D collider has the same name, it will trigger the textbox), 
        //                    text: string (text inside the textbox),
        //                    character: charachterClass (string name, optional: Sprite icon,optional: string innerTextboxColor, optional: string outterTextBoxColor)
        //                    neededStatesFor1:  string[] (optional - states that need to be 1 before we show the textbox),
        //                    neededStatesFor0:  string[] (optional - states that need to be 0 before we show the textbox),
        //                    settingStatesAfterRead:  string[] (optional - states that will be set to true after textbox is read),
        //                    nextTextbox: string (optional - key of Textbox that will be triggered after this textbox),   
        //                    doAfterTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is closed
        //                    doBeforeTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is opened


        addToDictionary(key: "talkToAsr*01",
                      text: "Kein Zutritt!",
                      neededStatesFor0: new string[] { "talkedToAsr" },
                      character: characters["asr"],
                      nextTextbox: "talkToAsr*02");
        addToDictionary(key: "talkToAsr*02",
                      text: "Was ist denn hier passiert?!",
                      character: characters["robot"],
                      nextTextbox: "talkToAsr*03");
        addToDictionary(key: "talkToAsr*03",
                      text: "Es wurden 2000 extrastarke Magnete über ein Datenpaket eingeschleust und die meisten Datensätze sind zerstört worden. Ein Aufräumtrupp wurde schon bestellt!",
                      character: characters["asr"],
                      nextTextbox: "talkToAsr*04");
        addToDictionary(key: "talkToAsr*04",
                      text: "Ist das wegen des Virus passiert?",
                      character: characters["robot"],
                      nextTextbox: "talkToAsr*05");
        addToDictionary(key: "talkToAsr*05",
                      text: "Darüber gibt es keine genauen Informationen. Wir benötigen allerdings dringend ein neues Team, sobald die Aufräumarbeiten erledigt sind.",
                      character: characters["asr"],
                      nextTextbox: "talkToAsr*06");
        addToDictionary(key: "talkToAsr*06",
                      text: "Ich könnte ein neues Team bestellen – aber leider ist unsere Kreditkarte wegen des Zwischenfalls ungültig.",
                      character: characters["robot"],
                      nextTextbox: "talkToAsr*07");
        addToDictionary(key: "talkToAsr*07",
                       text: "Da haben Sie Glück, eine Kreditkarte habe ich im Chaos retten können. Hier, bitte! Am Computer im CPU-Raum können Sie noch Bestellungen aufgeben.",
                       doAfterTextbox: delegate { GameObject.FindObjectOfType<InventoryManager>().FillInventory("creditCard"); PlayerPrefs.SetInt("creditCardTaken", 1); },
                       //doAfterTextbox: ToDo: Kreditkarte hinzufügen
                       character: characters["asr"],
                       nextTextbox: "talkToAsr*08");
        addToDictionary(key: "talkToAsr*08",
                       text: "Vielen Dank! Ich werde mich gleich darum kümmern.",
                       character: characters["robot"],
                       settingStatesAfterRead: new string[] { "talkedToAsr" },
                       doAfterTextbox: delegate { GameObject.FindObjectOfType<LevelChanger>().FadeToLevel(3); });
        addToDictionary(key: "FPNotYetRepaired",
                       text: "Was sollen wir dort? Da ist noch keiner, da wir noch niemanden bestellt haben.",
                       character: characters["robot"]);

        addToDictionary(key: "angryAsr*01",
                       text: "WAS AN DEM VERBOTSSCHILD HINTER MIR IST NICHT ZU VERSTEHEN?",
                       character: characters["angry_asr"],
                       nextTextbox: "angryAsr*02");
        addToDictionary(key: "angryAsr*02",
                       text: "Was unser wütender Roboterfreund nicht weiß: Ich kann seine Handydetektoren deaktivieren. Ähähähä!",
                       character: characters["dog"],
                       doAfterTextbox: delegate { GameObject.Find("/UI/Canvas/AngryAsrOverlay").SetActive(false); });

        addToDictionary(key: "sortingPuzzleTalkToRRFirst",
                       text: "Bevor ich hier etwas mache, sollte ich mit dem Rezeptionsroboter sprechen.",
                       character: characters["robot"]);
        addToDictionary(key: "sortingPuzzle*01",
                       text: "Dann wollen wir doch mal sehen. Aber... irgendetwas stimmt hier nicht.",
                       character: characters["robot"],
                       nextTextbox: "sortingPuzzle*02");
        addToDictionary(key: "sortingPuzzle*02",
                       text: "Wäre ja auch zu schön, wenn in diesem Gebäude etwas auf Anhieb funktionieren würde.",
                       character: characters["dog"],
                       doAfterTextbox: delegate { GameObject.Find("SortingPuzzle").transform.Find("PuzzleUI").gameObject.SetActive(true); });
        addToDictionary(key: "sortingPuzzle*03a",
                       text: "Ich habe keine Ahnung, was wir hier tun sollen. Die Liste sieht durcheinander aus. Vielleicht finden wir irgendwo ein Buch über Sortieralgorithmen?",
                       character: characters["robot"]);
        addToDictionary(key: "sortingPuzzle*03b",
                       text: "Ich denke, wir sollen die Zahlen aufsteigend sortieren. Hier könnte uns das Buch über Sortieralgorithmen weiterhelfen.",
                       character: characters["robot"]);
        addToDictionary(key: "sortingPuzzle*04",
                       text: "Das sieht richtig aus. Der Algorithmus sollte jetzt wieder korrekt funktionieren. Ich frage mich aber, ob es schnellere Wege gibt, eine Liste zu sortieren...",
                       character: characters["robot"],
                       nextTextbox: "sortingPuzzle*05");
        addToDictionary(key: "sortingPuzzle*05",
                       text: "Die Bücherregale sind jetzt viel ordentlicher. So können wir besser suchen!",
                       character: characters["dog"],
                       doAfterTextbox: delegate { GameObject.Find("SortingPuzzle").transform.Find("PuzzleUI").gameObject.SetActive(false); });
        addToDictionary(key: "sortingPuzzleSolved",
                       text: "Das Problem haben wir schon gelöst.",
                       character: characters["robot"]);

        addToDictionary(key: "searchPuzzle",
                       text: "Los geht die Suche!",
                       character: characters["robot"],
                       doAfterTextbox: delegate { GameObject.Find("SearchPuzzle").transform.Find("PuzzleUI").gameObject.SetActive(true); });
        addToDictionary(key: "searchPuzzleRightAnswer",
                       text: "Aha, da hast du dich also versteckt, flüchtiges Zahlenwesen!",
                       character: characters["dog"],
                       nextTextbox: "searchPuzzleRightAnswer*02");
        addToDictionary(key: "searchPuzzleRightAnswer*02",
                       text: "Die richtige Datei ist im zweiten Regal unten rechts.",
                       character: characters["robot"],
                       doAfterTextbox: delegate { GameObject.Find("SearchPuzzle").transform.Find("PuzzleUI").gameObject.SetActive(false); } );
        addToDictionary(key: "searchPuzzleWrongAnswer",
                       text: "Ich glaube, wir sind irgendwo falsch abgebogen. Versuchen wir es noch einmal?",
                       character: characters["dog"],
                       doAfterTextbox: delegate { GameObject.Find("SearchPuzzle").transform.Find("PuzzleUI").GetComponent<SearchPuzzleManager>().ButtonClickedReset(); });

        addToDictionary(key: "unsortedShelf",
                       text: "Hier ist so ein Durcheinander. So finde ich niemals die richtige Datei.",
                       character: characters["robot"]);

        addToDictionary(key: "antiVirusBookTaken",
                       text: "Da ist die Antivirendatei. Endlich!",
                       character: characters["robot"]);
        addToDictionary(key: "antiVirusBookNotTaken",
                       text: "Das ist das falsche Regal.",
                       character: characters["robot"]);
        addToDictionary(key: "antiVirusBookAlreadyTaken",
                       text: "Wir haben die Datei schon.",
                       character: characters["robot"]);

        addToDictionary(key: "reception_roboter*01",
                       text: "Hallo, ich bin der CEO des Gesamtsystems.",
                       character: characters["robot"],
                       nextTextbox: "reception_roboter*02");
        addToDictionary(key: "reception_roboter*02",
                       text: "Psst! Das geht doch auch sicher leiser! Benötigen Sie eine Auskunft?",
                       character: characters["asr"],
                       nextTextbox: "reception_roboter*03");
        addToDictionary(key: "reception_roboter*03",
                       text: "Wir möchten gerne das Antivirensystem installieren, könnten Sie die Installationsdatei an den Prozessor schicken, damit dieser die Software installieren und ausführen kann?",
                       character: characters["dog"],
                       nextTextbox: "reception_roboter*04");
        addToDictionary(key: "reception_roboter*04",
                       text: "Am besten suchen Sie die entsprechenden Datenpakete. Wenn Sie sie gefunden haben, kommen Sie wieder zu mir!",
                       character: characters["asr"],
                       settingStatesAfterRead: new string[] { "TalkedToRR_1" },
                       doAfterTextbox: delegate { GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Sortiere die Daten, damit wir das Datenpaket besser suchen können!"); });

        addToDictionary(key: "reception_roboter*alreadyTalked",
                       text: "Am besten suchen Sie die entsprechenden Datenpakete. Wenn Sie sie gefunden haben, kommen Sie wieder zu mir!",
                       character: characters["asr"]);

        addToDictionary(key: "reception_roboter*AntiVirusBookTaken*00",
                       text: "Wir haben die Antivirendatei gefunden!",
                       character: characters["robot"],
                       nextTextbox: "reception_roboter*AntiVirusBookTaken*01");
        addToDictionary(key: "reception_roboter*AntiVirusBookTaken*01",
                       text: "Endlich! Jetzt können wir das System vom Virus befreien!",
                       character: characters["asr"],
                       doAfterTextbox: delegate { GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel(6); });
        // END OF PACER

        addToDictionary(key: "fp_worker_0_dialogue_unsolved",
                        text: "Hey, in dem Chaos finde ich nicht die richtigen Bücher! Vielleicht kannst du mir ein paar Fragen beantworten?",
                        character: characters["facility_manager"],// need new icon
                        doAfterTextbox: delegate {textBoxLogic.startQuiz("fpQuiz");});
        addToDictionary(key: "fp_worker_0_dialogue_solved",
                        text: "Nochmal vielen Dank für das Beantworten meiner Fragen.",
                        character: characters["facility_manager"]);
        addToDictionary(key: "fp_worker_1_dialogue_unsolved",
                        text: "Hallo, dich habe ich doch schon beim Programmieren beobachtet. Bei dieser Aufgabe komme ich nicht weiter. Mir wurde gesagt, dass ich nur 8 Blöcke benutzen darf und deswegen Schleifen (wiederhole... bis...) und bedingte Anweisungen (mit den Schlüsselwörtern: wenn... dann... sonst...) benutzen muss. Kannst du mir vielleicht helfen?",
                        character: characters["facility_manager"],// need new icon
                        doAfterTextbox: delegate {textBoxLogic.startUBlockly(4);});
        addToDictionary(key: "fp_worker_1_dialogue_solved",
                        text: "Puh, das war echt kniffelig. Du bist echt gut in der Programmierung!",
                        character: characters["facility_manager"]);
        addToDictionary(key: "fp_worker_2_dialogue_0",
                       text: "Hi, kannst du uns helfen?",
                       character: characters["robot"],
                       nextTextbox: "fp_worker_2_dialogue_1"); 
        addToDictionary(key: "fp_worker_2_dialogue_1",
                       text: "Das kommt drauf an, was ist denn los?",
                       character: characters["facility_manager"],
                       nextTextbox: "fp_worker_2_dialogue_2");
        addToDictionary(key: "fp_worker_2_dialogue_2",
                       text: "Wir brauchen die Installationsdatei des Antivirensystems und müssen sie ganz dringend an den Prozessor schicken.",
                       character: characters["robot"],
                       nextTextbox: "fp_worker_2_dialogue_3"); 
        addToDictionary(key: "fp_worker_2_dialogue_3",
                       text: "Puh, die ist auf jeden Fall nicht durch meine Hände gelaufen. Falls sie hier irgendwo sein sollte, dann findet ihr sie über die Suchfunktion. Allerdings müsstet ihr den Sortieralgorithmus vermutlich erst noch reparieren, sonst dauert die Suche viel zu lang. Wir haben immerhin Milliarden von Dateien ...",
                       character: characters["facility_manager"],
                       nextTextbox: "fp_worker_2_dialogue_4"); 
        addToDictionary(key: "fp_worker_2_dialogue_4",
                       text: "Ich glaube, das können wir schaffen! Vielleicht finden wir hier im Raum ja Hinweise, wie das geht.",
                       character: characters["dog"]); 

        addToDictionary(key: "just_a_shelf",
                       text: "Noch mehr Bücher! Aber hier ist nichts Interessantes dabei.",
                       character: characters["dog"]); 
                                                                            
        addToDictionary(key: "plant_2.0",
                       text: "Eine Pflanze.",
                       character: characters["robot"]);
        addToDictionary(key: "plant_3.0",
                       text: "Eine Pflanze.",
                       character: characters["robot"]);

        addToDictionary(key: "vending_machine",
                       text: "Leckere Getränke zu wenig schmackhaften Preisen.",
                       character: characters["robot"]);

        addToDictionary(key: "table_football",
                       text: "Vielleicht später.",
                       character: characters["robot"]);
    }

    private void addToDictionary(string key, string text, CharacterClass character, string[] neededStatesFor1 = null, string[] neededStatesFor0 = null, string[] settingStatesAfterRead = null, string nextTextbox = "",  Action doAfterTextbox = null, Action doBeforeTextbox = null) // Add Text to Dictionary 
    {
        Texts.Add(key, new TextBoxClass(collider: key, icon: character.icon, innerTextboxColor: character.innerTextboxColor, outterTextBoxColor: character.outterTextBoxColor, characterName: character.name, neededStatesFor1: neededStatesFor1, neededStatesFor0: neededStatesFor0, text: text, settingStatesAfterRead: settingStatesAfterRead, nextTextbox: nextTextbox, doAfterTextbox: doAfterTextbox, doBeforeTextbox: doBeforeTextbox));
    }
}