using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;


public class TextBoxTextsHO
{
    public Dictionary<string, TextBoxClass> Texts = new Dictionary<string, TextBoxClass>(); // Dictionary (better search performance than iterate list) with all Textelements

    public TextBoxTextsHO(Dictionary<string, CharacterClass> characters, TextBoxLogic textBoxLogic)
    {

        //  addToDictionary: 
        //                    key: string (if a gameobject with a 2D collider has the same name, it will trigger the textbox), 
        //                    text: string (text inside the textbox),
        //                    character: charachterClass (string name, optional: Sprite icon,optional: string innerTextboxColor, optional: string outterTextBoxColor)
        //                    neededStatesFor1:  string[] (optional - states that need to be 1 before we show the textbox),
        //                    neededStatesFor0:  string[] (optional - states that need to be 0 before we show the textbox),
        //                    settingStatesAfterRead:  string[] (optional - states that will be set to true after textbox is read),
        //                    nextTextbox: string (optional - key of Textbox that will be triggered after this textbox),   
        //                    doAfterTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is closed
        //                    doBeforeTextbox: Action (optional - delegate (no parameter and no return) that will be called after the textbox is opened

        // 1.0.0 Start to the game: They trigger each other after being read
        addToDictionary(key: "Start_1_0_0_0",
                        text: "Bei dem Lärm kann sich ja keiner konzentrieren, hoffentlich ist das gleich vorbei. Ich sollte dringend diese neu ausgebildeten Festplatten-Kräfte, die sich SSD nennen, bestellen. Die sollen schneller und geräuschlos arbeiten. Wo ist denn meine Kreditkarte?",
                        nextTextbox: "Start_1_0_0_1",character: characters["robot"]);
        addToDictionary(key: "Start_1_0_0_1",
                        text: "Roboter X4-13 wurde leider entlassen. Bitte stellen Sie sicher, dass Sie Bestellungen mit persönlichen oder vertraulichen Daten nur über Webseiten tätigen, die das sicherere, da verschlüsselte, \"HTTPS\" statt \"HTTP\" verwenden. Es wurden bereits neue Kreditkarten beantragt, diese können Sie im Festplattenraum abholen. Die alten funktionieren ab sofort nicht mehr! Vielen Dank!",
                        nextTextbox: "Start_1_0_0_2", character: characters["durchsage"]);
        addToDictionary(key: "Start_1_0_0_2",
                        text: "Na toll, dann muss ich das auf später verschieben. Dabei weiß doch jedes Kind, dass das \"S\" in HTTPS für Secure steht. HTTP ist nur ohne Dateneingaben zu verwenden.",
                        character: characters["robot"],
                        doAfterTextbox: delegate { GameObject.Find("NameSetterManager").GetComponent<NameSetterManager>().enableDogNameSetter();} // after name is set Start_1_0_0_3 will trigger
                        );
        addToDictionary(key: "Start_1_0_0_3",
                        text: "Notfall! Du musst schnell etwas unternehmen! Im gesamten Gebäude verbreitet sich ein Virus. Die Dateien und Mitarbeiter sind schon betroffen! Wir müssen sofort ein Antivirensystem einschalten!",
                        nextTextbox: "Start_1_0_0_4", character: characters["dog"]);
        addToDictionary(key: "Start_1_0_0_4",
                        text: "Das ist ja furchtbar, ich rufe sofort an! Wo ist denn mein Smartphone?",
                        character: characters["robot"],
                        nextTextbox: "Start_1_0_0_5"
                        );
        addToDictionary(key: "Start_1_0_0_5",
                        text: "Das liegt doch direkt neben dir auf dem Tisch! Aber kannst du mir helfen, den Weg zum lecker-süffigen Öl zu finden? Durch das Verbinden von Programmcode-Blöcken musst du mich zum Ziel steuern!",
                        character: characters["dog"],
                        doAfterTextbox: delegate {
                                                    textBoxLogic.startUBlockly(0);
                                                 } // after solved blockly doggo will walk to position where he can start to eat
                        );
        // Triggered by collider
        addToDictionary(key: "ventilation_broken",
                        text: "Das muss bei Gelegenheit mal wieder befestigt werden... Aber nicht jetzt.",
                        character: characters["robot"]);
        addToDictionary(key: "plant_1",
                        text: "Eine Pflanze.",
                        character: characters["robot"]);
        addToDictionary(key: "plant_2",
                        text: "Eine Pflanze.",
                        character: characters["robot"]);
        addToDictionary(key: "shelf",
                        text: "Ich habe jetzt keine Zeit zu lesen. Ich sollte mich lieber um wichtigere Dinge kümmern!",
                        character: characters["robot"]);
        addToDictionary(key: "conf_Table",
                        text: "Ein großer Konferenztisch, der viel Platz bietet. Er wird nur selten benutzt. Aber was, wenn mich jemand aus dem letzten Meeting anruft?",
                        character: characters["robot"]);
        addToDictionary(key: "dog_bowl",
                        text: "Bald brauche ich neues Zeug!",
                        character: characters["dog"]);

        addToDictionary(key: "tool_case",
                text: "Ich glaube, ganz unten im Werkzeugkasten hat sich ein Schraubenzieher versteckt. Wenn du mir den Weg zeigst, hole ich ihn dir.",
                character: characters["dog"],
                doAfterTextbox: delegate {
                    textBoxLogic.startUBlockly(1);
                }
                );
        addToDictionary(key: "tool_case_taken_message",
                        neededStatesFor1: new string[]{ "ScrewdriverTaken"},
                        text: "Den Schraubenzieher haben wir schon dabei!",
                        character: characters["dog"]);
  

        addToDictionary(key: "router",
                       text: "Der Router wurde mit einem neuen Passwort versehen!",
                       character: characters["durchsage"],
                       neededStatesFor1: new string[] { "EmbarrassingPosterClicked", "SSIDToNotes" },
                       nextTextbox: "router1");

        addToDictionary(key: "router1",
                       text: "Jetzt fällt es mir wieder ein. Nachdem das Schmuseposter aufgehängt wurde, habe ich das WLAN-Passwort sicherer gemacht.",
                       nextTextbox: "router2",
                       character: characters["robot"]);

        addToDictionary(key: "router2",
                        text: "Besser spät als nie!",
                        character: characters["dog"]);

        addToDictionary(key: "verkabelung*01",
                        text: "Super, das ist die Datei! Ich sollte sie doch drucken... Aber der Drucker funktioniert nicht!",
                        character: characters["robot"]);

        addToDictionary(key: "windowsScreen*01",
                        neededStatesFor0: new string[] { "SmartphoneTaken" },
                        text: "Mhh... also so kann ich mir die Notiz nicht notieren. Dafür fehlt mir wohl noch ein Gegenstand!",
                        character: characters["robot"]);

        addToDictionary(key: "wrongPassword*01",
                       text: "Es gab da doch so einen ganz bestimmten Weg, ein sicheres Passwort zu erstellen... Genau dafür habe ich mir doch die Erinnerung im Büro gelassen! Und den Satz hatte ich mir auf dem Laptop abgespeichert...",
                       character: characters["robot"],
                       doAfterTextbox: delegate { GameObject.Find("Printer").GetComponent<PrinterManager>().wrongPasswordCounter = 0; });

        addToDictionary(key: "correctPassword*01",
                       text: "Der Drucker ist wieder mit dem Netzwerk verbunden. Belassen wir es dabei!",
                       character: characters["robot"]);

        addToDictionary(key: "wiringFileAlreadyPrinted",
                       text: "Ein Ausdruck reicht uns!",
                       character: characters["robot"]);

        addToDictionary(key: "PCPuzzle*01",
                       text: "Huch, ein Rätsel!",
                       character: characters["robot"],
                       doAfterTextbox: delegate { GameObject.Find("PCPuzzle").transform.Find("PuzzleUI").gameObject.SetActive(true); });
        addToDictionary(key: "PCPuzzle*02",
                       text: "Das war richtig! Zur Belohnung haben wir eine Münze für die Quiz-Machine im CPU-Raum bekommen!",
                       character: characters["robot"],
                       doAfterTextbox: delegate { PlayerPrefs.SetInt("quizMachineMoney", PlayerPrefs.GetInt("quizMachineMoney") + 1); },
                       nextTextbox: "PCPuzzle*03");
        addToDictionary(key: "PCPuzzle*03",
                       text: "Sind das diese traumhaften IT-Gehälter?",
                       character: characters["dog"]);
        addToDictionary(key: "PCPuzzleSolved",
                       text: "Ich glaube nicht, dass es besonders sinnvoll ist, das Rätsel noch einmal zu lösen.",
                       character: characters["dog"]);

        addToDictionary(key: "SmartphoneNotTaken",
                        text: "Ich sollte das Smartphone mitnehmen, damit ich unterwegs erreichbar bin.",
                        character: characters["robot"]);

    }
    private void addToDictionary(string key, string text, CharacterClass character, string[] neededStatesFor1 = null, string[] neededStatesFor0 = null, string[] settingStatesAfterRead = null, string nextTextbox = "",  Action doAfterTextbox = null, Action doBeforeTextbox = null) // Add Text to Dictionary 
    {
        Texts.Add(key, new TextBoxClass(collider: key, icon: character.icon, innerTextboxColor: character.innerTextboxColor, outterTextBoxColor: character.outterTextBoxColor, characterName: character.name, neededStatesFor1: neededStatesFor1, neededStatesFor0: neededStatesFor0, text: text, settingStatesAfterRead: settingStatesAfterRead, nextTextbox: nextTextbox, doAfterTextbox: doAfterTextbox, doBeforeTextbox: doBeforeTextbox));
    }
}