using UnityEngine;
using UnityEngine.UI;

public class TextUpdater : MonoBehaviour
{
    private void Start()
    {
        Text[] texts = GetComponentsInChildren<Text>(includeInactive: true);
        // change all instances of {characterNameDog} in texts in all child objects to characterNameDog
        foreach (Text text in texts)
        {
            text.text = text.text.Replace("{characterNameDog}", PlayerPrefs.GetString("characterNameDog"));
        }
    }
}