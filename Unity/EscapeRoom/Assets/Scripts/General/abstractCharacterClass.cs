using UnityEngine;

public abstract class abstractCharacterClass : MonoBehaviour // abstract class for our Character for each room.
{
    public abstract void teleport(Vector3 vector);
    public abstract void walkToPosition(Vector3 vector);
}