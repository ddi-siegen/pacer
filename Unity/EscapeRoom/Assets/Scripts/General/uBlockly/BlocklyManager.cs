// NOTICE: THIS FILE WAS ADDED BY SVEN JACOBS
using UBlocklyGame.Maze;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
public class BlocklyManager : MonoBehaviour
{
    [Header("Level that will be loaded")]
    [SerializeField] int GameLevel;

    [Header("UI-Elements")]
    [SerializeField] GameObject uBlockly;

    NavMeshAgent agent;
    [SerializeField] GameObject LooseScreen;
    [SerializeField] GameObject WinScreen;
    [SerializeField] GameObject TooManyBlocksScreen;
    [SerializeField] GameObject LevelOverviewScreen;
    GameObject menuPanel; // Settings and Inventory
    [SerializeField] TextMeshProUGUI allowedBlocksText;
    [SerializeField] TextMeshProUGUI recordText;
    [SerializeField] TextMeshProUGUI levelText;

    [SerializeField] GameObject levelButton;
    [SerializeField] GameObject levelButtonParent;
    [SerializeField] GameObject HintButton;

    [SerializeField] GameObject BookButton;
    [SerializeField] GameObject HintTextBox;
    [SerializeField] GameObject Workspace;
    [SerializeField] GameObject ExitButton;

    [SerializeField] GameObject Book;
    private bool isCurrentBlocksTooHigh = false;

    private void Awake()
    {
        agent = GameObject.Find("Robot").GetComponent<NavMeshAgent>();
        menuPanel = GameObject.Find("MenuPanel");
    }
    void Start()
    {
        checkForHintButton();
        if (GameLevel == 0) // deactivate Exitbutton for first level so Player has to solve it to continue
        {
            ExitButton.SetActive(false); ; 
        }
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        GameLevel = levelManager.getCurrentLevel();

    }

    private List<GameObject> ListWithLevelButtonObjects = new List<GameObject>();

    public void endBlockly()
    {
        Workspace.GetComponent<UBlockly.UGUI.WorkspaceView>().stopCode();
        GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        //menuPanel.SetActive(true);
        FindObjectOfType<MenuPanelManager>(true).SetMenuPanelActive(true);
        WinScreen.SetActive(false);
        LooseScreen.SetActive(false);
        TooManyBlocksScreen.SetActive(false);
        uBlockly.SetActive(false);
        agent.isStopped = false;
        doStuffAfterLevel();
        Destroy(FindObjectOfType<MazeController>(true));
        Destroy(GameObject.Find("CodeRunners"));
        Destroy(uBlockly);
    }

    public void hardReset()
    {
        Workspace.GetComponent<UBlockly.UGUI.WorkspaceView>().stopCode();
        Destroy(GameObject.Find("CodeRunners"));
        GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
    }

    public void showLooseScreen(bool boolean)
    {
        if (boolean == false) // Reset Game if player deactivates loosescreen
        {
            GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        }
        Workspace.GetComponent<UBlockly.UGUI.WorkspaceView>().stopCode();
        GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        LooseScreen.SetActive(boolean);
    }


    public void showTooManyBlocksScreen(bool boolean)
    {
        if (boolean == false)
        {
            GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        }
        Workspace.GetComponent<UBlockly.UGUI.WorkspaceView>().stopCode();
        GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        TooManyBlocksScreen.SetActive(boolean);
    }

    public void checkForHintButton(bool incrementTries = false)
    {
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        if (incrementTries)
        {
            levelManager.addWrongTry();
        }
        print("wrong trys for this level:" + levelManager.getWrongTry());
        if (levelManager.getWrongTry() >= 2)
        {
            HintButton.SetActive(true);
        }
        else
        {
            HintButton.SetActive(false);
        }
    }

    public void displayHint(bool boolean)
    {
        HintTextBox.SetActive(boolean);
        if (boolean)
        {
            uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
            HintTextBox.transform.Find("HintTextBoxText").GetComponent<TMPro.TextMeshProUGUI>().text = "Tipp: \n\n" + levelManager.getCurrentLevelClass().hint;
        }
    }

    public void showLevelOverviewScreen(bool boolean)
    {
        if (boolean == false) // Reset Game if player deactivates loosescreen
        {
            GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        }
        LooseScreen.SetActive(false);
        WinScreen.SetActive(false);
        LevelOverviewScreen.SetActive(boolean);
        if (boolean) // if activated
        {
            foreach (GameObject child in ListWithLevelButtonObjects) // clear old buttons in grif
            {
                GameObject.Destroy(child.gameObject);
            }
            ListWithLevelButtonObjects.Clear();
            uBlocklyLevelManager levelManager = new uBlocklyLevelManager();

            int solvedLevelCount = 0;
            //print("levevount: " + levelManager.getLevelCount());
            for (int i = 0; i < levelManager.getLevelCount(); i++)  // create a new button for each level that is already solved
            {
                if (levelManager.levelStorage.AllLevels[i].isSolved)
                {
                    solvedLevelCount++;
                    addLevelButton(i, true); // add a button for every solved level
                    //print("Level " + i + " solved");
                }
            }
        }
        levelButton.SetActive(false);
    }

    public void showWinScreen(bool boolean)
    {
        if (!LooseScreen.activeSelf) // only show winscreen if not already lost 
                                     // this is needed for situations where the avatar touches the winning position and then still moves to somewhere else
        {
            uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
            levelManager.newRecord(getBlockCount());
            WinScreen.SetActive(boolean);
        }
        Workspace.GetComponent<UBlockly.UGUI.WorkspaceView>().stopCode();
        GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
    }

    public int getGameLevel()
    {
        return GameLevel;
    }

    public int getBlockCount()
    {
        int blockCount = GameObject.Find("BlocklyCanvas/Workspace").GetComponent<UBlockly.UGUI.WorkspaceView>().Workspace.GetAllBlocks().Count; // amount of blocks in workspace
        return blockCount;
    }


    public void refreshBlockCountText()
    {
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        int currBlocks = getBlockCount();
        int maxBlocks = levelManager.getCurrentLevelClass().maxBlocks;
        if (currBlocks > maxBlocks)
        {
            isCurrentBlocksTooHigh = true;
            allowedBlocksText.color = new Color(225, 0, 0, 1);
        }
        else
        {
            isCurrentBlocksTooHigh = false;
            allowedBlocksText.color = new Color(0, 0, 0, 225);
        }

        allowedBlocksText.text = "Blöcke: " + currBlocks + " von maximal " + maxBlocks;
    }

    public void refreshRecordAndLevelText()
    {
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        levelText.text = "Level: " + levelManager.getCurrentLevel();
        if (levelManager.getCurrentLevelClass().recordBox != 0)
        {
            recordText.text = "Rekord: " + levelManager.getCurrentLevelClass().recordBox;
        }
        else
        {
            recordText.text = "Rekord: Noch nicht gelöst";
        }
    }

    public void goToNextLevel()
    {
        GameLevel++;
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        levelManager.setCurrentLevel(levelManager.getCurrentLevel() + 1);
        WinScreen.SetActive(false);
        GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        refreshBlockCountText();
        refreshRecordAndLevelText();
        checkForHintButton();
    }
    public void goToLevel(int level)
    {
        FindObjectOfType<MenuPanelManager>(true).SetMenuPanelActive(false);
        GameLevel = level;
        Debug.Log("GoTOLEvel: " + level);
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        levelManager.setCurrentLevel(level);
        WinScreen.SetActive(false);
        GameObject.Find("MazeGame").GetComponent<MazeController>().Reset();
        refreshBlockCountText();
        refreshRecordAndLevelText();
        checkForHintButton();
    }

    public void setToCurrLevel()
    {
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        GameLevel = levelManager.getCurrentLevel();
        if (GameLevel!=0)
        {
            goToLevel(GameLevel);
        }

    }

    public bool getIsCurrentBlocksTooHigh()
    {
        return isCurrentBlocksTooHigh;
    }
    public void addLevelButton(int level, bool solved = true)
    {
        GameObject duplicate = Instantiate(levelButton);
        duplicate.SetActive(true);
        //the duplicate is now the child of notes Parent
        duplicate.transform.SetParent(levelButtonParent.transform);
        //scale it properly
        duplicate.gameObject.transform.localScale = new Vector3(1, 1, 1);

        duplicate.transform.Find("Button").GetComponentInChildren<TextMeshProUGUI>().text = "" + level;
        duplicate.transform.Find("Button").GetComponentInChildren<Button>().onClick.AddListener(() => { goToLevel(level); LevelOverviewScreen.SetActive(false); });
        ListWithLevelButtonObjects.Add(duplicate);
        /*if (!isEditable)
        {
            duplicate.transform.Find("InputField").GetComponent<InputField>().interactable = false;
            duplicate.transform.Find("DeleteButton").gameObject.SetActive(false);
        }
        else
        {
            duplicate.transform.Find("DeleteButton").GetComponent<Button>().onClick.AddListener(() => { Destroy(duplicate); deleteAndReload(number); });
        }

        if (!init)
        {
            noteManager.newNote(content, isNew, isEditable);
        }

        if (isNew)
        {
            duplicate.transform.Find("Circle").GetComponent<Image>().sprite = noteNotReadCircle;
        }
        else
        {
            duplicate.transform.Find("Circle").GetComponent<Image>().sprite = noteReadCircle;
        }
        ListWithNoteObjects.Add(duplicate); */
    }

    public void activate()
    {
        uBlockly.SetActive(true);
        agent.isStopped = true;
    }

    private void doStuffAfterLevel()
    {
        uBlocklyLevelManager levelManager = new uBlocklyLevelManager();
        int currLevel = levelManager.getCurrentLevel();
        if (levelManager.checkUblocklyLevelSolved(currLevel))
        {
            switch(currLevel) // do Stuff depending on solved Level
            {
                case 0:
                    GameObject.Find("Doggo").GetComponent<DoggoScript>().startDrinking(new Vector3(-1.41999996f, 6.76999998f, 0));
                    GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Sammele das Smartphone auf!");
                    PlayerPrefs.SetInt("ChapterOneActive", 1);
                    break;
                case 1: FindObjectOfType<InventoryManager>(true).FillInventory("tool_case_done");
                    break;
                case 2:
                    PlayerPrefs.SetString("currDogScene", "CPU");
                    FindObjectOfType<DoggoScript>(true).startDrinking(new Vector3(-17.3600006f, -6.9000001f, 0));
                    //GameObject.Find("Doggo").GetComponent<DoggoScript>().startDrinking(new Vector3(-17.3600006f, -6.9000001f, 0));
                    break;
                case 3:
                    PlayerPrefs.SetInt("PlugInserted", 1);
                    FindObjectOfType<LevelManagerCPU>(true).insertPlugAndActivateMachine();
                    break;
                default:
                    break;
            }
        }
    }

    public void openHelpBook()
    {
        Book.GetComponentInChildren<BookManager>().openBook("uBlockly");
        Instantiate(Book);
    }

}