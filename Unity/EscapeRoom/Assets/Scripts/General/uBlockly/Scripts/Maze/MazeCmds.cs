﻿// NOTICE: THIS FILE WAS CHANGED BY SVEN JACOBS
using System.Collections;
using UBlockly;
using UnityEngine;

namespace UBlocklyGame.Maze
{
    [CodeInterpreter(BlockType = "maze_move")]
    public class Maze_Move_Cmdtor : EnumeratorCmdtor
    {
        protected override IEnumerator Execute(Block block)
        {
            yield return MazeController.Instance.DoMoveForward();
        }
    }

    [CodeInterpreter(BlockType = "maze_turn")]
    public class Maze_Turn_Cmdtor : EnumeratorCmdtor
    {
        protected override IEnumerator Execute(Block block)
        {
            string dirStr = block.GetFieldValue("DIRECTION");
            Direction dir = dirStr.Equals("LEFT") ? Direction.Left : Direction.Right;
            yield return MazeController.Instance.DoTurn(dir);
        }
    }

    [CodeInterpreter(BlockType = "if_bool_access")]
    public class Maze_BoolAccess_Cmdtor : ValueCmdtor
    {
        protected override DataStruct Execute(Block block)
        {
            string dirStr = block.GetFieldValue("ACCESS");
            Direction dir = Direction.Front;
            switch (dirStr)
            {
                case "FRONT":
                    dir = Direction.Front;
                    break;
                case "RIGHT":
                    dir = Direction.Right;
                    break;
                case "LEFT":
                    dir = Direction.Left;
                    break;
            }
            bool access = MazeController.Instance.DoCheckAccess(dir);
            return new DataStruct(access);
        }
    }
    
    [CodeInterpreter(BlockType = "if_reach_terminal")]
    public class Maze_ReachTerminal_Cmdtor : ValueCmdtor
    {
        protected override DataStruct Execute(Block block)
        {
            bool reach = MazeController.Instance.DoCheckAccomplish();
            Debug.Log("hi" + reach);
            return new DataStruct(reach);
        }
    }
}