﻿// NOTICE: THIS FILE WAS ADDED BY SVEN JACOBS
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelClass
{
    public int number;
    public string mapJson; // json with the mapdata like "{\"Name\":\"Level1\",\"Width\":5,\"Height\":5,\"Grids\":[11,12,12,12,11,11,12,2,12,11,11,12,10,12,11,11,12,10,12,11,11,12,1,12,11]}"
    public bool isSolved; // true if the level 
    public int maxBlocks; // maximum allowed blocks to solve the level
    public int recordBox; // the best amount of blocks the user needed to solve the quiz
    public string hint; // hints and tips to solve the level
    public int wrongTrys;

    public LevelClass(int number, string mapJson, bool isSolved, int maxBlocks, int recordBox, string hint, int wrongTrys = 0)
    {
        this.number = number;
        this.mapJson = mapJson;
        this.isSolved = isSolved;
        this.maxBlocks = maxBlocks;
        this.recordBox = recordBox;
        this.hint = hint;
        this.wrongTrys = wrongTrys;
    }
}

[System.Serializable]
public class LevelStorage
{
    public int currentLevel = 1; // currently active Level
    public int bestLevel = 0; // the best level the user has reached/solved
    public List<LevelClass> AllLevels = new List<LevelClass>();
}

public class uBlocklyLevelManager
{
    public LevelStorage levelStorage = new LevelStorage();

    public uBlocklyLevelManager()
    {
        string dogName = PlayerPrefs.GetString("characterNameDog");
        if (!PlayerPrefs.HasKey("uBlocklyLevelStorage")) // init levelData if not already saved - if you change values here you have to start a new game to delte old PlayerPrefs
        {
            addLevel(new LevelClass(0, "{\"Name\":\"Level0\",\"Width\":3,\"Height\":4,\"Grids\":[11,2,11,11,10,11,11,10,11,11,1,11]}",
                                    false, 3, 0, "Verknüpfe die verfügbaren Anweisungen (linke Seite). Wenn du auf Start klickst wird " + dogName + " diesen Anweisungen folgen. Laufe hier 3 Schritte vorwärts."));
            addLevel(new LevelClass(1, "{\"Name\":\"Level1\",\"Width\":4,\"Height\":4,\"Grids\":[11,11,11,11,11,2,10,11,11,12,10,11,11,11,1,11]}",
                        false, 20, 0, "VORSICHT! Eine Hundefalle liegt im Weg. Nutze die richtige Anweisung, sodass " + dogName + " ihr ausweicht (drehe links)"));
            addLevel(new LevelClass(2, "{\"Name\":\"Level2\",\"Width\":5,\"Height\":5,\"Grids\":[11,11,11,11,11,11,10,2,10,11,11,10,12,10,11,11,10,10,10,11,11,11,1,11,11]}",
                                    false, 20, 0, "2 Verknüpfe die verfügbaren Anweisungen (linke Seite). Wenn du auf Start klickst wird " + dogName + " diesen Anweisungen folgen."));
            addLevel(new LevelClass(3, "{\"Name\":\"Level3\",\"Width\":3,\"Height\":8,\"Grids\":[11,11,11,11,2,11,11,10,11,11,10,11,11,10,11,11,10,11,11,10,11,11,1,11]}",
                                    false, 3, 0, "Du hast hier nur 3 Befehle zur Verfügung. Benutze daher eine Zählschleife. Innerhalb einer Zählschleife werden die Befehle X-mal wiederholt ausgeführt."));
            addLevel(new LevelClass(4, "{\"Name\":\"Level4\",\"Width\":6,\"Height\":6,\"Grids\":[11,11,11,11,11,11,11,10,10,10,10,11,11,10,11,11,10,11,11,10,11,2,10,11,11,10,11,11,11,11,11,1,11,11,11,11]}",
                                    false, 8, 0, "Erinnere dich an die Schleife (Wiederhole bis Ziel erreicht) und erweitere diese um eine bedingte Anweisung: Wenn Feld frei vorne - gehe vorwärts; sonst drehe rechts"));                        
        } // neues Level zwischen 4 und 5, welches normale Schleifen (mit Bedingung einführt)
    }

    void LoadSavedData()
    {
        if (PlayerPrefs.HasKey("uBlocklyLevelStorage"))
        {
            string saveJason = PlayerPrefs.GetString("uBlocklyLevelStorage");
            levelStorage = JsonUtility.FromJson<LevelStorage>(saveJason);
        }
        else
        {
            Debug.Log("no saved uBlocklyLevelData");
        }
    }
    public void SaveData()
    {
        string saveJason = JsonUtility.ToJson(levelStorage);
        PlayerPrefs.SetString("uBlocklyLevelStorage", saveJason);
        PlayerPrefs.Save();
    }
    public void solvedCurrentLevel()
    {
        LoadSavedData();
        levelStorage.AllLevels[levelStorage.currentLevel].isSolved = true;
        levelStorage.AllLevels[levelStorage.currentLevel].wrongTrys = 0; // reset Try/remove hints if level got solved
        SaveData();
    }

    public bool checkUblocklyLevelSolved(int level)
    {
        LoadSavedData();
        return levelStorage.AllLevels[level].isSolved;
    }

    public LevelClass getLevelClass(int index)
    {
        LoadSavedData();
        return levelStorage.AllLevels[index];
    }
    public LevelClass getCurrentLevelClass()
    {
        LoadSavedData();
        return levelStorage.AllLevels[levelStorage.currentLevel];
    }
    public void setCurrentLevel(int index)
    {
        LoadSavedData();
        levelStorage.currentLevel = index;
        SaveData();
    }
    public int getCurrentLevel()
    {
        LoadSavedData();
        return levelStorage.currentLevel;
    }
    public void addLevel(LevelClass level)
    {
        LoadSavedData();
        levelStorage.AllLevels.Add(level);
        SaveData();
    }

    public void newRecord(int blockCount)
    {
        LoadSavedData();
        if (levelStorage.bestLevel < levelStorage.currentLevel)
        {
            levelStorage.bestLevel = levelStorage.currentLevel;
        }
        if (getCurrentLevelClass().recordBox > blockCount || getCurrentLevelClass().recordBox == 0)
        {
            getCurrentLevelClass().recordBox = blockCount;
        }
        SaveData();
    }
    public void addWrongTry()
    {
        LoadSavedData();
        levelStorage.AllLevels[levelStorage.currentLevel].wrongTrys = levelStorage.AllLevels[levelStorage.currentLevel].wrongTrys + 1;
        SaveData();
    }
    public void resetWrongTry()
    {
        LoadSavedData();
        levelStorage.AllLevels[levelStorage.currentLevel].wrongTrys = 0;
        SaveData();
    }
    public int getWrongTry()
    {
        LoadSavedData();
        return levelStorage.AllLevels[levelStorage.currentLevel].wrongTrys;
    }

    public int getBestLevel()
    {
        LoadSavedData();
        return levelStorage.bestLevel;
    }

    public int getLevelCount()
    {
        LoadSavedData();
        return levelStorage.AllLevels.Count;
    }
}
