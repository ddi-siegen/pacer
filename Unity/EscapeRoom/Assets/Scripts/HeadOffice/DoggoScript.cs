using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class DoggoScript : abstractCharacterClass
{

    //[SerializeField] Transform target;

    public GameObject destination;

    NavMeshAgent agent;

    Animator animator;

    public bool atDestination;
     
    // Start is called before the first frame update
    void Start()
    {
        //grabs target (robot) in runtime, that is why it needs to be overwritten
       // target = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;

        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //if want to follow player
        //agent.SetDestination(target.position);

        //agent.SetDestination(destination.transform.position);
        if(agent.remainingDistance < 0.1f)
        {
           //if doggo is nearly approaching his destination, change it
            atDestination = true;
        }
        else
        {
            atDestination = false;
        }

        //stand
        if (agent.velocity.x == 0)
        {
            animator.SetInteger("Direction", 0);
        }
        //if moved to left
        else if (agent.velocity.x < 0)
        {
            animator.SetInteger("Direction", 1);
            agent.GetComponent<SpriteRenderer>().flipX = true;
        }
        //if moved to right
        else if (agent.velocity.x > 0)
        {
            animator.SetInteger("Direction", 2);
            agent.GetComponent<SpriteRenderer>().flipX = false;
        }

    }

    public override void teleport(Vector3 vector)
    {
        Start();
        this.gameObject.SetActive(true);
        agent.gameObject.SetActive(true);
        agent.enabled = false;
        agent.transform.position = vector + new Vector3(-6.38f, -2.85f, 0f); // offset navmesh in headoffice
        // each scene got a different offset so maybe we need to check for current scene later
        agent.enabled = true;
    }

    public override void walkToPosition(Vector3 vector)
    {
        agent.SetDestination(vector + new Vector3(-6.38f, -2.85f, 0f));

    }
    //funktion mit laufen

    public void startDrinking(Vector3 vector)
    {
        Start();
        this.gameObject.SetActive(true);
        agent.gameObject.SetActive(true);
        agent.SetDestination(vector + new Vector3(-6.38f, -2.85f, 0f));
        animator.SetTrigger("StartDrinking");
    }

    public void stopDrinking()
    {
        animator.SetTrigger("StopDrinking");
    }
}


