using UnityEngine;
using UnityEngine.AI;

public class LevelManagerHO : LevelManager
{
    [Header("General")]
    public LevelChanger levelChanger;
    public TextBoxLogic TextBoxLogic;
    public InventoryManager inventory;

    //UI
    [Header("UI")]
    public GameObject homeScreen;
    public GameObject routerUI;
    public GameObject routerAddNoteButton;
    public GameObject printerImage;
    public GameObject PasswortPosterParent;
    public GameObject uBlockly;
    public GameObject Book;

    //player
    [Header("Player")]
    public GameObject player;
    AgentScript agentScript;
    NavMeshAgent navmeshAgent;

    //dog
    [Header("Dog")]
    public GameObject doggo;
    DoggoScript doggoScript;
    NavMeshAgent navmeshDoggo;

    [Header("Ventilation")]
    [SerializeField] GameObject ventilation;
    [SerializeField] GameObject ventilation_broken;


    void Awake()
    {
        agentScript = player.GetComponent<AgentScript>();
        navmeshAgent = player.GetComponent<NavMeshAgent>();
        doggoScript = doggo.GetComponent<DoggoScript>();
        navmeshDoggo = doggo.GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // port player to laptop if this is the first time he didnt read the TextBoxLogic yet
        if (PlayerPrefs.GetInt("ChapterOneActive") == 0)
        {
            agentScript.teleport(new Vector3(-3.93000007f, 4.07999992f, 0f));
            doggoScript.teleport(new Vector3(7.82000017f, 0.860000014f, 0f));
            TextBoxLogic.setText(TextBoxLogic.Texts["Start_1_0_0_0"]);
        }
        else
        {
            doggoScript.startDrinking(new Vector3(-1.41999996f, 6.76999998f, 0));
        }

        if (PlayerPrefs.GetString("currDogScene") != "HO")
        {
            doggo.SetActive(false);
        }

        if (PlayerPrefs.GetInt("VentilationFixed") == 1)
        {
            ventilation.SetActive(true);
            ventilation_broken.SetActive(false);
        }
        else
        {
            ventilation.SetActive(false);
            ventilation_broken.SetActive(true);
        }
    }

    //handles the collision of mouse (player) with objects named with specified tag
    public override void AgentCollision(string myCollider)
    {
        if (myCollider != "")
        {
            agentScript.stopAtCurrentPosition();
        }

        switch (myCollider)
        {
            //**Collider Trigger**
            case "laptop":
                homeScreen.SetActive(true);
                navmeshAgent.isStopped = true;
                FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(false);
                break;

            case "exitSign":
                if (PlayerPrefs.GetInt("SmartphoneTaken") == 0)
                {
                    if (TextBoxLogic.Texts.ContainsKey("SmartphoneNotTaken")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["SmartphoneNotTaken"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "SmartphoneNotTaken");
                }
                else
                {
                    levelChanger.FadeToLevel(3);
                }

                break;

            case "tool_case":
                if (PlayerPrefs.GetInt("ScrewdriverTaken").Equals(0))
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["tool_case"]);
                }
                else
                {
                    TextBoxLogic.setText(TextBoxLogic.Texts["tool_case_taken_message"]);
                }
                break;

            case "shelf_0":
                Book.GetComponentInChildren<BookManager>().openBook("Lizenzen");
                Instantiate(Book);
                break;

            case "shelf_1":
                Book.GetComponentInChildren<BookManager>().openBook("Datenschutz");
                Instantiate(Book);
                break;

            case "router":
                if (PlayerPrefs.GetInt("SSIDToNotes").Equals(1) && PlayerPrefs.GetInt("EmbarrassingPosterClicked").Equals(1))
                {
                    if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + myCollider);
                }
                else
                {
                    //disable MenuPanel
                    FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(false);

                    routerUI.SetActive(true);
                    navmeshAgent.isStopped = true;
                    if (PlayerPrefs.GetInt("SSIDToNotes").Equals(1) | PlayerPrefs.GetInt("SmartphoneTaken").Equals(0))
                    {
                        routerAddNoteButton.SetActive(false);

                    }
                    else if (PlayerPrefs.GetInt("SmartphoneTaken").Equals(1))
                    {
                        routerAddNoteButton.SetActive(true);
                    }
                }
                break;

            case "printer":
                printerImage.SetActive(true);
                navmeshAgent.isStopped = true;
                break;

            case "passwort_poster":
                PasswortPosterParent.SetActive(true);
                navmeshAgent.isStopped = true;
                break;

            case "slot_machine_purple":
                if (PlayerPrefs.GetInt("PCPuzzleSolved").Equals(0))
                {
                    navmeshAgent.isStopped = true;
                    // PuzzleUI is activated via textbox
                    if (TextBoxLogic.Texts.ContainsKey("PCPuzzle*01")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["PCPuzzle*01"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "PCPuzzle*01");
                    break;
                }
                else
                {
                    if (TextBoxLogic.Texts.ContainsKey("PCPuzzleSolved")) // Check if Collider exists for Texts
                    {
                        TextBoxLogic.setText(TextBoxLogic.Texts["PCPuzzleSolved"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "PCPuzzleSolved");
                    break;
                }

            case "":
                //Debug.Log("Empty myCollider");
                break;

            //**Default**  
            default:
                bool isInTexts = false, isInItems = false;
                if (TextBoxLogic.Texts.ContainsKey(myCollider)) // Check if Collider exists for Texts
                {
                    isInTexts = true;
                    TextBoxLogic.setText(TextBoxLogic.Texts[myCollider]);
                }
                if (inventory.Items.ContainsKey(myCollider))
                {
                    isInItems = true;
                    //Debug.Log("Before fill Inventory");
                    //add specific item to items list in order to place it in inventory at correct position
                    //in need to check in which status we are currently in
                    inventory.FillInventory(myCollider);
                }
                if (!isInTexts && !isInItems)
                    Debug.Log("Collider not defined. Collider is: " + myCollider);
                break;
        }
        agentScript.myCollider = "";
    }

    public void refreshTextBox()
    {
        TextBoxLogic.refresh();
    }

}


