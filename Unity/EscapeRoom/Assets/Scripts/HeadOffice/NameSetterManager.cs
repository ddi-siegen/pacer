using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AI;

public class NameSetterManager : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI TextDog, TextAgent, TextError;
    [SerializeField] Button submitButton;
    [SerializeField] InputField inputField;
    [SerializeField] GameObject backgroundImage;
    [SerializeField] GameObject doggo3d;
    [SerializeField] LevelManagerHO levelManagerHO;

    int state = 0; // 0 = dog, 1 = agent
    string customName = "";

    void Start()
    {
        submitButton.onClick.AddListener(delegate { submitName(); });
    }

    void submitName()
    {
        customName = inputField.transform.Find("Text").GetComponent<Text>().text;
        if (checkNameLength())
        {
            switch (state)
            {
                case 0:
                    TextDog.text = customName;
                    PlayerPrefs.SetString("characterNameDog", customName.Trim());  
                    AudioManager.instance.Play("doggo_newName");
                    levelManagerHO.refreshTextBox();
                    disableNameSetter();
                    GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = true;
                    GameObject.Find("Doggo").GetComponent<DoggoScript>().walkToPosition(new Vector3(-2.05999994f, 2.02999997f, 0));
                    FindObjectOfType<TextBoxLogic>().waitForSecondsUntilNextTextbox(6.0f, "Start_1_0_0_3");
                    break;
                case 1:
                    TextAgent.text = customName;
                    PlayerPrefs.SetString("characterNameRobot", customName);
                    levelManagerHO.refreshTextBox();
                    disableNameSetter();
                    break;
            }
        }
    }
    
    private bool checkNameLength()
    {
        if (customName.Trim().Length < 2)
        {
            TextError.text = "Der Name muss mindestens 2 Zeichen lang sein. Nur Elons Kinder haben solche Namen.";
            TextError.gameObject.SetActive(true);
            return false;
        }
        else if (customName.Trim().Length > 10)
        {
            TextError.text = "Sorry Miss Langstrumpf. Aber wir akzeptieren nur Namen mit bis zu 10 Zeichen.";
            TextError.gameObject.SetActive(true);
            return false; 
        }
        else
        {
            return true;
        }
    }
    public void enableDogNameSetter()
    {
        state = 0;
        GameObject.Find("Robot").GetComponent<NavMeshAgent>().isStopped = true;
        TextDog.gameObject.SetActive(true);
        TextAgent.gameObject.SetActive(false);
        TextError.gameObject.SetActive(false);
        inputField.gameObject.SetActive(true);
        backgroundImage.SetActive(true);
        doggo3d.SetActive(true);
        submitButton.gameObject.SetActive(true);
    }

    public void enableAgentNameSetter()
    {
        state = 1;
        TextDog.gameObject.SetActive(false);
        TextAgent.gameObject.SetActive(true);
        TextError.gameObject.SetActive(false);
        inputField.gameObject.SetActive(true);
        backgroundImage.SetActive(true);
        doggo3d.SetActive(true);
        submitButton.gameObject.SetActive(true);
    }

    void disableNameSetter()
    {
        TextDog.gameObject.SetActive(false);
        TextAgent.gameObject.SetActive(false);
        TextError.gameObject.SetActive(false);
        inputField.gameObject.SetActive(false);
        backgroundImage.SetActive(false);
        doggo3d.SetActive(false);
        submitButton.gameObject.SetActive(false);
    }


}

