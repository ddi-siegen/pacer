using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PCPuzzleDragItem : MonoBehaviour, IPointerDownHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IDropHandler
{
    public PCPuzzleDragSlot inSlot { get; set; }
    public bool isCorrectlyPaired { get; set; }

    private string _itemName;
    private Vector2 _defaultPosition;
    private Vector2 _lastPosition;
    private RectTransform _rectTransform;
    private Canvas _canvas;
    private CanvasGroup _canvasGroup;
    [SerializeField] private GameObject _puzzleUI;
    private Transform _startParent;
    [SerializeField] private Transform _dragCanvas;
    private int _siblingIndex;
    [SerializeField] private Text _selectedItemName;

    public static bool isDragging { get; private set; }

    private void Awake()
    {
        _itemName = GetComponentInChildren<Text>(includeInactive: true).text;
    }

    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _canvas = GetComponentInParent<Canvas>();
        _canvasGroup = GetComponent<CanvasGroup>();
        _defaultPosition = transform.position;
        _lastPosition = transform.position;
    }

    // implements interfaces required for drag n drop
    public void OnPointerDown(PointerEventData eventData)
    {
        // Debug.Log("Clicked");
    }

    public void OnDrag(PointerEventData eventData)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            _puzzleUI.GetComponent<RectTransform>(),
            eventData.position,
            eventData.pressEventCamera,
            out Vector2 localPoint);

        // limit drag to puzzleUI

        int bgAdjustment = 12; // adjust for background image

        if (localPoint.x >= _puzzleUI.GetComponent<RectTransform>().rect.xMax - _rectTransform.rect.width / 2 - _puzzleUI.GetComponent<RectTransform>().rect.width / 8) // leave some hspace for menu buttons
        {
            localPoint.x = _puzzleUI.GetComponent<RectTransform>().rect.xMax - _rectTransform.rect.width / 2 - _puzzleUI.GetComponent<RectTransform>().rect.width / 8;
        }
        else if (localPoint.x <= _puzzleUI.GetComponent<RectTransform>().rect.xMin + _rectTransform.rect.width / 2 + bgAdjustment)
        {
            localPoint.x = _puzzleUI.GetComponent<RectTransform>().rect.xMin + _rectTransform.rect.width / 2 + bgAdjustment;
        }

        if (localPoint.y >= _puzzleUI.GetComponent<RectTransform>().rect.yMax - _rectTransform.rect.height / 2 - bgAdjustment)
        {
            localPoint.y = _puzzleUI.GetComponent<RectTransform>().rect.yMax - _rectTransform.rect.height / 2 - bgAdjustment;
        }
        else if (localPoint.y <= _puzzleUI.GetComponent<RectTransform>().rect.yMin + _rectTransform.rect.height / 2 + bgAdjustment)
        {
            localPoint.y = _puzzleUI.GetComponent<RectTransform>().rect.yMin + _rectTransform.rect.height / 2 + bgAdjustment;
        }

        _rectTransform.anchoredPosition = new Vector2(localPoint.x, localPoint.y);
        _canvasGroup.blocksRaycasts = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        // Debug.Log("Drag started");
        PCPuzzleDragItem.isDragging = true;
        _lastPosition = transform.position;
        _startParent = transform.parent;
        _siblingIndex = transform.GetSiblingIndex();
        transform.SetParent(_dragCanvas);
        if (inSlot != null)
        {
            isCorrectlyPaired = false;
            inSlot = null;
        }
        _selectedItemName.text = _itemName;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        // Debug.Log("Drag ended");
        PCPuzzleDragItem.isDragging = false;
        _canvasGroup.blocksRaycasts = true;
        if (transform.parent == _dragCanvas)
        {
            transform.SetParent(_startParent);
            transform.SetSiblingIndex(_siblingIndex);
        }
        _selectedItemName.text = "";
    }

    public void OnDrop(PointerEventData eventData)
    {
        // Debug.Log("Dropped");
        eventData.pointerDrag.GetComponent<PCPuzzleDragItem>().MoveToLastPosition();
    }

    public void MoveToLastPosition()
    {
        transform.position = _lastPosition;
    }

    public void ResetPosition()
    {
        transform.position = _defaultPosition;
    }
}