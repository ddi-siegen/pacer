using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PCPuzzleDragSlot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    private PCPuzzleManager _PCPuzzleManager;


    private void Awake()
    {
        _PCPuzzleManager = GetComponentInParent<PCPuzzleManager>();
    }

    // implements interfaces required for drag n drop
    public void OnDrop(PointerEventData eventData)
    {
        // Debug.Log("Dropped in slot " + this);

        if (eventData.pointerDrag != null)
        {
            eventData.pointerDrag.GetComponent<PCPuzzleDragItem>().inSlot = this;

            List<PCPuzzleDragItem> result;
            if (_PCPuzzleManager.correctPairings.TryGetValue(this, out result))
            {
                if (result.Contains(eventData.pointerDrag.GetComponent<PCPuzzleDragItem>()))
                {
                    eventData.pointerDrag.GetComponent<PCPuzzleDragItem>().isCorrectlyPaired = true;
                }
            }

            AudioManager.instance.Play("drag_drop");
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // Debug.Log("Mouse entered slot " + this);
        if (PCPuzzleDragItem.isDragging)
        {
            GetComponent<Outline>().enabled = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // Debug.Log("Mouse exited slot " + this);
        GetComponent<Outline>().enabled = false;
    }
}