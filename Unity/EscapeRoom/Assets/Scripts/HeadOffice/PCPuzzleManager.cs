using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PCPuzzleManager : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _agent;

    [SerializeField] private GameObject _menuPanel;
    private MenuPanelManager _menuPanelManager;

    [SerializeField] private GameObject _textBox;
    private TextBoxLogic _textBoxLogic;

    [SerializeField] private List<PCPuzzleDragItem> _dragItems = new List<PCPuzzleDragItem>();
    [SerializeField] private List<PCPuzzleDragSlot> _dragSlots = new List<PCPuzzleDragSlot>();

    [SerializeField] private Dictionary<PCPuzzleDragSlot, List<PCPuzzleDragItem>> _correctPairings;
    public Dictionary<PCPuzzleDragSlot, List<PCPuzzleDragItem>> correctPairings { get { return _correctPairings; } }

    private bool _isTaskCompleted = false;


    private void Awake()
    {
        _correctPairings = new Dictionary<PCPuzzleDragSlot, List<PCPuzzleDragItem>>
        {
            [_dragSlots[0]] = new List<PCPuzzleDragItem> { _dragItems[4], _dragItems[6], _dragItems[7], _dragItems[9] },
            [_dragSlots[1]] = new List<PCPuzzleDragItem> { _dragItems[1], _dragItems[2] },
            [_dragSlots[2]] = new List<PCPuzzleDragItem> { _dragItems[0], _dragItems[3], _dragItems[5], _dragItems[8] }
        };
    }

    private void OnEnable()
    {
        _menuPanelManager = _menuPanel.GetComponent<MenuPanelManager>();
        _menuPanelManager.SetMenuPanelActive(false);

        _textBoxLogic = _textBox.GetComponent<TextBoxLogic>();

        _agent.isStopped = true;

        Debug.Log("CTC coroutine started");
        StartCoroutine(CheckTaskCompletion());
    }

    private void OnDisable()
    {
        _menuPanelManager.SetMenuPanelActive(true);

        _agent.isStopped = false;

        Debug.Log("CTC coroutine stopped");
        StopCoroutine(CheckTaskCompletion());
    }

    private IEnumerator CheckTaskCompletion()
    {
        while (!_isTaskCompleted)
        {
            int correctlyPairedItems = 0;
            int pairedItems = 0;

            foreach (PCPuzzleDragItem dragItem in _dragItems)
            {
                if (dragItem.inSlot != null) { pairedItems++; }
                if (dragItem.isCorrectlyPaired) { correctlyPairedItems++; }
            }

            // all items paired?
            if (pairedItems >= _dragItems.Count)
            {
                // all pairings correct?
                if (correctlyPairedItems >= _dragItems.Count)
                {
                    Debug.Log("TASK COMPLETED");
                    _isTaskCompleted = true;
                    if (_textBoxLogic.Texts.ContainsKey("PCPuzzle*02")) // Check if Collider exists for Texts
                    {
                        _textBoxLogic.setText(_textBoxLogic.Texts["PCPuzzle*02"]);
                    }
                    else
                        Debug.Log("Collider not defined. Collider is: " + "PCPuzzle*02");

                    AudioManager.instance.Play("correct");
                    PlayerPrefs.SetInt("PCPuzzleSolved", 1);
                    gameObject.SetActive(false);
                }
                else
                {
                    // Debug.Log("WRONG PAIRINGS: TASK NOT COMPLETED");
                }
            }
            else
            {
                // Debug.Log("TASK NOT COMPLETED");
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    public void ButtonClickedBack()
    {
        gameObject.SetActive(false);
    }
}