using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class PrinterManager : MonoBehaviour
{
    public GameObject printerScreen;
    public GameObject screenZoom;
    public NavMeshAgent agent;

    public InputField SSIDInput;
    public InputField passwordInput;
    public Button enterButton;
    public Button visibleButton;
    public Image sentenceImage;

    public int wrongPasswordCounter;

    public GameObject inventory;
    // Start is called before the first frame update
    void Start()
    {
        passwordInput.contentType = InputField.ContentType.Password;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ZoomInButtonClicked()
    {
        AudioManager.instance.Play("Maus");
        if (PlayerPrefs.GetInt("PrinterPWCorrect").Equals(0))
        {
            screenZoom.SetActive(true);

            //display password sentence help if it has already been copied to the notes App
            if (PlayerPrefs.GetInt("PasswordFileClicked").Equals(0))
            {
                sentenceImage.gameObject.SetActive(false);
            }
            else
            {
                sentenceImage.gameObject.SetActive(true);
            }

            //as long as the password file has not been clicked (copied to Notes App) one cannot type in password or username
                /*if (PlayerPrefs.GetInt("PasswordFileClicked").Equals(0))
                {
                    SSIDInput.interactable = false;
                    passwordInput.interactable = false;
                    enterButton.interactable = false;
                    visibleButton.interactable = false;

                }
                else if (PlayerPrefs.GetInt("PasswordFileClicked").Equals(1) && PlayerPrefs.GetInt("WiringFilePrinted").Equals(1))
                {
                    SSIDInput.interactable = true;
                    passwordInput.interactable = true;
                    enterButton.interactable = true;
                    visibleButton.interactable = true;
                    sentenceImage.gameObject.SetActive(true);
                }*/
        }
        else
        {
            if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("correctPassword*01")) // Check if Collider exists for Texts
            {
                FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["correctPassword*01"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "correctPassword*01");
        }

    }

    public void ReturnButtonClicked()
    {
        AudioManager.instance.Play("Maus");
        printerScreen.SetActive(false);
        agent.isStopped = false;
    }

    public void ZoomReturnButtonClicked()
    {
        AudioManager.instance.Play("Maus");
        screenZoom.SetActive(false);
    }

    public void VisibleButtonCLicked() 
    {
        AudioManager.instance.Play("Maus");
        if (passwordInput.inputType == InputField.InputType.Standard)
        {
            passwordInput.inputType = InputField.InputType.Password;
            passwordInput.ForceLabelUpdate();
        }
        else if (passwordInput.inputType == InputField.InputType.Password)
        {
            passwordInput.inputType = InputField.InputType.Standard;
            passwordInput.ForceLabelUpdate();

        }
    }
    public void EnterButtonClicked()
    {
        AudioManager.instance.Play("Maus");
        //correct Inputs
        string benutzername = "buerolan01";
        string passwort = "D+isw2gF!";

        //delete all spaces from the two Input strings
        SSIDInput.text = SSIDInput.text.Replace(" ", "");
        passwordInput.text = passwordInput.text.Replace(" ", "");
        

        //wrong input
        if (!SSIDInput.text.Equals(benutzername))
        {
            Text textSSID = SSIDInput.transform.Find("Text").GetComponent<Text>();
            textSSID.color = Color.red;
        }
        if (!passwordInput.text.Equals(passwort))
        {
            Text textPasswort = passwordInput.transform.Find("Text").GetComponent<Text>();
            textPasswort.color = Color.red;
            //increase wrong password counter
            wrongPasswordCounter += 1;

            if (wrongPasswordCounter == 3)
            {
                //reminder about poster in Headoffice after 3 incorrect entries
                if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("wrongPassword*01")) // Check if Collider exists for Texts
                {
                    FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["wrongPassword*01"]);
                }
                else
                    Debug.Log("Collider not defined. Collider is: " + "wrongPassword*01");
            }
        }

        //correct input
        if(SSIDInput.text.Equals(benutzername)&& passwordInput.text.Equals(passwort))
        {
            Text textSSID = SSIDInput.transform.Find("Text").GetComponent<Text>();
            textSSID.color = Color.green;

            Text textPasswort = passwordInput.transform.Find("Text").GetComponent<Text>();
            textPasswort.color = Color.green;

            SSIDInput.interactable = false;
            passwordInput.interactable = false;

            PlayerPrefs.SetInt("PrinterPWCorrect", 1);
            GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Drucke die Anleitung für die richtige Verkabelung im Fahrstuhl aus.");


            StartCoroutine(CoroutineWaitForSeconds(1.5f));

            printerScreen.SetActive(false);
            agent.isStopped = false;
        }
    }

    private IEnumerator CoroutineWaitForSeconds(float secGreen) // Coroutine that waits for sec until the nextTextboxId will be shown
    {
        yield return new WaitForSeconds(secGreen);
        screenZoom.SetActive(false);
    }

    public IEnumerator PrintPDF(float secSound)
    {
        if (PlayerPrefs.GetInt("WiringFilePrinted").Equals(1))
        {
            if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("wiringFileAlreadyPrinted")) // Check if Collider exists for Texts
            {
                FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["wiringFileAlreadyPrinted"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "wiringFileAlreadyPrinted");

            yield break;
        }

        PlayerPrefs.SetInt("WiringFilePrinted", 1);
        
        AudioManager.instance.Play("Printer");
        yield return new WaitForSeconds(secSound);

        //hier dann papier zu inventar hinzuf�gen 
        FindObjectOfType<InventoryManager>(true).FillInventory("pdf");
        GameObject.Find("Notification").GetComponent<Notification_Script>().changeText("Mit der gedruckten Anleitung sollten wir das Rätsel im Fahrstuhl lösen können.");
        //inventory.GetComponent<InventoryManager>().FillInventory("pdf");
    }

}
