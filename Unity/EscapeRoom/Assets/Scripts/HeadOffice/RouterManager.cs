using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RouterManager : MonoBehaviour
{

   private int counter = 0;

    //Router_CloseUp
    public GameObject routerUI;
    public GameObject routerAddToNotes;

    //SSID_PW_CloseUp
    public GameObject ssid_pw_CloseUp;

    // Start is called before the first frame update
    public NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ButtonClickedExit()
    {
        AudioManager.instance.Play("Maus");
        routerUI.SetActive(false);
        ssid_pw_CloseUp.SetActive(false);
        counter = 0;
        agent.isStopped = false;

        //enableMenuPanel
        FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(true);

    }
    public void ButtonClickedSSID_PW()
    {
        AudioManager.instance.Play("Maus");
        if (counter == 0)
        {
            ssid_pw_CloseUp.SetActive(true);
            counter = 1;
        }else if(counter == 1)
        {
            ssid_pw_CloseUp.SetActive(false);
            counter = 0;
        }
 
    }

    public void AddtoNotesButton()
    {
        AudioManager.instance.Play("Maus");

        //notify NotesApp & Smartphone
        FindObjectOfType<NewSmartphoneManager>().NotifyNewNote();

        NoteManager noteManager = new NoteManager();
        noteManager.newNote(content: "SSID: buerolan01; passwort: ?", isNew: true, isEditable: false);
        Debug.Log("Add SSId + Password to Notes App");

        routerAddToNotes.SetActive(false);

        if (PlayerPrefs.GetInt("SSIDToNotes").Equals(0))
        {
            PlayerPrefs.SetInt("SSIDToNotes", 1);
        }
    }
}
