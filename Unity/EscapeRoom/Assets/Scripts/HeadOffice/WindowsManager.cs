using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class WindowsManager : MonoBehaviour
{
    [Header ("Screens")]
    public GameObject homeScreen;
    //Bildschirm mit dateien
    public GameObject bildschirm;
    public GameObject windowsScreen;


    [Header("FileScreens")]
    //screen for CC file
    public GameObject camelCaseScreen;
    //screen for PW file
    public GameObject passwordScreen;

    public GameObject verkabelungsScreen;

    public GameObject WichtigeLinks;
    public GameObject Einkaufsliste;
    public GameObject ToDos;
    public GameObject BitErklaerung;

    [Header("FileIcons")]
    public GameObject verkabelungsPDFIcon;

    [Header("Other")]
    public NavMeshAgent agent;
    public Button addToNoteButtonPW;
    public Button printVerkablungButton;

    [Header("Router")]
    public GameObject printer;
    private PrinterManager printerManager;




    // Start is called before the first frame update
    void Start()
    {
        printerManager = printer.GetComponent<PrinterManager>();
    }

    public void ButtonClickedExit()
    {
        windowsScreen.SetActive(true);
        homeScreen.SetActive(false);
        bildschirm.SetActive(false);
        agent.isStopped = false;

        AudioManager.instance.Play("Maus");
        FindObjectOfType<MenuPanelManager>().SetMenuPanelActive(true);

    }

    public void ButtonClickedFolder()
    {
        //homeScreen.SetActive(false);
        bildschirm.SetActive(true);
        AudioManager.instance.Play("Maus");

        if (PlayerPrefs.GetInt("PasswordFileClicked").Equals(1))
        {
            addToNoteButtonPW.gameObject.SetActive(false);
        }
        if (PlayerPrefs.GetInt("WiringFilePrinted").Equals(1))
        {
            //printVerkablungButton.gameObject.SetActive(false);
        }


    }

    public void ButtonClickedReturn()
    {
        //homeScreen.SetActive(true);
        bildschirm.SetActive(false);
        AudioManager.instance.Play("Maus");
    }


    public void EinkaufslisteTXTClicked()
    {
        //homeScreen.SetActive(false);
        bildschirm.SetActive(false);
        Einkaufsliste.SetActive(true);
        AudioManager.instance.Play("Maus");

    }

    public void EKExitButton()
    {
        Einkaufsliste.SetActive(false);
        homeScreen.SetActive(true);
        AudioManager.instance.Play("Maus");
    }

    public void ToDosTXTClicked()
    {
        //homeScreen.SetActive(false);
        bildschirm.SetActive(false);
        ToDos.SetActive(true);
        AudioManager.instance.Play("Maus");

    }

    public void TDExitButton()
    {
        ToDos.SetActive(false);
        homeScreen.SetActive(true);
        AudioManager.instance.Play("Maus");
    }

    public void BitErklaerungTXTClicked()
    {
        //homeScreen.SetActive(false);
        bildschirm.SetActive(false);
        BitErklaerung.SetActive(true);
        AudioManager.instance.Play("Maus");

    }

    public void BEExitButton()
    {
        BitErklaerung.SetActive(false);
        homeScreen.SetActive(true);
        AudioManager.instance.Play("Maus");
    }

 

    public void PWClicked()
    {
        bildschirm.SetActive(false);
        passwordScreen.SetActive(true);
        AudioManager.instance.Play("Maus");

    }

    public void PWExitButton()
    {
        passwordScreen.SetActive(false);
        bildschirm.SetActive(true);
        AudioManager.instance.Play("Maus");
    }

    public void AddToNoteButton()
    {
        AudioManager.instance.Play("Maus");

        //notify Inventory
        // FindObjectOfType<InventoryManager>().InventoryNotification();
        //InventoryManager inventoryManager = GameObject.Find("/MenuPanel/Canvas/Panel/Inventory").GetComponent<InventoryManager>();
        //inventoryManager.InventoryNotification();

        if (PlayerPrefs.GetInt("SmartphoneTaken").Equals(1))
        {
            NoteManager noteManager = new NoteManager();
            noteManager.newNote(content: "Du und ich sind wirklich zwei gute Freunde!", isNew: true, isEditable: false);

            if (PlayerPrefs.GetInt("PasswordFileClicked").Equals(0))
            {
                PlayerPrefs.SetInt("PasswordFileClicked", 1);

            }
            addToNoteButtonPW.gameObject.SetActive(false);

            //close windowsscreen and close all windows
            this.PWExitButton();
            this.ButtonClickedExit();
            //notify NotesApp & Smartphone
            FindObjectOfType<NewSmartphoneManager>().NotifyNewNote();
        }
        else
        {
            if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("windowsScreen*01")) // Check if Collider exists for Texts
            {
                FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["windowsScreen*01"]);
            }
            else
                Debug.Log("Collider not defined. Collider is: " + "windowsScreen*01");
        }
    }
        

    public void VerkabelungClicked()
    {
        bildschirm.SetActive(false);
        verkabelungsScreen.SetActive(true);
        AudioManager.instance.Play("Maus");
        if (PlayerPrefs.GetInt("WiringFilePrinted").Equals(1))
        {
            //printVerkablungButton.gameObject.SetActive(false);
        }
    }

    public void VerkabelungPrintClicked()
    {
        AudioManager.instance.Play("Maus");

        if (PlayerPrefs.GetInt("PrinterPWCorrect").Equals(0))
        {
            if (FindObjectOfType<TextBoxLogic>().Texts.ContainsKey("verkabelung*01")) // Check if Collider exists for Texts
            {
                FindObjectOfType<TextBoxLogic>().setText(FindObjectOfType<TextBoxLogic>().Texts["verkabelung*01"]);
            }
            else
            {
                Debug.Log("Collider not defined. Collider is: " + "verkabelung*01");
            }

        }
        else
        {
                StartCoroutine(printerManager.PrintPDF(8.0f));
        }
    
    }

    public void VerkabelungsExitButton()
    {
        verkabelungsScreen.SetActive(false);
        homeScreen.SetActive(true);
        AudioManager.instance.Play("Maus");
    }


   /* private IEnumerator PrintPDF(float secSound)
    {
        AudioManager.instance.Play("Printer");
        yield return new WaitForSeconds(secSound);

        //hier dann papier zu inventar hinzuf�gen 
        FindObjectOfType<InventoryManager>().FillInventory("pdf");
    }
   */

}
