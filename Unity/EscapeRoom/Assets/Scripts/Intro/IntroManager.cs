using TMPro;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    [SerializeField] private float _scrollSpeed;
    [SerializeField] private GameObject _text;
    [SerializeField] private GameObject _bgCanvas;
    private Vector3[] _worldCorners = new Vector3[4];
    [SerializeField] protected LevelChanger _levelChanger;

    private void Start()
    {
        AudioManager.instance.SetMusic("intro");
    }

    private void Update()
    {
        transform.Translate(Camera.main.transform.up * Time.deltaTime * _scrollSpeed);

        _text.GetComponent<TextMeshProUGUI>().rectTransform.GetWorldCorners(_worldCorners);

        // skip intro if mouse is clicked (or screen touched) or end of text is reached
        if (Input.GetMouseButtonDown(0) || _worldCorners[0].y >= _bgCanvas.transform.position.y)
        {
            ChangeScene();
        }
    }

    protected virtual void ChangeScene()
    {
        // change music
        AudioManager.instance.SetMusic("soundtrack_01");
        // go to headoffice
        _levelChanger.FadeToLevel(2);
    }
}
