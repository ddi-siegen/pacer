using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagerSc : MonoBehaviour
{
    public GameObject CreditsUI;
    public GameObject StartScreen;
    public GameObject CreateNewGame;
    public LevelChanger levelChanger;
    public GameObject StartScreenNewButton;
    public DefaultStates defaultStates;
    public GameObject continueButton;

    void Awake()
    {
        defaultStates = new DefaultStates();
    }
    private void Start()
    {
        continueButton.SetActive(PlayerPrefs.GetInt("AlreadyPlayed") == 1); // only show continue button if the player played the game before
    }
    public void ContinueGame()
    {
        levelChanger.FadeToLevel(PlayerPrefs.GetInt("currentScene"));
        AudioManager.instance.Play("Maus");
    }

    public void NewGame()
    {
        if (PlayerPrefs.GetInt("AlreadyPlayed") == 1)
        {
            CreateNewGame.SetActive(true);
            AudioManager.instance.Play("Maus");
        }
        else
        {
            NewGameYes();
            AudioManager.instance.Play("Maus");
        }
    }


    public void NewGameYes()
    {
        defaultStates.setToDefault();
        PlayerPrefs.SetInt("AlreadyPlayed", 1);
        // start intro
        levelChanger.FadeToLevel(5);
        AudioManager.instance.Play("Maus");
    }

    public void NewGameNo()
    {
        CreateNewGame.SetActive(false);
        AudioManager.instance.Play("Maus");
    }

    public void CreditsOpen()
    {
        StartScreen.SetActive(false);
        CreditsUI.SetActive(true);
        AudioManager.instance.Play("Maus");
    }

    public void CreditsClose()
    {
        StartScreen.SetActive(true);
        CreditsUI.SetActive(false);
        AudioManager.instance.Play("Maus");
    }

    public void End()
    {
        Application.Quit();
        AudioManager.instance.Play("Maus");
    }
    public void goToCheckPoint001()
    {
        defaultStates.setToDefault();
        PlayerPrefs.SetInt("previousScene", 1);
        PlayerPrefs.SetInt("nextscene", 1);
        PlayerPrefs.SetInt("currentScene", 3);
        PlayerPrefs.SetInt("ChapterOneActive", 1);
        PlayerPrefs.SetInt("AlreadyPlayed", 1);
        PlayerPrefs.SetInt("EnteredElevatorFirst", 1);
        PlayerPrefs.SetInt("SmartphoneTaken", 1);
        PlayerPrefs.SetInt("HatchClicked", 1);
        PlayerPrefs.SetInt("WirePuzzleInProgress", 1);
        PlayerPrefs.SetInt("ScrewdriverTaken", 1);
        PlayerPrefs.SetInt("PasswordPosterClicked", 1);
        PlayerPrefs.SetInt("PasswordFileClicked", 1);
        PlayerPrefs.SetInt("SSIDToNotes", 1);
        PlayerPrefs.SetInt("PrinterPWCorrect", 1);
        PlayerPrefs.SetInt("LiftCablesCorrect", 1);
        PlayerPrefs.SetInt("LiftQuestionAnswered", 1);
        PlayerPrefs.SetInt("ElevatorPanelBlockedAgain", 1);
        PlayerPrefs.SetString("currentNotification", "Fahre zum Festplattenraum!");
        PlayerPrefs.SetString("NoteStorage", "{\"AllNotes\":[{\"content\":\"Doggo und ich sind wirklich zwei gute Freunde!\",\"isNew\":false,\"isEditable\":false},{\"content\":\"SSID: buerolan01; passwort: DasBueroIstSicher64!\",\"isNew\":false,\"isEditable\":false}]}");
        //PlayerPrefs.SetString("TheQuizStorage", "{\"currentQuiz\":\"passwordQuiz\",\"AllQuiz\":[{\"questions\":[{\"question\":\"Welches der folgenden Passw�rter ist am sichersten?\",\"answers\":[\"1234\",\"13.01.1994\",\"Ig+h2igIm?\",\"Universit�tSiegen\"],\"correctAnswersIndexes\":[2]},{\"question\":\"Wof�r steht das 's' in https?\",\"answers\":[\"sant\",\"sauber\",\"super\",\"sicher\"],\"correctAnswersIndexes\":[3]}],\"isSolved\":true,\"name\":\"passwordQuiz\"},{\"questions\":[{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]},{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]},{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]}],\"isSolved\":false,\"name\":\"passwordQuiz2\"}]}");
        levelChanger.FadeToLevel(3);
        AudioManager.instance.Play("Maus");
    }
    public void goToCheckPoint002()
    {
        defaultStates.setToDefault();
        PlayerPrefs.SetInt("previousScene", 0);
        PlayerPrefs.SetInt("nextscene", 1);
        PlayerPrefs.SetInt("currentScene", 3);
        PlayerPrefs.SetInt("ChapterOneActive", 1);
        PlayerPrefs.SetInt("AlreadyPlayed", 1);
        PlayerPrefs.SetInt("EnteredElevatorFirst", 1);
        PlayerPrefs.SetInt("SmartphoneTaken", 1);
        PlayerPrefs.SetInt("HatchClicked", 1);
        PlayerPrefs.SetInt("WirePuzzleInProgress", 1);
        PlayerPrefs.SetInt("ScrewdriverTaken", 1);
        PlayerPrefs.SetInt("PasswordPosterClicked", 1);
        PlayerPrefs.SetInt("PasswordFileClicked", 1);
        PlayerPrefs.SetInt("SSIDToNotes", 1);
        PlayerPrefs.SetInt("PrinterPWCorrect", 1);
        PlayerPrefs.SetInt("LiftCablesCorrect", 1);
        PlayerPrefs.SetInt("LiftQuestionAnswered", 1);
        PlayerPrefs.SetInt("ElevatorPanelBlockedAgain", 1);
        PlayerPrefs.SetString("currentNotification", "Bestelle ein neues Festplatten-Team.");
        PlayerPrefs.SetString("NoteStorage", "{\"AllNotes\":[{\"content\":\"Doggo und ich sind wirklich zwei gute Freunde!\",\"isNew\":false,\"isEditable\":false},{\"content\":\"SSID: buerolan01; passwort: DasBueroIstSicher64!\",\"isNew\":false,\"isEditable\":false}]}");
        //PlayerPrefs.SetString("TheQuizStorage", "{\"currentQuiz\":\"passwordQuiz\",\"AllQuiz\":[{\"questions\":[{\"question\":\"Welches der folgenden Passw�rter ist am sichersten?\",\"answers\":[\"1234\",\"13.01.1994\",\"Ig+h2igIm?\",\"Universit�tSiegen\"],\"correctAnswersIndexes\":[2]},{\"question\":\"Wof�r steht das 's' in https?\",\"answers\":[\"sant\",\"sauber\",\"super\",\"sicher\"],\"correctAnswersIndexes\":[3]}],\"isSolved\":true,\"name\":\"passwordQuiz\"},{\"questions\":[{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]},{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]},{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]}],\"isSolved\":true,\"name\":\"passwordQuiz2\"}]}");
        PlayerPrefs.SetInt("talkedToAsr", 1);
        PlayerPrefs.SetInt("creditCardTaken", 1);
        levelChanger.FadeToLevel(3);
        AudioManager.instance.Play("Maus");
    }
    public void goToCheckPoint003()
    {
        defaultStates.setToDefault();
        PlayerPrefs.SetString("currentNotification", "Erkunde den Festplattenraum!");
        PlayerPrefs.SetInt("previousScene", 1);
        PlayerPrefs.SetInt("nextscene", 1);
        PlayerPrefs.SetInt("currentScene", 1);
        PlayerPrefs.SetInt("ChapterOneActive", 1);
        PlayerPrefs.SetInt("AlreadyPlayed", 1);
        PlayerPrefs.SetInt("EnteredElevatorFirst", 1);
        PlayerPrefs.SetInt("SmartphoneTaken", 1);
        PlayerPrefs.SetInt("HatchClicked", 1);
        PlayerPrefs.SetInt("WirePuzzleInProgress", 1);
        PlayerPrefs.SetInt("ScrewdriverTaken", 1);
        PlayerPrefs.SetInt("PasswordPosterClicked", 1);
        PlayerPrefs.SetInt("PasswordFileClicked", 1);
        PlayerPrefs.SetInt("SSIDToNotes", 1);
        PlayerPrefs.SetInt("PrinterPWCorrect", 1);
        PlayerPrefs.SetInt("LiftCablesCorrect", 1);
        PlayerPrefs.SetInt("LiftQuestionAnswered", 1);
        PlayerPrefs.SetString("currentNotification", "Erkunde den Festplattenraum!");
        PlayerPrefs.SetInt("ElevatorPanelBlockedAgain", 0); // changed to 0 in checkpoint 3
        
        //PlayerPrefs.SetString("TheQuizStorage", "{\"currentQuiz\":\"passwordQuiz\",\"AllQuiz\":[{\"questions\":[{\"question\":\"Welches der folgenden Passw�rter ist am sichersten?\",\"answers\":[\"1234\",\"13.01.1994\",\"Ig+h2igIm?\",\"Universit�tSiegen\"],\"correctAnswersIndexes\":[2]},{\"question\":\"Wof�r steht das 's' in https?\",\"answers\":[\"sant\",\"sauber\",\"super\",\"sicher\"],\"correctAnswersIndexes\":[3]}],\"isSolved\":true,\"name\":\"passwordQuiz\"},{\"questions\":[{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]},{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]},{\"question\":\"passwordQuiz2 Frage 1\",\"answers\":[\"Anwort 1\",\"Anwort 2 (correct)\",\"Anwort 3 (correct)\",\"Anwort 4\"],\"correctAnswersIndexes\":[1,3]}],\"isSolved\":true,\"name\":\"passwordQuiz2\"}]}");
        PlayerPrefs.SetInt("talkedToAsr", 1);
        PlayerPrefs.SetInt("creditCardTaken", 1);
        levelChanger.FadeToLevel(1);
        AudioManager.instance.Play("Maus");

        // new in checkpoint 3:
        PlayerPrefs.SetString("MessageStorage", "{\"Allmessages\":[{\"content\":\"15:32:22 PM: Das war ne super Party gestern. Hoffentlich hat keiner Fotos gemacht ;)\",\"isNew\":false,\"isEditable\":false,\"icon\":\"doggo\"},{\"content\":\"23:43:39 nachm.: Kreditkarten-Banking: Ihr 2-Faktor Sicherheitscode lautet: 2d355\",\"isNew\":false,\"isEditable\":false,\"icon\":\"defaultIcon\"},{\"content\":\"23:43:41 nachm.: Kreditkarten-Banking: Ihr 2-Faktor Sicherheitscode lautet: 2d355\",\"isNew\":false,\"isEditable\":false,\"icon\":\"defaultIcon\"},{\"content\":\"23:43:42 nachm.: Kreditkarten-Banking: Ihr 2-Faktor Sicherheitscode lautet: 2d355\",\"isNew\":false,\"isEditable\":false,\"icon\":\"defaultIcon\"},{\"content\":\"23:43:43 nachm.: Kreditkarten-Banking: Ihr 2-Faktor Sicherheitscode lautet: 2d355\",\"isNew\":false,\"isEditable\":false,\"icon\":\"defaultIcon\"}]}");
        PlayerPrefs.SetString("uBlocklyLevelStorage", "{\"currentLevel\":3,\"bestLevel\":0,\"AllLevels\":[{\"number\":0,\"mapJson\":\"{\\\"Name\\\":\\\"Level0\\\",\\\"Width\\\":3,\\\"Height\\\":4,\\\"Grids\\\":[11,2,11,11,10,11,11,10,11,11,1,11]}\",\"isSolved\":true,\"maxBlocks\":3,\"recordBox\":3,\"hint\":\"Verknüpfe die verfügbaren Anweisungen (linke Seite). Wenn du auf Start klickst wird fasdf diesen Anweisungen folgen. Laufe hier 3 Schritte vorwärts.\",\"wrongTrys\":1},{\"number\":1,\"mapJson\":\"{\\\"Name\\\":\\\"Level1\\\",\\\"Width\\\":4,\\\"Height\\\":4,\\\"Grids\\\":[11,11,11,11,11,2,10,11,11,12,10,11,11,11,1,11]}\",\"isSolved\":true,\"maxBlocks\":20,\"recordBox\":4,\"hint\":\"VORSICHT! Eine Hundefalle liegt im Weg. Nutze die richtige Anweisung, sodass fasdf ihr ausweicht (drehe links)\",\"wrongTrys\":1},{\"number\":2,\"mapJson\":\"{\\\"Name\\\":\\\"Level2\\\",\\\"Width\\\":5,\\\"Height\\\":5,\\\"Grids\\\":[11,11,11,11,11,11,10,2,10,11,11,10,12,10,11,11,10,10,10,11,11,11,1,11,11]}\",\"isSolved\":false,\"maxBlocks\":20,\"recordBox\":0,\"hint\":\"2 Verknüpfe die verfügbaren Anweisungen (linke Seite). Wenn du auf Start klickst wird fasdf diesen Anweisungen folgen.\",\"wrongTrys\":0},{\"number\":3,\"mapJson\":\"{\\\"Name\\\":\\\"Level3\\\",\\\"Width\\\":3,\\\"Height\\\":8,\\\"Grids\\\":[11,2,11,11,10,11,11,10,11,11,10,11,11,10,11,11,10,11,11,10,11,11,1,11]}\",\"isSolved\":true,\"maxBlocks\":3,\"recordBox\":2,\"hint\":\"Du hast hier nur 3 Befehle zur Verfügung. Benutze daher eine Zählschleife. Innerhalb einer Zählschleife werden die Befehle X-mal wiederholt ausgeführt.\",\"wrongTrys\":1},{\"number\":4,\"mapJson\":\"{\\\"Name\\\":\\\"Level4\\\",\\\"Width\\\":6,\\\"Height\\\":6,\\\"Grids\\\":[11,11,11,11,11,11,11,10,10,10,10,11,11,10,11,11,10,11,11,10,11,2,10,11,11,10,11,11,11,11,11,1,11,11,11,11]}\",\"isSolved\":false,\"maxBlocks\":6,\"recordBox\":0,\"hint\":\"Erinnere dich an die Schleife (Wiederhole bis Ziel erreicht) und erweitere diese um eine bedingte Anweisung: Wenn Feld frei vorne - gehe vorwärts; sonst drehe rechts\",\"wrongTrys\":0}]}");
        PlayerPrefs.SetString("NoteStorage", "{\"AllNotes\":[{\"content\":\"SSID: buerolan01; passwort: DasBueroIstSicher64!\",\"isNew\":true,\"isEditable\":false},{\"content\":\"Doggo und ich sind wirklich zwei gute Freunde!\",\"isNew\":true,\"isEditable\":false},{\"content\":\"REDcat110\",\"isNew\":true,\"isEditable\":false}]}");
        PlayerPrefs.SetInt("ElevatorPanelActive", 1);
        PlayerPrefs.SetInt("LiftQuestionTwoAnswered", 1);
        PlayerPrefs.SetInt("creditCardTaken", 1);
        PlayerPrefs.SetInt("creditCardTaken", 1);
        PlayerPrefs.SetInt("BuildingPlanFound", 1);
        PlayerPrefs.SetInt("BinaryPosterFound", 1);
        PlayerPrefs.SetInt("LiftStatesCorrect", 1);
        PlayerPrefs.SetInt("correctOnlineShopSelected", 1);
        PlayerPrefs.SetInt("PlugClicked", 1);
        PlayerPrefs.SetInt("REDpwTaken", 1);
        PlayerPrefs.SetInt("CreditCardCodeCorrect", 1);
        PlayerPrefs.SetInt("SsdDepartmentOrdered", 1);
        PlayerPrefs.SetInt("PlugInserted", 1);
    }


}